// react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View } from 'react-native';

// store
import store from '../redux/store';

// api
import { getOtherFactorsSummary } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import { fontFamily } from '../styles/variables';

export default class OtherFactors extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      otherFactorsSummary: store.getState().otherFactorsSummary,
      otherFactorsSentimentTypes: store.getState().otherFactorsSentimentTypes,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      otherFactorsSummary: store.getState().otherFactorsSummary,
      otherFactorsSentimentTypes: store.getState().otherFactorsSentimentTypes,
    });
  }

  componentDidMount() {
    if (this.state.user === null) return;
    
    let _this = this;
    let userId = this.state.user.id;

    if (this.state.otherFactorsSummary === null) {
      try { getOtherFactorsSummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    let st = this.state;

    let title = 'Upload credit report to see other factors summary';
    let item = null;

    if (st.otherFactorsSummary && st.otherFactorsSummary.numInCollection) {
      title = 'You have ' + st.otherFactorsSummary.numInCollection + ' accounts in collection and  ' + st.otherFactorsSummary.numChargedOff + ' charge offs.';
    }

    if (st.otherFactorsSummary !== null && st.otherFactorsSentimentTypes !== null && st.otherFactorsSentimentTypes.length > 0) {
      let total = st.otherFactorsSummary.numInCollection + st.otherFactorsSummary.numChargedOff;

      st.otherFactorsSentimentTypes.forEach(i => {
        if (i.min <= total && total <= i.max) {
          item = i;
        }
      });
    }

    return (
      <View style={cs.shadowBox}>

        <View style={cs.flex_row_space_between}>

          <Text style={cs.title1}>Other significant factors</Text>

          {item !== null &&
            <View style={{ backgroundColor: item.colorBg, height: 24, borderRadius: 8, width: 80, alignItems: 'center', paddingVertical: 1 }}>
              <Text style={{ fontFamily: fontFamily.semiBold, fontSize: 13, color: item.colorFg, letterSpacing: 0.5 }}>
                {item.result}
              </Text>
            </View>
          }

        </View>

        <View style={[cs.flex_row_space_between, cs.mr10]}>

          <Text style={[cs.subtitle2gray, cs.m5, cs.mr10]}>
            {title} {item !== null ? item.sentiment : ''}
          </Text>

          {/*
          <View style={[cs.mt20, cs.w10]}>
            <TouchableOpacity onPress={() => this.props.navigate('others')}>
              <Image
                source={require('../../images/chevron-right.png')}
                style={{ height: 35 }}
              />
            </TouchableOpacity>
          </View>
          */}
          
        </View>

      </View>
    );
  }
}

OtherFactors.propTypes = {
  navigate: PropTypes.func.isRequired
};
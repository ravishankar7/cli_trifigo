// react
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

// elements
import PrimeModal from '../elements/prime_modal';
import Button from '../elements/button';

// store
import store from '../redux/store';

// api
import { getCreditBossRatingSummary } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import { colors, fontFamily, fontSize, deviceWidth } from '../styles/variables';

const credit = {
  image1: {
    uri: require('../../images/credit_page/creditscore.png')
  }
}

export default class CreditRank extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      creditBossRatingSummary: store.getState().creditBossRatingSummary,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,

      modalVisible: false,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      creditBossRatingSummary: store.getState().creditBossRatingSummary,
    });
  }

  componentDidMount() {
    let _this = this;
    let userId = this.state.user.id;

    if (this.state.creditBossRatingSummary === null) {
      try { getCreditBossRatingSummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    let st = this.state;
    let rank = '-';
    
    if(st.creditBossRatingSummary !== null) {
      let score = st.creditBossRatingSummary.creditBossRating;

      if (score == 10) {
        rank = 'Credit Ace';
      }
      else if (score == 8 || score == 9) {
        rank = 'Credit King'; 
      }
      else if (score == 6 || score == 7) {
        rank = 'Credit Knight';
      }
      else if (score == 5 || score == 4) {
        rank = 'Credit Jack';
      }
      else {
        rank = 'Credit Novice';
      }
    }

    //onPress={() => this.setState({ modalVisible: true })}
    return (
      <TouchableOpacity style={styles.w} activeOpacity={0.8} >
          <View style={[cs.flex_center, { paddingTop: 5 }]}>
            <Text style={[styles.headertext, { fontSize: 12 }]}> {'Credit Knowledge'} </Text>
            
            <View style={[{ flexDirection: 'row', height: 50, justifyContent: 'center', marginTop: 5, alignItems : 'center' }]}>
            <Image style={[{ marginLeft: 5,  width: 50, height:50 }]} source={credit.image1.uri} />
            <Text style={[ styles.headertext, { color:colors.green, fontWeight: '500', fontSize: 16, flex: 1,  flexWrap: 'wrap' }]} > {'Credit Novice'} </Text>

            </View>

          <PrimeModal
            modalVisible={st.modalVisible}
            onRequestClose={() => { this.setState({ modalVisible: false }) }}>
            <View style={[cs.shadowBox, styles.wp]}>
              <View style={[cs.flex_row_start, cs.p5]}>
                <Text style={styles.headertextpopup}>Credit Rank</Text>
              </View>
              <Text style={[cs.p10]}>Coming soon</Text>
              <View style={[cs.flex_center, cs.p10, cs.mb20]}>
                <Button
                  text='Close'
                  width={100}
                  height={28}
                  fontSize={13}
                  backgroundColor='#546E7A'
                  onClick={() => this.setState({ modalVisible: false })} />
              </View>
            </View>
          </PrimeModal>

        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  headertext: {
    marginTop: 1,
    fontFamily: fontFamily.normal,
    fontSize: fontSize.xsmall
  },
  headertextpopup: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.normal
  },
  subtext: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.xsmall,
    color: colors.green,
  },
  subtextpopup: {
    fontFamily: fontFamily.normal,
    fontSize: fontSize.normal,
    color: colors.green,
  },
  w: {
    width: deviceWidth / 3
  },
  wp: {
    width: deviceWidth - 50
  }
});
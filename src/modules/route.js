export default class Route {

	constructor(rootUrl) {
		this.rootUrl = rootUrl;
	}

	get = (url) => {
		return fetch(this.rootUrl + url)
			.then(this.checkStatus)
			.then(response => response.json())
			.catch(e => e)
	}

	post = (url) => {
		// console.log('url >>>', this.rootUrl + url);
		return fetch(this.rootUrl + url, {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "POST",
		}).then(this.checkStatus)
			.then(response => response.json())
			.catch(e => e)
	}
	
	postdata = (url, data) => {
		// console.log('url >>>', this.rootUrl + url);
		// console.log('data >>>', JSON.stringify(data, null, 2));
		return fetch(this.rootUrl + url, {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: "POST",
			body: JSON.stringify(data)
		})
			.then(this.checkStatus)
			.then(response => response.json())
			.catch(e => e)
	}

	postfile = (url, data) => {
		return fetch(this.rootUrl + url, {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			method: "POST",
			body: data
		})
			.then(this.checkStatus)
			.then(response => response.json())
			.catch(e => e)
	}

	postfile_x = (url, data) => {
		return fetch(this.rootUrl + url, {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'multipart/form-data'
			},
			method: "POST",
			body: data
		});
	}

	checkStatus = (response) => {
		//console.log('checkStatus response >>>', response);
		if (response.ok) {
			return response;
		}
		else if (response.status === 400) {
			let error = new Error('Error', 'Not found');

			try {
				if (response.headers && response.headers.map && response.headers.map.err) {
					//console.log('header err >>>', response.headers.map.err);
					let _err = String(response.headers.map.err);
					response._bodyText = _err;
					let error = new Error(response._bodyText);
					error = response;
					throw error;
				}
			} catch (e) {
				/* no op */
			}

			throw error;
		}
		else {
			let error = new Error(response.statusText);
			error.response = response;
			throw error;
		}
	}
}
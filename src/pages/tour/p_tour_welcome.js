// react
import React from 'react';
import PropTypes from 'prop-types';

import { View, Image, Text } from 'react-native';

// elements
import LightGradient from '../../elements/light_gradient';

// styles
import cs from '../../styles/common_styles.js';

export default class PageTourWelcome extends React.Component {

	static navigationOptions = {
		header: null,
	};

	render = () => {
		return (
			<View style={[cs.flex_col_center_top]}>
			
				<LightGradient />

				<View style={[cs.flex_row_center, cs.ph15, cs.mt10]}>

					<Text style={[cs.title1, cs.mh10, cs.mt7, { flex: 0.4 }]} onPress={() => this.props.navigation.navigate('credit_profile')}>
						Skip Tour
					</Text>

					<View style={{ flex: 1 }} />

					<Text style={[cs.title1, cs.mh10, cs.mt7, { flex: 0.25 }]} onPress={() => this.props.navigation.navigate('tour_add_photo')}>
						Next
					</Text>
				</View>

				<View style={[cs.flex_row_center, cs.mt10]}>
					<View style={[cs.roundimage]}>
						<Image
							source={require('../../../images/welcome.jpg')}
							style={[{ borderRadius: 130, width: 260, height: 260 }]}
						/>
					</View>
				</View>

				<View style={[cs.flex_row_center, cs.mt10]}>
					<Text style={[cs.logotextlg]}>
						Welcome to
					</Text>
				</View>

				<View style={[cs.flex_row_center]}>
					<Text style={[cs.logotextlg]}>
						Trifigo
					</Text>
				</View>

				<View style={[cs.flex_row_center, cs.mt10]}>
					<Text style={[cs.title2white, cs.m5]}>
						{`Let's get you started on your Credit Journey`}
					</Text>
				</View>

			</View>
		);
	}
}

PageTourWelcome.propTypes = {
	navigation: PropTypes.object.isRequired
};
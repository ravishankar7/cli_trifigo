// react
import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';

// store
import store from '../redux/store';

// api
import { getAccountMixSummary } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import { fontFamily } from '../styles/variables';

export default class AccountMix extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      accountMixSummary: store.getState().accountMixSummary,
      accountMixSentimentTypes: store.getState().accountMixSentimentTypes,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      accountMixSummary: store.getState().accountMixSummary,
      accountMixSentimentTypes: store.getState().accountMixSentimentTypes,
    });
  }

  componentDidMount() {
    if (this.state.user === null) return;
    
    let _this = this;
    let userId = this.state.user.id;

    if (this.state.accountMixSummary === null) {
      try { getAccountMixSummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    let st = this.state;

    let title = 'Upload credit report to see account summary';
    let item = null;

    if (st.accountMixSummary && st.accountMixSummary.totalAccounts) {
      title = 'Your total number of accounts are ' + st.accountMixSummary.totalAccounts + '.';
    }

    if (st.accountMixSummary !== null && st.accountMixSentimentTypes !== null && st.accountMixSentimentTypes.length > 0) {
      st.accountMixSentimentTypes.forEach(i => {
        if (i.min <= st.accountMixSummary.totalAccounts && st.accountMixSummary.totalAccounts <= i.max) {
          item = i;
        }
      });
    }

    return (
      <View style={cs.shadowBox}>

        <View style={cs.flex_row_space_between}>

          <Text style={cs.title1}>Total Accounts</Text>

          {item !== null &&
            <View style={{ backgroundColor: item.colorBg, height: 24, borderRadius: 8, width: 80, alignItems: 'center', paddingVertical: 1 }}>
              <Text style={{ fontFamily: fontFamily.semiBold, fontSize: 13, color: item.colorFg, letterSpacing: 0.5 }}>
                {item.result}
              </Text>
            </View>
          }

        </View>

        <Text style={[cs.secondtitletext, cs.mb10]}>
          Low Importance / 10%
        </Text>

        <View style={[cs.flex_row_space_between, cs.mr10]}>

          <Text style={[cs.subtitle2gray, cs.m5, cs.mr10]}>
            {title} {item !== null ? item.sentiment : ''}
          </Text>
          
          {/*
          <View style={[cs.mt10, cs.w10]}>
            <TouchableOpacity onPress={() => this.props.navigate('account_mix')}>
              <Image
                source={require('../../images/chevron-right.png')}
                style={{ height: 35 }}
              />
            </TouchableOpacity>
          </View>
          */}

        </View>

      </View>
    );
  }
}

AccountMix.propTypes = {
  navigate: PropTypes.func.isRequired
};
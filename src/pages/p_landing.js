// react
import React from 'react';
import PropTypes from 'prop-types';
import { Image, View, Text, Platform } from 'react-native';

// google analytics
import { trackScreenView } from '../modules/ga';

// store
import store from '../redux/store';

// api
import {
  getRefConsumerTypes,
  getRefPaymentSentimentTypes,
  getRefCreditUtilizationSentimentTypes,
  getRefAccountAgeSentimentTypes,
  getRefAccountMixSentimentTypes,
  getRefCreditInquirySentimentTypes,
  getRefOtherFactorsSentimentTypes,
  getRecoTemplates,
} from '../modules/api';

// styles
import cs from '../styles/common_styles.js';
import DarkGradient from '../elements/dark_gradient.js';

export default class PageLanding extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      consumerTypes: store.getState().consumerTypes,
      paymentSentimentTypes: store.getState().paymentSentimentTypes,
      creditUtilizationSentimentTypes: store.getState().creditUtilizationSentimentTypes,
      accountAgeSentimentTypes: store.getState().accountAgeSentimentTypes,
      accountMixSentimentTypes: store.getState().accountMixSentimentTypes,
      creditInquirySentimentTypes: store.getState().creditInquirySentimentTypes,
      otherFactorsSentimentTypes: store.getState().otherFactorsSentimentTypes,
      recoTemplates: store.getState().recoTemplates,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };

    trackScreenView('Landing');
  }

  updateState = () => {
    this.setState({
      consumerTypes: store.getState().consumerTypes,
      paymentSentimentTypes: store.getState().paymentSentimentTypes,
      creditUtilizationSentimentTypes: store.getState().creditUtilizationSentimentTypes,
      accountAgeSentimentTypes: store.getState().accountAgeSentimentTypes,
      accountMixSentimentTypes: store.getState().accountMixSentimentTypes,
      creditInquirySentimentTypes: store.getState().creditInquirySentimentTypes,
      otherFactorsSentimentTypes: store.getState().otherFactorsSentimentTypes,
      recoTemplates: store.getState().recoTemplates,
    });
  }

  componentDidMount() {

    if (this.state.consumerTypes === null) {
      getRefConsumerTypes();
    }

    if (this.state.paymentSentimentTypes === null) {
      getRefPaymentSentimentTypes();
    }

    if (this.state.creditUtilizationSentimentTypes === null) {
      getRefCreditUtilizationSentimentTypes();
    }

    if (this.state.accountAgeSentimentTypes === null) {
      getRefAccountAgeSentimentTypes();
    }

    if (this.state.accountMixSentimentTypes === null) {
      getRefAccountMixSentimentTypes();
    }

    if (this.state.creditInquirySentimentTypes === null) {
      getRefCreditInquirySentimentTypes();
    }

    if (this.state.otherFactorsSentimentTypes === null) {
      getRefOtherFactorsSentimentTypes();
    }

    if (this.state.recoTemplates === null) {
      getRecoTemplates();
    }

    this.timer = setInterval(() => {
      let st = this.state;

      if (st.consumerTypes !== null
        && st.paymentSentimentTypes !== null
        && st.creditUtilizationSentimentTypes !== null
        && st.accountAgeSentimentTypes !== null
        && st.accountMixSentimentTypes !== null
        && st.creditInquirySentimentTypes !== null
        && st.otherFactorsSentimentTypes !== null
        && st.recoTemplates !== null
      ) {
        const { navigate } = this.props.navigation;

        clearInterval(this.timer);

        navigate('login');
      }
    }, 60000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    this.state.unsubscribe();
  }

  render() {
    return (
      <View style={cs.flex_center}>

        <DarkGradient />

        <Image source={require('../../images/t_icon.png')}
          style={{ width: 130, height: 110, marginTop: (Platform.OS === 'android' ? 55 : 0) }} />
        <Text style={cs.logotextlg}>TRIFIGO</Text>
        <View style={cs.m5} />
        <Text style={cs.logotext2green}>JOIN THE 800 CLUB</Text>
      </View>
    );
  }
}

PageLanding.propTypes = {
  navigation: PropTypes.object.isRequired
};

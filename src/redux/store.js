import { createStore } from 'redux';

const initialState = {
	userId: null,
	user: null,
	signup_data: {},
	security_questions: null,
	user_goals: [],
	// one signal device info
	onesignal_device: null,
	this_user_goals: [],

	// summaries
	additionalCreditSummary: null,
	consumerTypeSummary: null,
	pastCashFlowSummary: null,
	currentCashFlowSummary: null,
	yodleeAccountSummary: null,
	bankingTotalsSummary: null,
	paymentSummary: null,
	accountAgeSummary: null,
	creditUtilizationSummary: null,
	accountMixSummary: null,
	creditInquirySummary: null,
	otherFactorsSummary: null,
	creditBossRatingSummary: null,
	recos: null,
	allRecos: null,
	dealItems: null,

	// ref data
	universities: null,
	consumerTypes: null,
	creditQuiz: null,
	paymentSentimentTypes: null,
	creditUtilizationSentimentTypes: null,
	accountAgeSentimentTypes: null,
	accountMixSentimentTypes: null,
	creditInquirySentimentTypes: null,
	otherFactorsSentimentTypes: null,
	recoTemplates: null,

	//netowrkstatus
	connectionStatus : null
};

const reducer = (state = initialState, action) => {

	switch (action.type) {

		case 'GET_SECURITY_QUESTIONS':
			return { ...state, security_questions: action.payload };

		case 'RESET':
			return { ...initialState };

		case 'LOGIN':
			return { ...state, user: action.payload };

		case 'SET_USER_ID':
			return { ...state, userId: action.payload };

		case 'SET_SIGNUP':
			return { ...state, signup_data: action.payload };

		case 'LOGOUT':
			return { ...state, user: null };

		case 'SET_USER':
			return { ...state, user: action.payload };

		case 'STORE_GOALS':
			{
				let goals = state.user_goals
				goals.push(action.payload)
				return { ...state, user_goals: goals }
			}

		case 'GET_USER_GOALS':
			//let goals = state.this_user_goals
			//console.log('Getting this user goals:',action.payload)
			//goals.push(action.payload)
			return { ...state, this_user_goals: action.payload }

		// onesignal device
		case 'SET_ONESIGNAL_DEVICE':
			return { ...state, onesignal_device: action.payload };

		// summary
		case 'SET_YODLEE_ACCOUNT_SUMMARY':
			return { ...state, yodleeAccountSummary: action.payload };

		case 'SET_QUOVO_ACCOUNT_SUMMARY':
			return { ...state, quovoAccountSummary: action.payload };

		case 'SET_CREDIT_SUMMARY':
			return { ...state, creditSummary: action.payload };

		case 'SET_CONSUMER_TYPE_SUMMARY':
			return { ...state, consumerTypeSummary: action.payload };

		case 'SET_ADDTIONAL_CREDIT_SUMMARY':
			return { ...state, additionalCreditSummary: action.payload };

		case 'SET_CREDIT_UTILIZATION_SUMMARY':
			return { ...state, creditUtilizationSummary: action.payload };

		case 'SET_ACCOUNT_MIX_SUMMARY':
			return { ...state, accountMixSummary: action.payload };

		case 'SET_PAST_CASH_FLOW_SUMMARY':
			return { ...state, pastCashFlowSummary: action.payload };

		case 'SET_CURRENT_CASH_FLOW_SUMMARY':
			return { ...state, currentCashFlowSummary: action.payload };

		case 'SET_CREDIT_INQUIRY_SUMMARY':
			return { ...state, creditInquirySummary: action.payload };

		case 'SET_BANKING_TOTALS_SUMMARY':
			return { ...state, bankingTotalsSummary: action.payload };

		case 'SET_PAYMENT_SUMMARY':
			return { ...state, paymentSummary: action.payload };

		case 'SET_ACCOUNT_AGE_SUMMARY':
			return { ...state, accountAgeSummary: action.payload };

		case 'SET_OTHER_FACTORS_SUMMARY':
			return { ...state, otherFactorsSummary: action.payload };

		case 'SET_CREDIT_BOSS_RATING_SUMMARY':
			return { ...state, creditBossRatingSummary: action.payload };

		case 'SET_RECOS':
			return { ...state, recos: action.payload };

		case 'SET_ALL_RECOS':
			return { ...state, allRecos: action.payload };

		// ref data 
		case 'SET_REF_UNIVERSITIES':
			return { ...state, universities: action.payload };

		case 'SET_REF_CONSUMER_TYPES':
			return { ...state, consumerTypes: action.payload };

		case 'SET_REF_CREDIT_QUIZ':
			return { ...state, creditQuiz: action.payload };

		case 'SET_PAYMENT_SENTIMENT_TYPES':
			return { ...state, paymentSentimentTypes: action.payload };

		case 'SET_CREDIT_UTILIZATION_SENTIMENT_TYPES':
			return { ...state, creditUtilizationSentimentTypes: action.payload };

		case 'SET_ACCOUNT_AGE_SENTIMENT_TYPES':
			return { ...state, accountAgeSentimentTypes: action.payload };

		case 'SET_ACCOUNT_MIX_SENTIMENT_TYPES':
			return { ...state, accountMixSentimentTypes: action.payload };

		case 'SET_CREDIT_INQUIRY_SENTIMENT_TYPES':
			return { ...state, creditInquirySentimentTypes: action.payload };

		case 'SET_OTHER_FACTORS_SENTIMENT_TYPES':
			return { ...state, otherFactorsSentimentTypes: action.payload };

		case 'SET_RECO_TEMPLATES':
			return { ...state, recoTemplates: action.payload };

		case 'SET_DEAL_ITEMS':
			return { ...state, dealItems: action.payload };

		case 'SET_NETWORK_STATUS' : 
			return { ...state, connectionStatus: action.payload }

		default:
			return state;
	}
};

export default createStore(reducer);
// react
import React from 'react';
import PropTypes from 'prop-types';

import { Image, View, Text, TextInput, TouchableOpacity } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import DarkGradient from '../../elements/dark_gradient';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';

// redux
import store from '../../redux/store';
import { setUserId } from '../../redux/actions';

// api
import { get_user_by_token } from '../../modules/api';

// styles
import cs from '../../styles/common_styles.js';
import { colors, deviceWidth } from '../../styles/variables';

export default class PageForgotPassToken extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {

      token: '',
      err_token: null,

      error: null,
      loading: ''
    };

    trackScreenView('ForgotPassToken');
  }

  backClick = () => {
    this.props.navigation.navigate('login');
  }

  submitForm = () => {
    let st = this.state;

    this.setState({ error: null, loading: ' is-loading' });

    this.resetErrors();

    let errors = this.validateError();

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    get_user_by_token(st.token).then((res) => {
      if(res && res.id) {
        store.dispatch(setUserId(res.id));
        this.props.navigation.navigate('forgot_pass_reset');
      }
      else {
        this.setState({ err_token: 'Code is invalid', error: 'Code is invalid' });
      }
    });
  }

  validateError = () => {
    let errors = 0;
    let st = this.state;
    let err = null;

    //Validate Token
    if (!st.token) {
      errors++;
      this.setState({ err_token: 'Code is required' });
      if (err === null) err = 'Code is required';
    }

    if (errors === 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  resetErrors = () => {
    this.setState({
      err_token: null,
      error: null
    });
  }

  render() {
    let st = this.state;

    return (
      <View style={cs.flex_col_center}>

        <DarkGradient />

        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='always'>

          <View style={[cs.formbox]}>

            <View style={cs.mt30} />

            <View style={[cs.flex_row_start, cs.mb10]}>
              <TouchableOpacity onPress={this.backClick} style={{ flex: 1 }}>
                <View style={[{ marginTop: 8 }]}>
                  <Image
                    source={require('../../../images/menu-back.png')}
                    style={{ height: 30 }}
                  />
                </View>
              </TouchableOpacity>

              <Text style={[cs.headertext, { flex: 2.5 }]}>
                Enter code
              </Text>
            </View>

            <View style={ cs.bluecontainer}>
              <Text style={cs.subtitle2white}>
                Check your email and enter the unique login code we just sent you.
                {'\n'}{'\n'}
                It might take a minute or so for the email to show up, check your spam folders.
                {'\n'}{'\n'}
                Use the token in that email to reset your password.
              </Text>
            </View>

            <View style={cs.mt20} />

            <View style={st.err_token !== null ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Verification code'
                style={cs.textInputDark}
                onChangeText={(text) => this.setState({ token: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(255,255,255,0.7)'
                autoCapitalize={'characters'}
                autoCorrect={false}
              />
            </View>

            <View style={[cs.buttonBox, { marginBottom: 8 }]}>

              <Button
                text='Next'
                width={deviceWidth - 55}
                height={44}
                fontSize={18}
                backgroundColor={colors.green}
                onClick={this.submitForm} />

            </View>

            <ErrorBox text={st.error} />

          </View>

        </KeyboardAwareScrollView>

      </View>
    );
  }
}

PageForgotPassToken.propTypes = {
  navigation: PropTypes.object.isRequired
};
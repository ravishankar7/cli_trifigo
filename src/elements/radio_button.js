// react
import React from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, View, TouchableOpacity } from 'react-native';

// styles
import { colors } from '../styles/variables';

export default class RadioButton extends React.Component {

	constructor(props) {
		super(props);
	}
	
	onClick = () => {
		if (this.props.onClick) this.props.onClick();
	}

	render() {
		const border = {
			borderColor: this.props.selected ? colors.seagreen : colors.lightGrey
		};
		const background = {
			backgroundColor: this.props.selected ? colors.seagreen : colors.lightGrey
		};

		return (
			<TouchableOpacity onPress={() => this.onClick()}>
				<View style={[styles.outer, border]}>
					<View style={[styles.inner, background]}/>
				</View>
			</TouchableOpacity>
		);
	}
}

RadioButton.propTypes = {
	selected: PropTypes.bool,
	onClick: PropTypes.func
};

const styles = StyleSheet.create({
	outer: {
		height: 24,
		width: 24,
		borderRadius: 12,
		borderWidth: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
	inner: {
		height: 16,
		width: 16,
		borderRadius: 8,
	}
});
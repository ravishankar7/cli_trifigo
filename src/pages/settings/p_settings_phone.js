//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View, Image, TextInput } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import OkBox from '../../elements/ok_box';

// store
import store from '../../redux/store';

// api
import { updateUser } from '../../modules/api';

// styles
import Spinner from 'react-native-spinkit';
import cs from '../../styles/common_styles';
import { colors } from '../../styles/variables';

export default class PageSettingsPhone extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;

    this.state = {
      user: _user,

      unsubscribe: store.subscribe(this.updateState),

      cellPhone: _user !== null ? _user.cellPhone : null,
      err_cellphone: null,

      otherPhone: _user !== null ? _user.otherPhone : null,
      err_otherphone: null,

      error: null,
      loading: null,
      success: null,
    };

    trackScreenView('Settings Phone');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,

      name: _user !== null ? _user.cellPhone : null,
      err_cellphone: null,

      lname: _user !== null ? _user.otherPhone : null,
      err_otherphone: null,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  resetErrors = () => {
    this.setState({
      loading: null,
      error: null,
      success: null,

      err_cellphone: null,
      err_otherphone: null,
    });
  }

  validateData = () => {
    this.resetErrors();

    let errors = 0;
    let err = null;

    let st = this.state;

    // Validate name
    if (st.cellPhone === null || st.cellPhone === undefined || st.cellPhone == '') {
      errors++;
      this.setState({ err_cellphone: 'Cellphone required' });
      if (err === null) err = 'Cellphone required';
    }

    // Validate last name
    if (st.otherPhone === null || st.otherPhone === undefined || st.otherPhone == '') {
      errors++;
      this.setState({ err_otherphone: 'Other phone required' });
      if (err === null) err = 'Other phone required';
    }

    if (errors == 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  saveData = () => {
    let errors = this.validateData();

    this.setState({
      loading : 'loading'
    })

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    // save data
    let _this = this;
    let st = this.state;

    let userId = this.state.user.id;
    let user = this.state.user;

    user.cellPhone = st.cellPhone;
    user.otherPhone = st.otherPhone;

    try {
      updateUser(userId, user);
      _this.setState({ success: 'Saved' }, () => {
        _this.timer = setInterval(() => {
          const { navigate } = this.props.navigation;
          clearInterval(_this.timer);
          navigate('settings');
        }, 1000);
      });
    }
    catch (e) { _this.setState({ error: e }); }
  }

  cancel = () => {
    this.props.navigation.navigate('settings');
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Settings'
          back='settings'
        />

        <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center, cs.mt30]}>

            <Text style={[cs.headertextdarkgray, cs.mb10]}>Phone</Text>

            {/* Name textbox */}
            <View style={st.err_cellphone !== null ? cs.textInputFieldError : cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Cell Phone'
                value={st.cellPhone}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ cellPhone: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'none'}
                autoCorrect={false}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                returnKeyType="next"
              />
            </View>

            {/* Last Name textbox */}
            <View style={st.err_otherphone !== null ? cs.textInputFieldError : cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Other Phone'
                ref={(input) => { this.secondTextInput = input; }}
                returnKeyType="done"
                value={st.otherPhone}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ otherPhone: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            { st.loading 
            ?
                  <Spinner
                  isVisible
                  size={60}
                  type='ThreeBounce'
                  color={colors.green} />

            :
             <View style={[cs.flex_row_center, cs.mb10]}>

              <View style={cs.m5}>
                <Button
                  text='Cancel'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#CFD8DC'
                  textColor='#404040'
                  onClick={this.cancel} />
              </View>

              <View style={cs.m5}>
                <Button
                  text='Save'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#66BB6A'
                  onClick={this.saveData} />
              </View>

            </View>}

            <View style={[cs.flex_row_center, cs.mb10]}>

              {st.error !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              }

              {st.success !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              }

            </View>

          </View>

        </KeyboardAwareScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsPhone.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettingsPhone.defaultProps = {
  user: null,
};

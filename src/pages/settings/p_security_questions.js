//react
import React from "react";
import PropTypes from "prop-types";

import { Text, View, TextInput } from "react-native";

// external
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";

// google analytics
import { trackScreenView } from "../../modules/ga";

// elements
import Header from "../../elements/header";
import Footer from "../../elements/footer";
import Button from "../../elements/button";
import ErrorBox from "../../elements/error_box";
import OkBox from "../../elements/ok_box";

// store
import store from "../../redux/store";

// api
import { updateUser } from "../../modules/api";

// styles
import cs from "../../styles/common_styles";
import { deviceWidth, colors } from "../../styles/variables";

//spinner
import Spinner from "react-native-spinkit";

export default class PageSecurityQuestions extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;
    let _questions = store.getState().security_questions;

    var data = [];
    for (let i = 0; i < _questions.length; i++) {
      data.push({ id: i, name: _questions[i] });
    }

    this.state = {
      user: _user,
      questions: data,

      unsubscribe: store.subscribe(this.updateState),

      Q1: _user.securityQuestion1,
      Q2: _user.securityQuestion2,
      Q3: _user.securityQuestion3,

      answer1: _user.securityAnswer1,
      answer2: _user.securityAnswer2,
      answer3: _user.securityAnswer3,

      error: null,
      loading: null,
      success: null,

      show1: true,
      show2: true,
      show3: true
    };

    trackScreenView("Settings Security Questions");
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user
    });
  };

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  resetErrors = () => {
    this.setState({
      loading: null,
      error: null,
      success: null
    });
  };

  validateData = () => {
    this.resetErrors();

    let errors = 0;
    let err = null;

    //let st = this.state;

    if (errors == 1) {
      this.setState({ error: err });
    } else if (errors > 1) {
      this.setState({ error: "Correct all errors" });
    }

    return errors;
  };

  saveData = () => {
    let errors = this.validateData();

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    this.setState({
      loading: 'loading'
    });

    // save data
    let _this = this;
    let st = this.state;

    let userId = this.state.user.id;
    let user = this.state.user;

    user.securityQuestion1 = st.Q1;
    user.securityQuestion2 = st.Q2;
    user.securityQuestion3 = st.Q3;

    user.securityAnswer1 = st.answer1;
    user.securityAnswer2 = st.answer2;
    user.securityAnswer3 = st.answer3;

    try {
      updateUser(userId, user);
      _this.setState({ success: "Saved" }, () => {
        _this.timer = setInterval(() => {
          const { navigate } = this.props.navigation;
          clearInterval(_this.timer);
          navigate("settings");
        }, 1000);
      });
    } catch (e) {
      _this.setState({ error: e });
    }
  };

  cancel = () => {
    this.props.navigation.navigate("settings");
  };

  changeQ1 = data => {
    this.setState({ Q1: data.name, show1: true });
  };

  changeQ2 = data => {
    this.setState({ Q2: data.name, show2: true });
  };

  changeQ3 = data => {
    this.setState({ Q3: data.name, show3: true });
  };

  render() {
    let st = this.state;

    //let user = st.user;

    const { navigate } = this.props.navigation;

    //if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>
        <Header
          navigate={navigate}
          title="Security Questions"
          back="settings"
        />

        <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
          <View style={[cs.flex_center]}>
            <Text style={[cs.headertextdarkgray, cs.mb10]}>
              Security Questions
            </Text>

            <View
              style={{
                width: deviceWidth - 40,
                marginBottom: 20,
                padding: 10,
                backgroundColor: "#4E6D87",
                borderRadius: 10
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Poppins-SemiBold",
                  fontWeight: "600",
                  justifyContent: "flex-start",
                  color: "white",
                  letterSpacing: 0.5
                }}
              >
                {this.state.questions[0].name}
              </Text>
            </View>

            {this.state.show1 ? (
              <View style={cs.textInputField}>
                <TextInput
                  placeholder="Your answer"
                  value={st.answer1}
                  style={cs.textInput}
                  onChangeText={text => this.setState({ answer1: text })}
                  underlineColorAndroid="rgba(0,0,0,0)"
                  placeholderTextColor="rgba(0,0,0,0.3)"
                  autoCapitalize={"none"}
                  autoCorrect={false}
                />
              </View>
            ) : null}

            <View
              style={{
                width: deviceWidth - 40,
                marginBottom: 20,
                padding: 10,
                backgroundColor: "#4E6D87",
                borderRadius: 10
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Poppins-SemiBold",
                  fontWeight: "600",
                  justifyContent: "flex-start",
                  color: "white",
                  letterSpacing: 0.5
                }}
              >
                {this.state.questions[1].name}
              </Text>
            </View>

            {this.state.show2 ? (
              <View style={cs.textInputField}>
                <TextInput
                  placeholder="Your answer"
                  value={st.answer2}
                  style={cs.textInput}
                  onChangeText={text => this.setState({ answer2: text })}
                  underlineColorAndroid="rgba(0,0,0,0)"
                  placeholderTextColor="rgba(0,0,0,0.3)"
                  autoCapitalize={"none"}
                  autoCorrect={false}
                />
              </View>
            ) : null}

            <View
              style={{
                width: deviceWidth - 40,
                marginBottom: 20,
                padding: 10,
                backgroundColor: "#4E6D87",
                borderRadius: 10
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Poppins-SemiBold",
                  fontWeight: "600",
                  justifyContent: "flex-start",
                  color: "white",
                  letterSpacing: 0.5
                }}
              >
                {this.state.questions[2].name}
              </Text>
            </View>

            {this.state.show3 ? (
              <View style={cs.textInputField}>
                <TextInput
                  placeholder="Your answer"
                  value={st.answer3}
                  style={cs.textInput}
                  onChangeText={text => this.setState({ answer3: text })}
                  underlineColorAndroid="rgba(0,0,0,0)"
                  placeholderTextColor="rgba(0,0,0,0.3)"
                  autoCapitalize={"none"}
                  autoCorrect={false}
                />
              </View>
            ) : null}

            { this.state.loading
            ?

            <Spinner
                  isVisible
                  style = {cs.mt2}
                  size={60}
                  type='ThreeBounce'
                  color={colors.green} />

            : <View style={[cs.flex_row_center, cs.mb10]}>
              <View style={cs.m5}>
                <Button
                  text="Cancel"
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor="#CFD8DC"
                  textColor="#404040"
                  onClick={this.cancel}
                />
              </View>

              <View style={cs.m5}>
                <Button
                  text="Save"
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor="#66BB6A"
                  onClick={this.saveData}
                />
              </View>
            </View>}

            <View style={[cs.flex_row_center, cs.mb10]}>
              {st.error && (
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              )}

              {st.success && (
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              )}
            </View>
          </View>
        </KeyboardAwareScrollView>

        <Footer navigate={navigate} />
      </View>
    );
  }
}

PageSecurityQuestions.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSecurityQuestions.defaultProps = {
  user: null,
  security_questions: null
};

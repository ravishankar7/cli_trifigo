//react 
import React from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, View, ScrollView, Text, Switch } from 'react-native';

//import ImagePicker from 'react-native-image-crop-picker';
import { RNCamera } from 'react-native-camera';

// google analytics
import { trackScreenView } from '../modules/ga';

// elements
import Header from '../elements/header';
import Footer from '../elements/footer';
import Button from '../elements/button';
import ErrorBox from '../elements/error_box';
import OkBox from '../elements/ok_box';

// store
import store from '../redux/store';

// api
import {
  uploadProfilePhoto,
  updateUser
} from '../modules/api';

import { setUser } from '../redux/actions';

// styles
import cs from '../styles/common_styles.js';
import { deviceWidth } from '../styles/variables';

//loader
import Spinner from 'react-native-spinkit';

export default class PageCamera extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;

    this.state = {
      user: _user,

      ppic: null,
      switchValue: true,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
      success: null,
    };
    
    trackScreenView('Camera');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  cancel = () => {
    this.props.navigation.navigate('settings_images');
  }
  
  takePhoto = async () => {
    let _this = this;

    if (this.camera) {
      const options = { quality: 0.4, base64: true,width : 400 };
      const image = await this.camera.takePictureAsync(options);
      //console.log('camera image >>>', image);
      this.setState({
        loading : 'loading'
      })
      let userId = this.state.user.id;
      let exnParts = image.uri.split('.');
      let pathParts = image.uri.split('/');

      let formData = new FormData();
      
      formData.append(`file`, {
        uri: image.uri,
        type: 'image/' + exnParts[exnParts.length - 1],
        name: pathParts[pathParts.length - 1]
      });

      //console.log('formData >>>', JSON.stringify(formData, null, 2));

      try {
        uploadProfilePhoto(userId, formData);

        let user = _this.state.user;
        user.profileImgUrl = 'https://s3-us-west-2.amazonaws.com/tfgimages/' + this.state.user.id + '/' + pathParts[pathParts.length - 1];

        updateUser(user.id, user);

        _this.setState({ user: user });

        setUser(user);

        _this.setState({ success: 'Image uploaded' }, () => {
          _this.timer = setInterval(() => {
            const { navigate } = this.props.navigation;
            clearInterval(_this.timer);
            navigate('settings_images', { image : image.uri });
          }, 2000);
        });
      }
      catch (e) { _this.setState({ error: e, loading : null }); }
    }
  }

  handleToggleSwitch = () => {
    this.setState(state => ({
      switchValue: !state.switchValue,
    }));
  }

  render() {
    let _this = this;
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (user === null || user === undefined) {
      return (
        <View style={cs.normalPage}>
        </View>
      );
    }
    
    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Camera'
          back='settings_images'
        />

        <ScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center, cs.mt30]}>

            {/*** Toggle switch ***/}
            <View style={[cs.flex_col_center, cs.mb10]}>
              <Text style={[cs.title1, cs.m5]}>{this.state.switchValue ? 'Front' : 'Back'} Camera</Text>
              <Switch
                style={cs.mh5}
                onValueChange={this.handleToggleSwitch}
                value={this.state.switchValue}
              />
            </View>

            <View style={[styles.preview]} >
              <RNCamera
                ref={ref => {
                  this.camera = ref;
                }}
                style={{ width : '100%', height : '100%' }}
                type={this.state.switchValue ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.on}
                captureAudio={false}
                permissionDialogTitle={'Permission to use camera'}
                permissionDialogMessage={'We need your permission to use your camera'}
              />
            </View>
            
            <View style={cs.m5} />

            <View style={[cs.flex_row_center, cs.mb10]}>

              <View style={cs.m5}>
                <Button
                  text='Cancel'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#CFD8DC'
                  textColor='#404040'
                  onClick={_this.cancel} />
              </View>

              <View style={cs.m5}>
                <Button
                  text='Capture'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#4DB6AC'
                  onClick={this.takePhoto} />
              </View>

            </View>

            <View style={[cs.flex_row_center, cs.mb10]}>

              {st.error !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              }

              {st.success !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              }

            </View>

            <View style={cs.m20} />

          </View>

        </ScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageCamera.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageCamera.defaultProps = {
  user: null,
};

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    height: deviceWidth -40,
    width: deviceWidth - 40,
    borderRadius: (deviceWidth - 40)/ 2,
    overflow : 'hidden'
  },
});
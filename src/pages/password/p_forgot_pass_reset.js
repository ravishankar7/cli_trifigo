// react
import React from 'react';
import PropTypes from 'prop-types';

import { Image, View, Text, TextInput, TouchableOpacity } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import DarkGradient from '../../elements/dark_gradient';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import OkBox from '../../elements/ok_box';

// styles
import cs from '../../styles/common_styles.js';
import { colors, deviceWidth } from '../../styles/variables';

// redux
import store from '../../redux/store';

// api
import { reset_password } from '../../modules/api';

export default class PageForgotPassReset extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      userId: store.getState().userId,

      password: '',
      confirmpass: '',

      err_password: null,
      err_confirmpass: null,

      success: null,
      error: null,
      loading: ''
    };

    trackScreenView('ForgotPassReset');
  }

  backClick = () => {
    this.props.navigation.navigate('login');
  }

  submitForm = () => {
    let _this = this;
    let st = this.state;

    this.setState({ error: null, loading: ' is-loading' });

    this.resetErrors();

    let errors = this.validateError();

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    let user = {
      id: st.userId,
      password: st.password
    };

    reset_password(user).then(() => {
      _this.setState({ success: 'Password Reset' }, () => {
        _this.timer = setInterval(() => {
          const { navigate } = this.props.navigation;
          clearInterval(_this.timer);
          navigate('login');
        }, 2000);
      });
    });
  }

  validateError = () => {
    let errors = 0;
    let st = this.state;
    let err = null;

    //Validate Password
    if (!st.password) {
      errors++;
      this.setState({ err_password: 'Password is required' });
      if (err === null) err = 'Password is required';
    }

    if (!st.confirmpass) {
      errors++;
      this.setState({ err_confirmpass: 'Confirm password is required' });
      if (err === null) err = 'Confirm password is required';
    }
    if (errors === 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    if(st.password !== st.confirmpass) {
      this.setState({ err: 'Passwords dont match' });
      if (err === null) err = 'Passwords dont match';
    }

    return errors;
  }

  resetErrors = () => {
    this.setState({
      err_password: null,
      err_confirmpass: null,
      error: null
    });
  }

  render() {
    let st = this.state;
    
    return (
      <View style={cs.flex_col_center}>

        <DarkGradient />

        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='always'>

          <View style={[cs.formbox]}>

            <View style={cs.mt30} />

            <View style={[cs.flex_row_start, cs.mb10]}>
              <TouchableOpacity onPress={this.backClick} style={{ flex: 1 }}>
                <View style={[{ marginTop: 8 }]}>
                  <Image
                    source={require('../../../images/menu-back.png')}
                    style={{ height: 30 }}
                  />
                </View>
              </TouchableOpacity>

              <Text style={[cs.headertext, { flex: 3 }]}>
                New Password
              </Text>
            </View>

            <View style={ cs.bluecontainer}>
              <Text style={cs.subtitle2white}>
                Set your new password
              </Text>
            </View>

            <View style={cs.mt20} />

            <View style={st.err_password !== null ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Password'
                style={cs.textInputDark}
                onChangeText={(text) => this.setState({ password: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(255,255,255,0.7)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            <View style={st.err_confirmpass !== null ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Confirm Password'
                style={cs.textInputDark}
                onChangeText={(text) => this.setState({ confirmpass: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(255,255,255,0.7)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            <View style={[cs.buttonBox, { marginBottom: 8 }]}>

              <Button
                text='Next'
                width={deviceWidth - 55}
                height={44}
                fontSize={18}
                backgroundColor={colors.green}
                onClick={this.submitForm} />

            </View>

            <ErrorBox text={st.error} />

            {st.success !== null &&
              <View style={[cs.flex_center, cs.mt10]}>
                <OkBox text={st.success} />
              </View>
            }

          </View>

        </KeyboardAwareScrollView>

      </View>
    );
  }
}

PageForgotPassReset.propTypes = {
  navigation: PropTypes.object.isRequired
};
//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import OkBox from '../../elements/ok_box';

// store
import store from '../../redux/store';

// api
import { updateUser } from '../../modules/api';

// styles
import cs from '../../styles/common_styles';
import AutoCompleteSelectLight from '../../elements/autocomplete_select_light';

//loader
import Spinner from 'react-native-spinkit';
import { colors } from '../../styles/variables';

export default class PageSettingsDOB extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;

    let userDob = _user ? _user.dob : null
    let dobLength = userDob ?  userDob.split('-').length : null
    this.state = {
      user: _user,

      unsubscribe: store.subscribe(this.updateState),

      month: userDob ? dobLength == 3 ? userDob.split('-')[1] : null : null,
      err_month: null,

      year: userDob ? dobLength == 3 ? userDob.split('-')[0] : null : null,
      err_year: null,

      date: userDob ? dobLength == 3 ? userDob.split('-')[2] : null : null,
      err_date: null,

      error: null,
      loading: null,
      success: null,

       dobYear: userDob ? dobLength == 3 ? userDob.split('-')[0] : null : null,

       dobMonth: userDob ? dobLength == 3 ? userDob.split('-')[1] : null : null,

       dobDate: userDob ? dobLength == 3 ? userDob.split('-')[2] : null : null
    };

    trackScreenView('Settings Date of Birth');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,

      month: _user !== null ? _user.month : null,
      err_month: null,

      year: _user !== null ? _user.year : null,
      err_year: null,

      date: _user !== null ? _user.date : null,
      err_date: null,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  resetErrors = () => {
    this.setState({
      loading: null,
      error: null,
      success: null,

      err_month: null,
      err_year: null,
      err_date: null,

    });
  }

  validateData = () => {

    this.resetErrors();

    let errors = 0;
    let err = null;

    let st = this.state;

    // Validate name
    if (st.month === null || st.month === undefined || st.month == '') {
      errors++;
      this.setState({ err_month: 'Month required' });
      if (err === null) err = 'Month required';
    }

    // Validate last name
    if (st.year === null || st.year === undefined || st.year == '') {
      errors++;
      this.setState({ err_year: 'Year required' });
      if (err === null) err = 'Year required';
    }

    if (st.date === null || st.date === undefined || st.date == '') {
      errors++;
      this.setState({ err_date: 'Date required' });
      if (err === null) err = 'Date required';
    }
    
    if (errors == 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  saveData = () => {

    // save data
    let _this = this;
    let st = this.state;



    let errors = this.validateData();

    if (errors > 0) {
      this.setState({
        loading: null
      });
      return;
    }

    this.setState({
      loading: 'loading'
    })
    

    let userId = this.state.user.id;
    let user = this.state.user;
    
    // console.log('state state component >>>', st.dobDate, st.dobMonth,st.dobYear)
    // console.log("date component >>>", st.month, st.date, st.selected_year ? st.selected_year.name : "undefined")

    var final_Dob = st.year + '-' + st.month + '-' + st.date;

    // console.log('not json >>>' ,final_Dob);

    // user.dob = JSON.stringify(final_Dob);
    user.dob = final_Dob;
    // console.log('stringify >>>',JSON.stringify(final_Dob));
    try {
      updateUser(userId, user);
      _this.setState({ success: 'Saved' }, () => {
        _this.timer = setInterval(() => {
          const { navigate } = this.props.navigation;
          clearInterval(_this.timer);
          navigate('settings');
        }, 1000);
      });
    }
    catch (e) { _this.setState({ error: e }); }
  }

  cancel = () => {
    this.props.navigation.navigate('settings');
  }

  selectYear = (data) => {
    // console.log('year selected >>>', data ) 
    if(data) this.setState({ year: data.name });
  }
  
  changeYear = (data) => {
    // console.log('year changeYear', data)
    if(data) this.setState({ year: null, selected_year: null });
  }
  
  selectMonth = (data) => {
    // console.log('year selectMonth', data)
    if(data) this.setState({ month: data.name });
  }

  changeMonth = (data) => {
    // console.log('year changeMonth', data)
    if(data) this.setState({ month: null, selected_month: null });
  }

  selectDate = (data) => {
    // console.log('year selectDate', data)
    if (data)this.setState({ date: data.name });
  }

  changeDate = (data) => {
    // console.log('year changeDate', data)
    if (data) this.setState({ date: null, selected_date: null });
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    let current_year = (new Date()).getFullYear();

    let years = [];

    for (let i = -100; i < 1; i++) {
      years.push({ id: i, name: String(current_year + i) });
    }

    let months = [];
    for (let i = 1; i < 13; i++) {
      months.push({ id: i, name: String(i) });
    }

    let dates = [];
    for (let i = 1; i < 32; i++) {
      dates.push({ id: i, name: String(i) });
    }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Settings'
          back='settings'
        />

        <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center, cs.mt30]}>

            <Text style={[cs.headertextdarkgray, cs.mb10]}>Date of Birth</Text>

            <AutoCompleteSelectLight
              value = {st.dobYear}
              data={years}
              onSelect={this.selectYear}
              onChange={this.changeYear}
              placeHolder='Select Year'
              icon='calendar'
            />

            <AutoCompleteSelectLight
              value = {st.dobMonth}
              data={months}
              onSelect={this.selectMonth}
              onChange={this.changeMonth}
              placeHolder='Select Month'
              icon='calendar'
            />


            <AutoCompleteSelectLight
              value = {st.dobDate}
              data={dates}
              onSelect={this.selectDate}
              onChange={this.changeDate}
              placeHolder='Select Date'
              icon='calendar'
            />

            { this.state.loading

            ?

            <Spinner
                  isVisible
                  style = {cs.mt2}
                  size={60}
                  type='ThreeBounce'
                  color={colors.green} />

            : 
            <View style={[cs.flex_row_center, cs.mb10]}>

              <View style={cs.m5}>
                <Button
                  text='Cancel'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#CFD8DC'
                  textColor='#404040'
                  onClick={this.cancel} />
              </View>

              <View style={cs.m5}>
                <Button
                  text='Save'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#66BB6A'
                  onClick={this.saveData} />
              </View>

            </View>}

            <View style={[cs.flex_row_center, cs.mb10]}>

              {st.error !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              }

              {st.success !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              }

            </View>

          </View>

        </KeyboardAwareScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsDOB.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettingsDOB.defaultProps = {
  user: null,
};

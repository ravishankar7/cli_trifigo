import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Modal } from 'react-native';

import { deviceHeight } from '../styles/variables';

export default class PrimeModal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Modal
        transparent
        animationType={"fade"}
        visible={this.props.modalVisible}
        onShow={this.props.onModalShow}
        onDismiss={this.props.onDismissProp}
        onRequestClose={this.props.onRequestClose}>
        <View style={styles.container}>
          <View style={styles.innerContainer}>
            {this.props.children}
          </View>
        </View>
      </Modal>
    );
  }
}

PrimeModal.propTypes = {
  children: PropTypes.object.isRequired,
  onRequestClose: PropTypes.func,
  modalVisible: PropTypes.bool
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    height: deviceHeight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    alignItems: 'center',
  },
});
//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, TouchableOpacity, View, FlatList, TextInput } from 'react-native';

import Slider from "react-native-slider";


import { getUserGoal, updateGoal } from '../../modules/api';
// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';

// store
import store from '../../redux/store';

// styles
import cs from '../../styles/common_styles';
import { colors, deviceWidth } from '../../styles/variables';
import Icon from 'react-native-vector-icons/Ionicons';

export default class GoalsDetails extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let goals = store.getState().this_user_goals;

    if (goals && goals.length > 0) {
      goals.forEach(goal => {
        switch (goal.goalType) {
          case 'Car': goal.icon = 'md-car'; break;
          case 'House': goal.icon = 'md-home'; break;
          case 'Loan': goal.icon = 'md-cash'; break;
          case 'Custom': goal.icon = 'ios-cog'; break;
          case 'Stocks': goal.icon = 'md-trending-up'; break;
          case 'College': goal.icon = 'md-school'; break;
          case 'Credit Card': goal.icon = 'ios-card'; break;
          default: break;
        }
      });
    }

    this.state = {
      user: store.getState().user,
      my_goals: goals,

      custom: false,
      selectedGoals: [
        { name: 'Loan', amount: '$230,092', time: '2.5 yrs', icon: 'md-cash' },
        { name: 'House', amount: '$320,420', time: '3.5 yrs', icon: 'md-home' },
        { name: 'Car', amount: '$100,000', time: '1.2 yrs', icon: 'md-car' }
      ],
      customGoal: [{ amount: '$320,420', time: '3.5 yrs', icon: 'ios.cog' }],
      error: null,
      loading: null,
    };

    trackScreenView('GoalsDetails');
  }

  updateState = () => {
    let goals = store.getState().this_user_goals;
    if(goals && goals.length > 0) {
      goals.forEach(goal => {
        switch (goal.goalType) {
          case 'Car': goal.icon = 'md-car'; break;
          case 'House': goal.icon = 'md-home'; break;
          case 'Loan': goal.icon = 'md-cash'; break;
          case 'Custom': goal.icon = 'ios-cog'; break;
          case 'Stocks': goal.icon = 'md-trending-up'; break;
          case 'College': goal.icon = 'md-school'; break;
          case 'Credit Card': goal.icon = 'ios-card'; break;
          default: break;
        }
      });
    }
    
    this.setState({
      user: store.getState().user,
      my_goals: goals,
    });
  }

  componentDidMount() {
    this.setState({ selectedGoals: this.state.my_goals })
  }

  renderMenuHeader = (title) => {
    return (
      <View style={cs.flex_row_space_between}>
        <Text style={[cs.title1black, cs.m5]}>{title}</Text>
      </View>
    );
  }

  changeAmount(val, name) {
    const data = [...this.state.selectedGoals];
    const index = data.findIndex(el => el.goalType === name);
    data[index] = { ...data[index], targetAmount: val };
    this.setState({ selectedGoals: data })
  }

  changeMonths(val, name) {
    //console.log(val,'and ',name)
    const data = [...this.state.selectedGoals];
    const index = data.findIndex(el => el.goalType === name);
    data[index] = { ...data[index], targetMonths: val };
    this.setState({ selectedGoals: data })
  }

  async postGoals() {
    var post = this.state.selectedGoals
    for (let i = 0; i < post.length; i++) {
      await updateGoal(post[i].id, { targetAmount: post[i].targetAmount, targetMonths: post[i].targetMonths });
    }

    getUserGoal(this.state.user.id);
    this.props.navigation.navigate('goals_progress');
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          back='goals_select'
          title='Set Goals'
          navigate={navigate}
        />

        { /*** Identity ***/}
        <View style={{ flex: 1, marginHorizontal: 6, marginTop: 10, marginBottom: 6/* backgroundColor:'#ff9478' */ }}>
          {this.state.custom ?
            <FlatList
              style={{ alignSelf: 'center', width: deviceWidth, height: '100%' }}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              data={this.state.selectedGoals}
              renderItem={({ item }) => (
                <View style={{ marginTop: 15, width: deviceWidth - 55, paddingVertical: 29,   paddingHorizontal: 19, backgroundColor: 'white', borderRadius: 10, alignItems: 'center' }}>

                  <View style={{ width: 60, height: 60, paddingVertical: 2, borderRadius: 30, borderColor: colors.seagreen, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
                    <Icon name={item.icon} color={colors.seagreen} size={55} />
                  </View>

                  <Text style={{ color: '#2e3131', marginVertical: 3 }}>{item.name}</Text>

                  <View style={{ width: '88%', marginVertical: 12 }}>
                    <View style={{ flexDirection: 'row', width: '100%' }}>
                      <View style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={cs.title1darkgray}>Amount</Text>
                      </View>
                      <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Text style={{ padding: 4, fontWeight: 'bold', borderRadius: 4, color: 'white', backgroundColor: colors.seagreen }}>${this.state.amount}</Text>
                      </View>
                    </View>
                    <Slider
                      style={{ width: '100%', }}
                      step={100}
                      minimumValue={0}
                      maximumValue={100000}
                      value={this.state.amount}
                      onValueChange={val => this.setState({ amount: val })}
                      minimumTrackTintColor='#e8ecf1'
                      maximumTrackTintColor='#e8ecf1'
                      thumbStyle={{ borderWidth: 0.7, borderColor: '#e8ecf1' }}
                      thumbTintColor='white'
                      trackStyle={{ height: 2 }}
                      thumbTouchSize={{ width: 50, height: 50 }}
                    //onSlidingComplete={ val => console.log({complete:val})}
                    />
                    <View style={{ width: '100%', flexDirection: 'row', marginVertical: 0 }}>
                      <View style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={{ color: '#2e3131', }}>
                          0
                        </Text>
                      </View>

                      <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Text style={{ color: '#2e3131', }}>
                          1,000,000
                        </Text>
                      </View>

                    </View>
                  </View>

                  <View style={{ width: '88%', marginVertical: 12 }}>
                    <View style={{ flexDirection: 'row', width: '100%' }}>
                      <View style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={cs.title1darkgray}>Time</Text>
                      </View>
                      <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Text style={{ padding: 4, fontWeight: 'bold', borderRadius: 4, color: 'white', backgroundColor: colors.seagreen }}>{this.state.time} months</Text>
                      </View>
                    </View>
                    <Slider
                      style={{ width: '100%', }}
                      step={0.5}
                      minimumValue={0}
                      maximumValue={5}
                      value={this.state.time}
                      onValueChange={val => this.setState({ time: val })}
                      minimumTrackTintColor='#e8ecf1'
                      maximumTrackTintColor='#e8ecf1'
                      thumbStyle={{ borderWidth: 0.7, borderColor: '#e8ecf1' }}
                      thumbTintColor='white'
                      trackStyle={{ height: 2 }}
                      thumbTouchSize={{ width: 46, height: 46 }}
                    //onSlidingComplete={ val => console.log({complete:val})}
                    />
                    <View style={{ width: '100%', flexDirection: 'row', marginVertical: 0 }}>
                      <View style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={{ color: '#2e3131', }}>
                          0
                        </Text>
                      </View>

                      <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'flex-start', borderColor: colors.seagreen, borderWidth: 1 }}>

                        <View style={{ width: '70%', }}>
                          <TextInput

                            style={{ textAlign: 'center', color: 'grey', }}
                            selectionColor={'#3d1767'}

                            multiline={false}
                            placeholder="Time"
                            autoCapitalize="words"

                            keyboardType="number-pad"
                            autoCorrect={false}
                            returnKeyType="done"

                            underlineColorAndroid="transparent"
                            placeholderTextColor="#d2d7d3"
                          />
                        </View>
                      </View>

                    </View>
                  </View>
                </View>
              )} />
            :
            <FlatList
              style={{ alignSelf: 'center', width: '100%', height: '100%' }}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              data={this.state.selectedGoals}
              //contentContainerStyle={{alignItems:'center'}}
              renderItem={({ item, index }) => (
                <View key={index} style={{ marginTop: 15, width: deviceWidth, paddingVertical: 29, paddingHorizontal: 19, backgroundColor: 'white', borderRadius: 10, alignItems: 'center' }}>

                  <View style={{ width: 60, height: 60, paddingVertical: 2, borderRadius: 30, borderColor: colors.seagreen, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
                    <Icon name={item.icon} color={colors.seagreen} size={55} />
                  </View>

                  <Text style={{ color: '#2e3131', marginVertical: 3 }}>{item.goalType}</Text>

                  <View style={{ width: '88%', marginVertical: 12 }}>
                    <View style={{ flexDirection: 'row', width: '100%' }}>
                      <View style={{ flex: 0.6, alignItems: 'flex-start' }}>
                        <Text style={cs.title1darkgray}>Amount</Text>
                      </View>
                      <View style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={{ padding: 4, fontWeight: 'bold', borderRadius: 4, color: 'white', backgroundColor: colors.seagreen }}>$ {item.targetAmount}</Text>
                      </View>
                    </View>

                    <Slider
                      style={{ width: '100%', }}
                      step={100}
                      minimumValue={0}
                      maximumValue={100000}
                      value={item.targetAmount}
                      onValueChange={val => this.setState({ amount: val })}
                      minimumTrackTintColor='#e8ecf1'
                      maximumTrackTintColor='#e8ecf1'
                      thumbStyle={{ borderWidth: 0.7, borderColor: '#e8ecf1' }}
                      thumbTintColor='white'
                      trackStyle={{ height: 2 }}
                      thumbTouchSize={{ width: 50, height: 50 }}
                      onSlidingComplete={val => this.changeAmount(val, item.goalType)}
                    />

                    <View style={{ width: '100%', flexDirection: 'row', marginVertical: 0 }}>
                      <View style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={{ color: '#2e3131', }}>
                          0
                        </Text>
                      </View>

                      <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Text style={{ color: '#2e3131', }}>
                          1,000,000
                        </Text>
                      </View>

                    </View>
                  </View>

                  <View style={{ width: '88%', marginVertical: 12 }}>
                    <View style={{ flexDirection: 'row', width: '100%' }}>
                      <View style={{ flex: 0.6, alignItems: 'flex-start' }}>
                        <Text style={cs.title1darkgray}>Time</Text>
                      </View>
                      <View style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={{ padding: 4, fontWeight: 'bold', borderRadius: 4, color: 'white', backgroundColor: colors.seagreen }}>{item.targetMonths} months</Text>
                      </View>
                    </View>

                    <Slider
                      style={{ width: '100%', }}
                      step={0.5}
                      minimumValue={0}
                      maximumValue={60}
                      value={item.targetMonths}
                      onValueChange={val => this.setState({ time: val })}
                      minimumTrackTintColor='#e8ecf1'
                      maximumTrackTintColor='#e8ecf1'
                      thumbStyle={{ borderWidth: 0.7, borderColor: '#e8ecf1' }}
                      thumbTintColor='white'
                      trackStyle={{ height: 2 }}
                      thumbTouchSize={{ width: 46, height: 46 }}
                      onSlidingComplete={val => this.changeMonths(val, item.goalType)}
                    />

                    <View style={{ width: '100%', flexDirection: 'row', marginVertical: 0 }}>
                      <View style={{ flex: 1, alignItems: 'flex-start' }}>
                        <Text style={{ color: '#2e3131', }}>
                          0
                        </Text>
                      </View>

                      <View style={{ flex: 1, alignItems: 'flex-end' }}>
                        <Text style={{ color: '#2e3131', }}>
                          5+
                        </Text>
                      </View>

                    </View>
                  </View>
                </View>
              )} />
          }

          <TouchableOpacity onPress={() =>/* this.props.navigation.navigate('goals_progress') */ this.postGoals()} activeOpacity={0.9} style={{ bottom: 0, marginBottom: 4, marginVertical: 7, width: '100%', paddingVertical: 13, borderRadius: 29, alignSelf: 'center', borderColor: colors.seagreen, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.seagreen, marginHorizontal: 10 }}>
            <Text style={{ color: 'white', fontSize: 17 }}>NEXT</Text>
          </TouchableOpacity>
        </View>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

GoalsDetails.propTypes = {
  navigation: PropTypes.object.isRequired
};

GoalsDetails.defaultProps = {
  user: null,
};
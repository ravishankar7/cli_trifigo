// react
import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet, Image } from 'react-native';

// api
import { getRandomCreditTip } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import common_styles from '../styles/common_styles';
import Button from '../elements/button';
import { colors, deviceWidth } from '../styles/variables';

export default class CreditSimulator extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      creditTip: '',

      error: null,
      loading: null,
    };
  }

    render() {
      let st = this.state;

      return (
          <View style={[cs.shadowBox]} >
          <View style={[styles.oval2, cs.flex_center, cs.shadow, { flexDirection: 'row' , padding: 0 } ]} >
              <View style={{ flex: 1 }} >
                  <View style={{ flexDirection: 'column', alignItems:'center' }} >
                  <View style={[ cs.p10 ,{ borderWidth: 0, borderColor: colors.lightGrey, borderRadius: 5}]}>
                      <Text style={{ color: colors.black, marginBottom: 5, textAlign: 'center', alignItems: 'center' }} >
                          {'600'}
                      </Text>
                      <Text style={{ color: colors.textGrey }} >
                          {'Current Score'}
                      </Text>
                  </View>
                  </View>
              </View>
              <View style={{ flex: 1 }} >
                  <View style={{ flexDirection: 'column', alignItems:'center' }} >
                  <View style={[ cs.p10 ,{ borderWidth: 0, borderColor: colors.lightGrey, borderRadius: 5}]}>
                  <Text style={{ color: colors.green, marginBottom: 5, textAlign: 'center', alignItems: 'center' }} >
                          {'?'}
                      </Text>
                      <Text style={{ color: colors.textGrey }} >
                          {'Simulated Score'}
                      </Text>
                      </View>
                  </View>
              </View>

          </View>
                
                <View style={[cs.flex_center, { marginVertical: 10 }]}>
                    <Button
                    text='Set Credit Breakdown'
                    width={200}
                    height={30}
                    fontSize={13}
                    backgroundColor={colors.greyButton}
                    />
                </View>

          </View>
         
      );
    }
  }

  const styles = StyleSheet.create({
    gradeView: {
        flex: 1,
        backgroundColor: colors.subtleGrey,
        height: 30,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderRadius: 10,
        marginVertical: 10,
    },
    gradeText: {
        fontSize: 20,
        letterSpacing: 2,
        fontWeight: 'bold',
        color: colors.textGrey
    },
    oval2: {
        width: deviceWidth * 90 / 100,
        flex: 1,
        borderRadius: 10,
    },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    }
})

CreditSimulator.propTypes = {
    navigate: PropTypes.func.isRequired
};
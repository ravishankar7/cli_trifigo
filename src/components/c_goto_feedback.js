// react
import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';

// elements
import Button from '../elements/button';

// store
import store from '../redux/store';

// style
import cs from '../styles/common_styles.js';

export default class GotoFeedback extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    return (
      <View style={[cs.shadowBox, cs.flex_col_center]}>
        <Text style={[cs.title1]}>
          Give us feedback
        </Text>
        <View style={styles.buttoncontainer}>
          <Button
            onClick={() => this.props.navigate('feedback', {
              navigateTo: this.props.navigateTo,
            })}
            text='App Feedback'
            width={150}
            height={30}
            fontSize={13}
            backgroundColor='#546E7A'
          />
        </View>
      </View>
    );
  }
}

GotoFeedback.propTypes = {
  navigate: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
  buttoncontainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingBottom: 10
  },
});
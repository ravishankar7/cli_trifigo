import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  WebView,
  ScrollView,
  Dimensions
} from 'react-native';
import HTML from 'react-native-render-html';

// google analytics
import { trackScreenView } from '../modules/ga';

// external
import Spinner from 'react-native-spinkit';

import Header from '../elements/header';
import Footer from '../elements/footer';
//import WebViewX from '../elements/webviewx';

import store from '../redux/store';

import Route from '../modules/route.js';
import { DevUrl, LiveUrl } from '../modules/api';
// const route = new Route('https://trifigoapi.azurewebsites.net/');
const route = new Route(LiveUrl);

import cs from '../styles/common_styles.js';
import { colors, deviceWidth, deviceHeight, fontFamily } from '../styles/variables';


export default class PageLinkAcct extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      quovo_user: null,
      unsubscribe: store.subscribe(this.updateState),

      fltoken: null,
      ctoken: null,

      error: null,
      loading: 'loading',
    };

    this.getOrRegisterYodleeUser(store.getState().user.id);
    // this.getOrRegisterQuovoUser(store.getState().user.id);
    //this.getFastLinkToken(store.getState().user.id);

    trackScreenView('Link Accounts');
  }

  getOrRegisterYodleeUser = (id) => {
    let _this = this;
    route.post('yodlee/registeruser/' + id).then(function (res, err) {
      if (res !== undefined && res !== null) {
        //console.log('res >>>', JSON.stringify(res, null, 2));
        _this.getFastLinkToken(res.id);
      }
      else if (err !== undefined) {
        _this.setState({
          error: 'Error',
          loading: null
        });
      }
    });
  }

  getOrRegisterQuovoUser = (id) => {
    //TODO:- change static userid to {id}
    let _this = this;
    route.post('quovo/registeruser/' + id).then(function (res, err) {
      if (res !== undefined && res !== null) {
        // console.log('res >>>', JSON.stringify(res, null, 2));
        _this.getConnectToken(res.id);
      }
      else if (err !== undefined) {
        _this.setState({
          error: 'Error',
          loading: null
        });
      }
    });
  }

  getConnectToken = (id) => {
    //TODO:- change static userid to {id}
    let _this = this;
    route.post('quovo/connect/' + id).then(function (res, err) {
      setTimeout(function () { _this.setState({loading : null }) }, 5000);
      if (res) {
        _this.setState({ ctoken: res });
      }
      if (err) {
        _this.setState({
          error: 'Error getting token',
        });
      }
    });
  }
  
  getFastLinkToken = (id) => {
    let _this = this;

    //console.log('id >>>', id);
    route.post('yodlee/fastlink/' + id).then(function (res, err) {
      //console.log('err >>>', err);
      console.log('fl token >>>', JSON.stringify(res, null, 2));

      if (res !== undefined) {
        _this.setState({ fltoken: res, loading: null });
      }
      if (err !== undefined) {
        _this.setState({
          error: 'Error getting token',
          loading: null
        });
      }
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  updateState = () => {
    this.setState({
      user: store.getState().user
    });

    this.getFastLinkToken(store.getState().user.userId);
  }

  /*
  _onNavigationStateChange(webViewState) {
    //console.log(webViewState);
    //console.log(this.myWebView.source);
  }

  _onMessage = (e) => {
    //console.log('on message >>>', e);
  }
  */

  render() {
    const htmlContent = `
    <h1>This HTML snippet is now rendered with native components !</h1>
    <h2>Enjoy a webview-free and blazing fast application</h2>
    <img src="https://i.imgur.com/dHLmxfO.jpg?2" />
    <em style="textAlign: center;">Look at how happy this native cat is</em>
`;

    let st = this.state;
    const { navigate } = this.props.navigation;

    let token = null;

    let html_body = "<html><body><h1>* * * LOADING * * *</h1></html>";
    let url = 'https://trifigo.com/connect.html';

    
    if (st.fltoken && st.fltoken.user && st.fltoken.user.accessTokens) {
      token = st.fltoken.user.accessTokens[0];
      let _url = 'https://usyinode.stage.yodleeinteractive.com/authenticate/private-usyistage12/?channelAppName=usyichnnel';
      token.url;

      html_body = "<html><body><h1>* * * LOADING * * * *</h1><form id='fl' action='" + _url + "' method='POST'><input type='hidden' name='app' value='" + token.appId + "' /><input type='hidden' name='rsession' value='" + token.session + "'/><input type='hidden' name='token' value='" + token.value + "'/><input type='hidden' name='redirectReq' value='true'/><input type='submit' name='btn_submit' style='visibility:hidden' value='Open Fastlink'/></form></body><script language='javascript'>document.forms[0].submit();</script></html>";
      console.log('html >>>', html_body)
    }
    

    if(st.loading) {
      return (
        <View style={cs.normalPage}>

          <Header
            // menu
            back='banking_profile'
            title='Link Accounts'
            navigate={navigate}
          />
          <View style={cs.flex_center}>

            <Spinner
              isVisible
              size={120}
              type='ThreeBounce'
              color={colors.green} />

            <Text
              style={styles.signuptextlink}>
              loading
            </Text>
          </View>

          <Footer navigate={navigate} />

        </View>
      );
    }

    if (st.ctoken) {
      //console.log('connect url >>>', 'https://trifigo.com/connect.html?token=' + st.ctoken.token);
      url = 'https://trifigo.com/connect.html?token=' + st.ctoken.token;
    }
    
    let scaleFit = false;
    
    if(Platform.OS === 'ios')
      scaleFit = false;
    else
      scaleFit = true;

    //console.log('web url >>>', url);
    
    return (
      <View style={cs.normalPage}>

        <Header
          menu
          title='Link Accounts'
          navigate={navigate}
        />

        {/* <WebView
          //ref={webview => { this.myWebView = webview; }}
          //onMessage={(e) => this._onMessage(e)}
          style={styles.webview}
          source={{ uri: url }}
          javaScriptEnabled
          domStorageEnabled
          scalesPageToFit={scaleFit}
        /> */}
            <ScrollView style={{ flex: 1 }}>
                <HTML html={html_body} imagesMaxWidth={Dimensions.get('window').width} />
            </ScrollView>
        {/* <Footer navigate={navigate} /> */}

      </View>
    );
  }
}

PageLinkAcct.propTypes = {
  navigation: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  signuptextlink: {
    letterSpacing: 3,
    color: colors.green,
    fontFamily: fontFamily.medium
  },
  webview:
    {
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      width: deviceWidth,
      height: deviceHeight
    },
});

import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet } from 'react-native';

import cs from '../styles/common_styles.js';

export default class Account extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let pr = this.props;
    let account = pr.account;

    //console.log('account >>>', pr.account);

    let _name = account.institution_name;

    // If name is > 25 then break at a good boundary
    if (_name && _name.length > 25) {
      let _newname = '';
      let _arr = _name.split(' ');

      for (let i = 0; i < _arr.length; i++) {
        const e = _arr[i];
        if ((_newname + ' ' + e).length > 25) {
          break;
        }
        _newname += ' ' + e;
      }
      _name = _newname;
    }

    return (
      <View>

        <View style={cs.flex_row_space_between}>

          <View>
            <Text style={cs.title2}>{_name}</Text>
            <Text style={[cs.subtitle2gray, cs.mt0, cs.mb10]}>
              {account.nickname}
            </Text>
          </View>

          <View style={cs.flex_row_space_between}>

            <View>
              <Text style={cs.title3gray}>${account.value}</Text>
            </View>

            {/*
            <View style={[cs.mt10, cs.w10]}>
              <TouchableOpacity onPress={() => this.props.navigate('transactions')}>
                <Image
                  style={[cs.mt10, cs.mr5]}
                  source={require('../../images/chevron-right.png')}
                />
              </TouchableOpacity>
            </View>
            */}
            
          </View>

        </View>

        <View
          style={{
            borderBottomColor: '#202020',
            borderBottomWidth: StyleSheet.hairlineWidth,
            marginBottom: 5,
          }}
        />
      </View>
    );
  }
}

Account.propTypes = {
  navigate: PropTypes.func.isRequired
};
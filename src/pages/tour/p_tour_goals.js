// react
import React from 'react';
import PropTypes from 'prop-types';

import { View, Image, Text, TouchableOpacity } from 'react-native';

// elements
import DarkGradient from '../../elements/dark_gradient';
import Button from '../../elements/button';

// styles
import cs from '../../styles/common_styles.js';
import { colors } from '../../styles/variables';

export default class PageTourGoals extends React.Component {

	static navigationOptions = {
		header: null,
	};

	render = () => {
		return (
			<View style={[cs.flex_col_center_top]}>

				<DarkGradient />

				<View style={cs.mt30} />

				<View style={[cs.flex_row_start, cs.m10]}>
					<TouchableOpacity onPress={() => this.props.navigation.navigate('tour_credit_behavior')} style={{ flex: 1 }}>
						<View style={[{ marginTop: 8 }]}>
							<Image
								source={require('../../../images/menu-back.png')}
								style={{ height: 40 }}
							/>
						</View>
					</TouchableOpacity>
					<Text style={{ flex: 2 }}></Text>
				</View>

				<View style={cs.mt30} />

				<View style={[cs.roundimage]}>
					<Image
						source={require('../../../images/addpic.jpg')}
						style={[{ borderRadius: 135, width: 270, height: 270 }]}
					/>
				</View>

				<Text style={[cs.headertext, cs.m10, cs.mt10]}>
					ADD GOALS
				</Text>
				<Text style={[cs.headertext, cs.mb10]}>

				</Text>
				<Text style={[cs.title2white, cs.m5]}>
					Add your Goals
				</Text>

				<View style={[cs.flex_row_center, cs.mt100, cs.ph15]}>

					<Text style={[cs.title2white, cs.mh10, cs.mt7, { flex: 0.4 }]} onPress={() => this.props.navigation.navigate('credit_profile')}>
						Skip
					</Text>

					<View style={{ flex: 1 }}>
						<Button
							onClick={() => this.props.navigation.navigate('settings_photo')}
							text='Complete Now'
							width={160}
							height={35}
							fontSize={14}
							backgroundColor={colors.green}
						/>
					</View>

					<Text style={[cs.title2white, cs.mh10, cs.mt7, { flex: 0.25 }]} onPress={() => this.props.navigation.navigate('banking_profile')}>
						Next
					</Text>
				</View>

			</View>
		);
	}
}

PageTourGoals.propTypes = {
	navigation: PropTypes.object.isRequired
};
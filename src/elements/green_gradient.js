import React from 'react';

import LinearGradient from 'react-native-linear-gradient';

import { deviceHeight } from '../styles/variables';

const GreenGradient = () => (
	<LinearGradient
		colors={['#17B890', '#2e8b57']}
		style={{
			position: 'absolute',
			left: 0,
			right: 0,
			top: 0,
			height: deviceHeight * 1,
		}}
	/>
);

export default GreenGradient;
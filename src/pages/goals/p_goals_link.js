//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, TouchableOpacity, View } from 'react-native';

// google analytics
import { trackScreenView } from '../../modules/ga';


// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';

// store
import store from '../../redux/store';

// styles
import cs from '../../styles/common_styles';
import { colors } from '../../styles/variables';
import Icon from 'react-native-vector-icons/Ionicons';


export default class GoalsLink extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      myGoals: store.getState().this_user_goals,

      laterLinking: false,
      error: null,
      loading: null,
    };

    trackScreenView('GoalsLink');
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
    });
  }

  renderMenuHeader = (title) => {
    return (
      <View style={cs.flex_row_space_between}>
        <Text style={[cs.title1black, cs.m5]}>{title}</Text>
      </View>
    );
  }

  checkGoals() {
    if (this.state.myGoals.length < 3) {
      this.props.navigation.navigate('goals_select')
    }
    else if (this.state.myGoals.length >= 3) {
      this.props.navigation.navigate('goals_detail')
    }
  }

  linkAccount() {
    this.props.navigation.navigate('link_account')
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Set Goals'
          back='credit_profile'
        />

        <View style={{ flex: 1, marginHorizontal: 6, marginVertical: 13, backgroundColor: 'white', paddingHorizontal: 15, borderRadius: 14 }}>

          <View style={{ width: '100%', height: '100%', flexWrap: 'wrap' }}>

            <View style={{ alignSelf: 'center', paddingVertical: 13 }}>
              {this.renderMenuHeader('Link account to complete goal setup')}
            </View>

            <View style={cs.mt20} />
            <TouchableOpacity onPress={() => this.linkAccount()} activeOpacity={0.9} style={{ marginVertical: 12, width: '100%', paddingVertical: 13, borderRadius: 29, alignSelf: 'center', borderColor: colors.seagreen, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
              <Text style={{ color: colors.seagreen, fontSize: 17 }}>Link Account</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.setState({ laterLinking: true })} activeOpacity={0.9} style={{ marginVertical: 10, width: '100%', paddingVertical: 13, borderRadius: 29, alignSelf: 'center', borderColor: colors.seagreen, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: 'white' }}>
              <Text style={{ color: colors.seagreen, fontSize: 17 }}>Link Account Later</Text>
            </TouchableOpacity>


            {this.state.laterLinking &&
              <View style={{ marginTop: 5, marginHorizontal: 15, flexDirection: 'row', alignSelf: 'center', alignItems: 'center' }}>
                <Icon name="md-warning" color='#3C5468' style={{ paddingHorizontal: 3 }} size={28} />
                <View>
                  <Text style={{ color: '#3C5468' }}>Until your account is linked your progress cannot be tracked.</Text>
                </View>
              </View>
            }

            <TouchableOpacity onPress={() => this.checkGoals()} activeOpacity={0.9} style={{ position: 'absolute', bottom: 0, marginBottom: 7, marginVertical: 12, width: '100%', paddingVertical: 13, borderRadius: 29, alignSelf: 'center', borderColor: colors.seagreen, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.seagreen }}>
              <Text style={{ color: 'white', fontSize: 17 }}>NEXT</Text>
            </TouchableOpacity>

          </View>
        </View>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

GoalsLink.propTypes = {
  navigation: PropTypes.object.isRequired
};

GoalsLink.defaultProps = {
  user: null,
};
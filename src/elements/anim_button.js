// react
import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableWithoutFeedback } from 'react-native';

import * as Animatable from 'react-native-animatable';

export default class AnimButton extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selected: false
    };
  }

  onPress = () => {
    this.props.onPress(!this.state.selected);
    this.setState({ selected: !this.state.selected });

    switch (this.props.effect) {
      case 'bounce':
        this.view.bounce(800);
        break;
      case 'flash':
        this.view.flash(800);
        break;
      case 'jello':
        this.view.jello(800);
        break;
      case 'pulse':
        this.view.pulse(800);
        break;
      case 'rotate':
        this.view.rotate(800);
        break;
      case 'rubberBand':
        this.view.rubberBand(800);
        break;
      case 'shake':
        this.view.shake(800);
        break;
      case 'swing':
        this.view.swing(800);
        break;
      case 'tada':
        this.view.tada(800);
        break;
      case 'wobble':
        this.view.wobble(800);
        break;
      default:
        this.view.pulse(800);
        break;
    }
  }

  render() {
    let selected = this.props.selected !== undefined ? this.props.selected : this.state.selected;
    return (
      <TouchableWithoutFeedback onPress={this.onPress}>
        <Animatable.View 
          ref={(c) => { this.view = c; }} 
          style={{ margin: 10, paddingTop: 10, paddingBottom: 10, paddingRight: 20, paddingLeft: 20, backgroundColor: selected ? this.props.onColor : "#efefef", borderRadius: 10 }}>
          <Text 
            style={{ color: selected ? "white" : "#696969", fontWeight: "bold" }}>
            {this.props.text}
          </Text>
        </Animatable.View>
      </TouchableWithoutFeedback>
    );
  }
}

AnimButton.propTypes = {
  onColor: PropTypes.string,
  onPress: PropTypes.func,
  effect: PropTypes.string,
  text: PropTypes.string,
  selected: PropTypes.bool,
};
// react
import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';

// elements
import RadioButton from '../elements/radio_button';
import Button from '../elements/button';

// store
import store from '../redux/store';

// api
import { updateRecoStatus, getRecos } from '../modules/api';

// styles
import cs from '../styles/common_styles';

export default class Reco extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      recoTemplates: store.getState().recoTemplates,

      //unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,

      selected: 'A'
    };
  }

  /*
  updateState = () => {
    this.setState({
      user: store.getState().user,
      recoTemplates: store.getState().recoTemplates,
    });
  }
  */

  componentWillUnmount() {
    //this.state.unsubscribe();
  }

  selectOption = (v) => {
    this.setState({ selected: v });
  }

  updateReco = () => {
    let _this = this;
    let status = this.state.selected;
    let id = this.props.reco.id;
    let userId = this.state.user.id;

    try {
      updateRecoStatus(id, status).then(function (res, err) {
        if (res) {
          getRecos(userId, 'N');
        }
        else if (err !== undefined) {
          throw err;
        }
      });
    }
    catch (e) { _this.setState({ error: e }); }
  }

  render() {
    let st = this.state;
    let reco = this.props.reco;
    let recoTemplates = this.state.recoTemplates;

    let recoTemplate = null;

    if (recoTemplates && recoTemplates.length > 0) {
      for (let i = 0; i < recoTemplates.length; i++) {
        const _template = recoTemplates[i];

        if (_template.id == reco.templateId) {
          recoTemplate = _template;
          break;
        }
      }
    }
    //console.log('recoTemplates >>>', recoTemplates);
    //console.log('recoTemplate >>>', recoTemplate);
    //console.log('reco >>>', reco);

    let text = recoTemplate !== null ? recoTemplate.actionSteps : '';

    if (text !== '' && reco.actionValues !== null && reco.actionValues.length > 0) {
      for (let x = 0; x < reco.actionValues.length; x++) {
        const v = reco.actionValues[x];
        text = text.replace(('[' + x + ']'), v);
      }
    }

    let dt = new Date(reco.recordDate);

    let adt = '';
    let act = '';

    if (reco.actionDate !== null) {
      adt = new Date(reco.actionDate);
      adt = adt.toLocaleDateString('en-US', options);

      switch (reco.status) {
        case 'A': act = 'Accepted'; break;
        case 'X': act = 'Declined'; break;
        case 'D': act = 'Deferred'; break;
      }
    }

    let options = { year: 'numeric', month: 'short', day: 'numeric' };

    return (
      <View style={cs.shadowBox}>

        <View style={[cs.flex_row_space_between, cs.mb10]}>

          {recoTemplate !== null &&
            <Text style={cs.title1}>{recoTemplate.generalReco}</Text>
          }

          <Text style={[cs.title2gray]}>
            {dt.toLocaleDateString('en-US', options)}
          </Text>

        </View>


        {text !== '' &&
          <Text style={[cs.subtitle3gray, cs.mb5]}>
            {text}
          </Text>
        }
        {reco.actionDate === null &&
          <View style={cs.mv10}>

            <View style={[cs.flex_row_start, cs.mh20, cs.mb10]}>

              <RadioButton selected={st.selected === 'A'} onClick={() => this.selectOption('A')} />

              <Text style={[cs.subtitle3gray, cs.ml5, cs.mt2]}>
                Accept
              </Text>

              <View style={cs.mh15} />

              <RadioButton selected={st.selected === 'X'} onClick={() => this.selectOption('X')} />

              <Text style={[cs.subtitle3gray, cs.ml5, cs.mt2]}>
                Decline
              </Text>

              <View style={cs.mh15} />

              <RadioButton selected={st.selected === 'D'} onClick={() => this.selectOption('D')} />

              <Text style={[cs.subtitle3gray, cs.ml5, cs.mt2]}>
                Defer
              </Text>

            </View>

            <View style={cs.flex_row_end}>
              <Button
                text='Save'
                onClick={() => this.updateReco()} />
            </View>

          </View>
        }

        {reco.actionDate !== null &&
          <View style={cs.flex_row_end}>
            <Text style={[cs.subtitle2]}>
              {act} on {adt}
            </Text>
          </View>
        }
      </View>
    );
  }
}

Reco.propTypes = {
  reco: PropTypes.object.isRequired
};

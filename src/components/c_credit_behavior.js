// react 
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View } from 'react-native';

// elements
import Button from '../elements/button';

// styles
import cs from '../styles/common_styles';

export default class CreditBehavior extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style={[cs.shadowBox, cs.flex_center]}>
        <Text style={[cs.title1, cs.m5]}>
          {'See your credit behavior'}
        </Text>

        <Button
          onClick={() => this.props.navigate('credit_behavior')}
          text='Credit Behavior'
          width={200}
          height={30}
          fontSize={13}
          backgroundColor='#546E7A'
        />

        <View style={cs.mb10} />

      </View>
    );
  }
}

CreditBehavior.propTypes = {
  navigate: PropTypes.func.isRequired
};
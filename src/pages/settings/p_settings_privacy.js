//react
import React from 'react';
import PropTypes from 'prop-types';

import { View, WebView, Platform, StyleSheet } from 'react-native';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';

// styles
import cs from '../../styles/common_styles';
import { deviceWidth, deviceHeight } from '../../styles/variables';

export default class PageSettingsPrivacy extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    trackScreenView('Settings Privacy');
  }

  render() {
    const { navigate } = this.props.navigation;
    let url = 'https://trifigo.com/privacy.html';

    let scaleFit = false;

    if (Platform.OS === 'ios')
      scaleFit = false;
    else
      scaleFit = true;

    return (
      <View style={[cs.normalPage]}>

        <Header
          navigate={navigate}
          title='Privacy'
          back='settings'
        />

        <WebView
          style={styles.webview}
          source={{ uri: url }}
          javaScriptEnabled
          domStorageEnabled
          scalesPageToFit={scaleFit}
        />

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsPrivacy.propTypes = {
  navigation: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  webview: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    width: deviceWidth,
    height: deviceHeight
  },
});

// react
import React from "react";
import PropTypes from "prop-types";
import { View, ScrollView, Text, Linking } from "react-native";

// google analytics
import { trackScreenView } from "../modules/ga";

// external
import Spinner from "react-native-spinkit";

import {
  DocumentPicker,
  DocumentPickerUtil
} from "react-native-document-picker";

// elements
import Header from "../elements/header";
import Footer from "../elements/footer";
import Button from "../elements/button";
import ErrorBox from "../elements/error_box";
import OkBox from "../elements/ok_box";

// store
import store from "../redux/store";

// api
import {
  uploadReportX,
  refreshQuovoAccounts,
  refreshQuovoTransactions,
  deleteAccountAgeSummary,
  deleteAccountMixSummary,
  deleteAdditionalCreditSummary,
  deleteBankingTotalsSummary,
  deleteCashFlowSummary,
  deleteConsumerTypeSummary,
  deleteCreditInquirySummary,
  deleteCreditUtilizationSummary,
  deleteOtherFactorsSummary,
  deletePaymentsSummary,
  runEngineForUser
} from "../modules/api";

// styles
import cs from "../styles/common_styles.js";
import { colors } from "../styles/variables";

export default class PageUploadReport extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      errcode: null,
      loading: null,
      success: null,

      document: null
    };

    trackScreenView("Upload Report");
  }

  updateState = () => {
    this.setState({
      user: store.getState().user
    });
  };

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  pickDocument = () => {
    let _this = this;

    DocumentPicker.show(
      {
        filetype: [DocumentPickerUtil.pdf()]
      },
      (error, res) => {
        if (error) {
          console.log("error >>>", error)
          _this.setState({ error : 'cancelled'})
        }
        
        if (res) {
          _this.setState({ loading: "uploading file", error: null });

          let userId = this.state.user.id;
          let uri = res.uri;

          let formData = new FormData();

          formData.append("pdf", {
            uri,
            name: res.fileName ? res.fileName : "tu_report.pdf",
            type: res.type ? res.type : "application/pdf" // mime type
          });

          try {
            uploadReportX(userId, formData).then(function(res) {
              //console.log('res>>> ', res);

              if (res.status === 200) {
                console.log("file uploaded !");

                _this.setState({ success: "Uploaded, refreshing data" }, () => {
                  _this.timer = setInterval(() => {
                    const { navigate } = _this.props.navigation;
                    clearInterval(_this.timer);
                    navigate("credit_profile");
                  }, 1500);
                });

                refreshQuovoAccounts(userId);
                refreshQuovoTransactions(userId);

                deleteAccountAgeSummary(userId);
                deleteAccountMixSummary(userId);
                deleteAdditionalCreditSummary(userId);
                deleteBankingTotalsSummary(userId);
                // deleteCashFlowSummary(userId);
                deleteConsumerTypeSummary(userId);
                deleteCreditInquirySummary(userId);
                deleteCreditUtilizationSummary(userId);
                deleteOtherFactorsSummary(userId);
                deletePaymentsSummary(userId);

                runEngineForUser(userId);
              } else {
                //console.log('file upload failed !');
                let _err = String(res.headers.map.err);

                let _errcode = null;
                let _errtext = null;
                if (_err.indexOf("ERR:") !== -1) {
                  _errcode = _err.substring(0, 8);
                  _errtext = _err.substring(8);
                }
                _this.setState({
                  error: _errtext,
                  errcode: _errcode,
                  loading: null
                });
              }
            });
          } catch (e) {
            _this.setState({ error: e });
            //console.error('file uploaded error >>>', e);
          }
        } 
      }
    );
  };

  render() {
    let _this = this;
    let st = this.state;

    const { navigate } = this.props.navigation;

    return (
      <View style={cs.normalPage}>
        <Header
          title={"Upload File"}
          navigate={navigate}
          back="credit_profile"
        />

        <ScrollView keyboardShouldPersistTaps="always">
          <View style={[cs.mt20, cs.bluecontainer]}>
            <Text style={cs.subtitle2white}>
              We need to review and analyze your credit report to give you
              tailored recommendations and ideas.
            </Text>

            <View style={cs.m10} />

            <Text style={cs.subtitle2white}>
              Visit this site to get your free credit report
            </Text>

            <View style={cs.m5} />

            <Text
              style={[cs.subtitle2green]}
              onPress={() =>
                Linking.openURL("https://www.annualcreditreport.com")
              }
            >
              https://www.annualcreditreport.com
            </Text>

            <View style={cs.m5} />

            <Text style={cs.subtitle2white}>
              and follow the instructions to download your TransUnion Report as
              a PDF file on your phone
            </Text>

            <View style={cs.m5} />

            <Text style={cs.subtitle2white}>
              Save the report and upload this report using the Upload file
              button
            </Text>
          </View>

          <View style={[cs.mt30, cs.flex_row_center]}>
            {st.loading ? (
              <Spinner
                isVisible
                size={60}
                type="ThreeBounce"
                color={colors.green}
              />
            ) : (
              <Button
                onClick={_this.pickDocument}
                text="Upload Report"
                width={250}
                height={36}
                fontSize={16}
                backgroundColor="#1ABC9C"
              />
            )}
          </View>

          {st.error && (
            <View style={[cs.flex_center, cs.mt10]}>
              <ErrorBox text={st.error} />
            </View>
          )}

          {st.success !== null && (
            <View style={[cs.flex_center, cs.mt10]}>
              <OkBox text={st.success} />
            </View>
          )}
        </ScrollView>

        <Footer navigate={navigate} />
      </View>
    );
  }
}

PageUploadReport.propTypes = {
  navigation: PropTypes.object.isRequired
};

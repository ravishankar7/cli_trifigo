import React from 'react';
import PropTypes from 'prop-types';

import { Text, View, StyleSheet } from 'react-native';

import { colors, fontSize, fontFamily } from '../styles/variables';

export default class OkBox extends React.Component {
	
	constructor(props) {
		super(props);
	}

	render = () => {
		let pr = this.props;

		if(pr.text === null || pr.text === undefined || pr.text === '') {
			return (
				<View style={[styles.emptyBox]} />
			);
		}
		return (
			<View style={[styles.okBox]}>
				<Text style={styles.okText}>
					{this.props.text}
				</Text>
			</View>
		);
	}
}

OkBox.propTypes = {
	text: PropTypes.string
};

// Define button style
const styles = StyleSheet.create({
	emptyBox: {
		alignItems: 'center',
		backgroundColor: 'transparent',
		height: 24
	},
	okBox: {
		alignItems: 'center',
		backgroundColor: '#27AE60',
		borderRadius: 6,
		height: 24,		
	},
	okText: {
		paddingVertical: 2,
		paddingHorizontal: 8,
		color: colors.white,
		fontSize: fontSize.small,
		letterSpacing: 3,
		backgroundColor: 'transparent',
		fontFamily: fontFamily.regular,
	},
});
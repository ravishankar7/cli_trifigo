// react
import React from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView } from 'react-native';

// google analytics
import { trackScreenView } from '../modules/ga';

// elements
import Header from '../elements/header';
import Footer from '../elements/footer';
import OfferNote from '../components/c_offer_note';

// store
import store from '../redux/store';

// styles
import cs from '../styles/common_styles.js';

export default class PageOffers extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      dealItems: store.getState().dealItems,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: '',
    };

    trackScreenView('Deals');
  }

  updateState = () => {
    this.setState({
      dealItems: store.getState().dealItems,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  renderDealItems = () => {
    let st = this.state;

    if(!st.dealItems) return null;

    let data = [];
    //console.log('dealItems >>>', JSON.stringify(st.dealItems, null, 2));
    st.dealItems.forEach(dealItem => {
      data.push(
        <OfferNote
          key={dealItem.id}
          logoUrl={dealItem.logoUrl}
          linkUrl={dealItem.linkUrl}
          text={dealItem.text} />
      );
    });

    return (
      <View>
        {data}
      </View>
    );
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={cs.normalPage}>

        <Header
          // menu
          back = 'banking_profile'
          title='Deal Sites'
          navigate={navigate}
        />

        <ScrollView keyboardShouldPersistTaps='always'>

          <View style={cs.m5} />
      
          {this.renderDealItems()}

        </ScrollView>

        {/* <Footer navigate={navigate} /> */}

      </View>
    );
  }
}

PageOffers.propTypes = {
  navigation: PropTypes.object.isRequired
};
// react
import React from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, Share } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

// elements
import AnimButton from '../elements/anim_button';
import Button from '../elements/button';

// store
import store from '../redux/store';
import * as actions from '../redux/actions';

// api
import { getCreditQuiz, saveCreditBossRatingSummary } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import { fontFamily, colors } from '../styles/variables';
import { SocialIcon } from 'react-native-elements';

const { width } = Dimensions.get('window');

export default class CreditQuiz extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      creditQuiz: store.getState().creditQuiz,
      creditBossRatingSummary: store.getState().creditBossRatingSummary,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,

      qno: 0,
      ans: 0,
      score: 0,
      countCheck: 0,

      selected: {}
    };
  }
  
  updateState = () => {
    this.setState({
      user: store.getState().user,
      creditQuiz: store.getState().creditQuiz,
      creditBossRatingSummary: store.getState().creditBossRatingSummary,
    });
  }

  componentDidMount() {
    let _this = this;

    if (this.state.creditQuiz === null) {
      try { getCreditQuiz(); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  next = () => {
    let _this = this;
    let st = this.state;
    let qno = st.qno;

    let score = st.score;
    console.log('score >>>', score)
    if (st.ans === 1)
      score++;

    if (st.creditQuiz === null || st.creditQuiz === undefined)
      return;

    if (qno < st.creditQuiz.length - 1) {
      qno++;
      _this.setState({ qno: qno, score: score });
    }
    else {
      try { saveCreditBossRatingSummary(st.user.id, st.score); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  answer = (label, ans) => {
    let selected = this.state.selected;
    for (var key in selected) {
      selected[key] = false;
    }

    selected[label] = true;
    this.setState({ ans: ans, selected: selected });
  }

  takeQuiz = () => {
    let _this = this;

    try { getCreditQuiz(); }
    catch (e) { _this.setState({ error: e }); }

    store.dispatch(actions.setCreditBossRatingSummary(null));

    this.setState( {
      error: null,
      loading: null,

      qno: 0,
      ans: 0,
      score: 0,
      countCheck: 0,

      selected: {}
    });
  }

  shareOnLinkedIn = () => {
    Share.share({
      message: 'BAM: we\'re helping your business with awesome React Native apps',
      url: 'http://bam.tech',
      title: 'Wow, did you see that?'
    }, {
        // Android only:
        dialogTitle: 'Share BAM goodness',
        // iOS only:
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter'
        ]
      });
  }

  shareOnFacebook = () => {

  }

  shareOnGoogle = () => {

  }

  shareOnTwitter = () => {

  }

  shareOnInstagram = () => {

  }

  render() {
    let st = this.state;

    if (st.creditBossRatingSummary !== null)
      return this.renderSummary();

    return this.renderQuiz();
  }

  renderSummary() {
    let _this = this;
    let st = this.state;

    let creditBossRatingSummary = st.creditBossRatingSummary;

    let dt = new Date(creditBossRatingSummary.recordDate);
    let options = { year: 'numeric', month: 'short', day: 'numeric' };

    let rank = '-';

    let score = st.creditBossRatingSummary.creditBossRating;

    if (score == 10) {
      rank = 'Credit Ace';
    }
    else if (score == 8 || score == 9) {
      rank = 'Credit King';
    }
    else if (score == 6 || score == 7) {
      rank = 'Credit Knight';
    }
    else if (score == 5 || score == 4) {
      rank = 'Credit Jack';
    }
    else {
      rank = 'Credit Novice';
    }

    return (
      <View style={cs.shadowBox}>

        <Text style={cs.title1}>Credit Challenge</Text>

        <View style={cs.m5} />

        <Text style={cs.subtitle2gray}>
          You took the credit challenge on {dt.toLocaleDateString('en-US', options)}
        </Text>

        <View style={cs.m5} />

        <View style={[styles.oval2, cs.flex_center, cs.shadow]} >
          <Text style={[styles.welcome3]}>
            You are a Credit {rank} !
          </Text>
          <Text style={[styles.welcome3]}>
            Your current score is {score != null ? score : 0 } / 10
          </Text>
        </View>
        
        
        <View style={cs.m5} />
        
        <Text style={[cs.subtitle2gray, cs.flex_center]}>
          Share your score and challenge your friends
        </Text>

        <View style={cs.flex_row_center}>
          <SocialIcon
            style={cs.shadowDark}
            type='linkedin'
            raised={false}
            iconSize={20}
            onPress={this.shareOnLinkedIn}
          />
          <SocialIcon
            style={cs.shadowDark}
            type='facebook'
            raised={false}
            iconSize={20}
            onPress={this.shareOnFacebook}
          />
          <SocialIcon
            style={cs.shadowDark}
            type='twitter'
            raised={false}
            iconSize={20}
            onPress={this.shareOnTwitter}
          />
          <SocialIcon
            style={cs.shadowDark}
            type='instagram'
            raised={false}
            iconSize={20}
            onPress={this.shareOnInstagram}
          />
        </View>
       

        <View style={cs.m5} />

        <View style={cs.flex_center}>
          <Button
            onClick={() => _this.takeQuiz()}
            text='Take Quiz Again'
            width={220}
            height={32}
            fontSize={13}
            backgroundColor={colors.greyButton}
          />
        </View>

        <View style={cs.m5} />

      </View>
    );
  }

  renderQuiz() {
    let _this = this;
    let st = this.state;

    let question = '';
    let answers = null;
    let displayOptions = [];

    if (st.creditQuiz && st.qno < st.creditQuiz.length) {
      question = st.creditQuiz[st.qno].question;
      answers = st.creditQuiz[st.qno].answers;

      if (answers.length > 0) {
        answers.forEach(answer => {
          displayOptions.push(
            <View key={st.qno + '--' + answer.label}>
              <AnimButton
                key={st.qno + '--' + answer.label}
                selected={st.selected[answer.label] !== undefined ? st.selected[answer.label] : false}
                onColor='#38c4a8'
                effect='pulse'
                onPress={() => _this.answer(answer.label, answer.point)}
                text={answer.label} />
            </View>
          );
        });
      }
    }

    return (
      <View style={cs.shadowBox}>

        <Text style={cs.title1}>Credit Challenge</Text>

        <View style={cs.m5} />

        <Text style={cs.subtitle2gray}>
          Take the credit challenge, see where you stack up and challenge your friends
        </Text>

        <View style={cs.m5} />

        <View style={styles.oval} >
          <Text style={styles.welcome}>
            {question}
          </Text>
        </View>

        <View style={cs.m5} />

        {displayOptions}

        <View style={cs.m5} />

        <TouchableOpacity onPress={() => this.next()} style={[cs.flex_center]}>
          <View
            style={[styles.next, cs.shadowDark]}>
            <Icon name="md-arrow-round-forward" size={24} color="white" />
          </View>
        </TouchableOpacity>

        <View style={cs.m5} />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  oval: {
    width: width * 90 / 100,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: '#38c4a8', //'#0bb795'
    height: 100
  },
  oval2: {
    width: width * 90 / 100,
    flex: 1,
    borderRadius: 10,
    backgroundColor: '#38c4a8', //'#0bb795'
    height: 100
  },
  welcome: {
    fontSize: 15,
    fontFamily: fontFamily.regular,
    margin: 15,
    color: "white"
  },
  welcome2: {
    fontSize: 16,
    fontFamily: fontFamily.regular,
    margin: 1,
    color: "white"
  },
  welcome3: {
    fontSize: 16,
    fontFamily: fontFamily.semiBold,
    marginTop: 5,
    color: "white"
  },
  next: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 180,
    height: 28,
    borderRadius: 14,
    backgroundColor: '#38c4a8'
  },
  next2: {
    alignItems: 'center',
    width: 170,
    paddingTop: 1,
    paddingBottom: 1,
    paddingRight: 20,
    paddingLeft: 20,
    borderRadius: 4,
    backgroundColor: '#38c4a8'
  }
});
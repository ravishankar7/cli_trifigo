// react
import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet, Image } from 'react-native';
//import { Button } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
// api
import { getRandomCreditTip } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import common_styles from '../styles/common_styles';
import Button from '../elements/button';
import { colors, deviceWidth } from '../styles/variables';

const credit_score = require('../../images/credit_page/cashflow.png')
const flow = require('../../images/credit_page/quiz.png')

export default class ScoreTracker extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      creditTip: '',

      error: null,
      loading: null,
    };
  }

    render() {
      let st = this.state;

      return (
          <View style={[cs.shadowBox]} >
          <View style={[styles.oval2, cs.flex_center, cs.shadow, { flexDirection: 'row' , padding: 10 } ]} >
              <View style={{ flex: 1 }} >
                  <View style={{ flexDirection: 'column', alignItems:'center' }} >
                  <View style={[ cs.p10 ,{ borderWidth: 0.5, borderColor: colors.lightGrey, borderRadius: 5}]}>
                      <Text style={{ color: colors.textGrey, marginBottom: 5, textAlign: 'center', alignItems: 'center' }} >
                          {'600'}
                      </Text>
                      <Text style={{ color: colors.textGrey }} >
                          {'Current Score'}
                      </Text>
                  </View>
                  </View>
              </View>
              <View style={{ flex: 1 }} >
                  <View style={{ flexDirection: 'column', alignItems:'center' }} >
                  <View style={[ cs.p10 ,{ borderWidth: 0.5, borderColor: colors.lightGrey, borderRadius: 5}]}>
                  <Text style={{ color: colors.green, marginBottom: 5, textAlign: 'center', alignItems: 'center' }} >
                          {'620'}
                      </Text>
                      <Text style={{ color: colors.textGrey }} >
                          {'Next Milestone'}
                      </Text>
                      </View>
                  </View>
              </View>

          </View>
          <View style={{ width: deviceWidth * 90/100, height: 20, flexDirection: 'column' , borderRadius: 5, overflow: 'hidden' }} >
          
                <LinearGradient
                    colors={[colors.red, colors.progressYellow, colors.green ]}
                    start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                    locations={[0,0.5,0.6]}
                    style={{
                        flex:1,
                        left: 0,
                        right: 0,
                        top: 0,
                        height: 20,
                    }}
                >
                </LinearGradient>

                <View style={{ width: 200, height: 20, position: 'absolute' }} />
                
        </View>
            <View  style={{ height: 10 }}/>
                <View style={[{flex:1, flexDirection: 'row', justifyContent: 'space-evenly' }]}>
                    <View style={[cs.shadowBox, { backgroundColor: colors.green, borderRadius: 10, margin: 30 } ]}>
                        <Text style={cs.title1white} >
                                {'Credit'}
                        </Text>
                        <Text style={[cs.subtitle2white, { textAlign: 'center' } ]} >
                                {'Start'}
                        </Text>
                    </View>
                    {/* //line */}
                    <View style={{ borderWidth:0.5, backgroundColor: colors.lightGrey, borderColor: colors.lightGrey }} />
                    <View style={[cs.shadowBox, { backgroundColor: colors.green, borderRadius: 10, margin: 30 } ]}>
                        <Text style={cs.title1white} >
                                {' Save '}
                        </Text>
                        <Text style={[cs.subtitle2white, { textAlign: 'center' } ]} >
                                {'$0'}
                        </Text>
                    </View>
                </View>
                
                <View  style={{ height: 10 }}/>
                
                <View style={[cs.flex_center, { marginVertical: 10 }]}>
                    <Button
                    text='Set Credit Breakdown'
                    width={200}
                    height={30}
                    fontSize={13}
                    backgroundColor={colors.greyButton}
                    />
                </View>

          </View>
         
      );
    }
  }

  const styles = StyleSheet.create({
    gradeView: {
        flex: 1,
        backgroundColor: colors.subtleGrey,
        height: 30,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderRadius: 10,
        marginVertical: 10,
    },
    gradeText: {
        fontSize: 20,
        letterSpacing: 2,
        fontWeight: 'bold',
        color: colors.textGrey
    },
    oval2: {
        width: deviceWidth * 90 / 100,
        flex: 1,
        borderRadius: 10,
    },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    }
})

ScoreTracker.propTypes = {
    navigate: PropTypes.func.isRequired
};
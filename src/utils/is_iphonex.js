import { Dimensions, Platform } from 'react-native';

const dimen = Dimensions.get('window');

module.exports = {

	hasNotch: function() {
		return this.isIphoneX() || this.isIphoneXsMax()
	},

	isIphoneX: function() {		
		return (
			Platform.OS === 'ios' &&
			!Platform.isPad &&
			!Platform.isTVOS &&
			(dimen.height === 812 || dimen.width === 812)
		);
	},

	isIphoneXsMax: function () {
		return (
			Platform.OS === 'ios' &&
			!Platform.isPad &&
			!Platform.isTVOS &&
			(dimen.height === 896 && dimen.width === 414)
		);
	},
};

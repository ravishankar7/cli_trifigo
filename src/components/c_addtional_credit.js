// react
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

// elements
import PrimeModal from '../elements/prime_modal';
import Button from '../elements/button';

// store
import store from '../redux/store';

// api
import { getAdditionalCreditSummary } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import { colors, fontFamily, fontSize, deviceWidth } from '../styles/variables';

export default class AddtionalCredit extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      additionalCreditSummary: store.getState().additionalCreditSummary,

      unsubscribe: store.subscribe(this.updateState),

      modalVisible: false,

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      additionalCreditSummary: store.getState().additionalCreditSummary
    });
  }

  componentDidMount() {
    let _this = this;
    let userId = this.state.user.id;

    if (this.state.additionalCreditSummary === null) {
      try { getAdditionalCreditSummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    let st = this.state;
    //TODO
    // let _additionalCredit = '-';
    let _additionalCredit = '$ 0 @ 0 %';

    if (st.additionalCreditSummary && st.additionalCreditSummary.additionalCreditAmount) {
      _additionalCredit = '$ ' + (500 *  Math.round(st.additionalCreditSummary.additionalCreditAmount / 500));
    }
    
    if (st.additionalCreditSummary && st.additionalCreditSummary.additionalCreditRate) {
      _additionalCredit += ' @ ' + st.additionalCreditSummary.additionalCreditRate + '%';
    }

    let _text = 'Based on your cashflow from connected accounts, we have calculated that you would be able to handle additional credit capacity displayed. This is an estimate and will vary based on actual Interest Rates and changes in Cash Flow';
    
    if (_additionalCredit == '-') {
      _text = 'Link accounts to see information';
    }
    
    return (
      <TouchableOpacity style={styles.w} activeOpacity={0.8} onPress={() => this.setState({ modalVisible: true })}>
        <View style={cs.shadowBox}>
          
          <View style={cs.flex_center}>
            <Text style={styles.headertext}>Additional Credit</Text>
          </View>

          <View style={cs.flex_center}>
            <Text style={styles.subtext}>
              {_additionalCredit}
            </Text>
          </View>

          <PrimeModal
            modalVisible={st.modalVisible}
            onRequestClose={() => { this.setState({ modalVisible: false }) }}>
            <View style={[cs.shadowBox, styles.wp]}>
              <View style={[cs.flex_row_start, cs.p5]}>
                <Text style={styles.headertextpopup}>Additional Credit
                  <Text style={styles.subtextpopup}> {_additionalCredit}</Text>
                </Text>
              </View>
              <Text style={[cs.p10]}>{_text}</Text>
              <View style={[cs.flex_center, cs.p10, cs.mb20]}>
                <Button
                  text='Close'
                  width={100}
                  height={28}
                  fontSize={13}
                  backgroundColor='#546E7A'
                  onClick={() => this.setState({ modalVisible: false })} />
              </View>
            </View>
          </PrimeModal>

        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  headertext: {
    fontFamily: fontFamily.normal,
    fontSize: fontSize.xsmall
  },
  headertextpopup: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.normal
  },
  subtext: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.xsmall,
    color: colors.green,
  },
  subtextpopup: {
    fontFamily: fontFamily.normal,
    fontSize: fontSize.normal,
    color: colors.green,
  },
  w: {
    width: deviceWidth / 3
  },
  wp: {
    width: deviceWidth - 50
  }
});
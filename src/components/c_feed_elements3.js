// TEMPORARY --- THIS FILE HAS TO BE DELETED 

import React from 'react';
import { StyleSheet, Text, View, Image, Linking } from 'react-native';
import { Avatar } from 'react-native-elements';

// styles
import cs from '../styles/common_styles';
import { deviceWidth } from '../styles/variables';

export default class FeedElements3 extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={cs.shadowBox}>

        <View style={styles.usericon}>
          <Avatar
            medium
            rounded
            source={require('../../images/user_pic.png')}
            activeOpacity={0.7}
          />
          <Text style={styles.username}>Agnes Strickland</Text>
        </View>

        <View style={styles.imagecontainer}>

          <Image style={styles.imagefeed}
            source={require('../../images/feed3.png')} />

          <Text style={[cs.secondtitletext, cs.mv5]}>
            Ball Out Like Jay-Z
          </Text>

          <Text
            style={cs.subtitle2green}
            onPress={() => Linking.openURL('http://blog.trifigo.com/2017/12/ball-out-like-jay-z-through-better_27.html')}>
            http://blog.trifigo.com/2017/12/ball-out-like-jay-z-through-better_27.html
          </Text>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  usericon: {
    flexDirection: 'row',
    flex: 1,
    padding: 5
  },
  username: {
    color: '#000',
    fontSize: 20,
    paddingLeft: 40,
    marginTop: 10
  },
  options: {
    color: '#000',
    fontSize: 20,
    paddingLeft: 150
  },
  imagecontainer: {
    flexDirection: 'column',
    flex: 1,
  },
  imagefeed: {
    height: 100,
    width: deviceWidth - 40,
    borderRadius: 8
  },
  feedtext: {
    color: '#000',
    fontSize: 15,
    paddingLeft: 5,
  },
  linktext: {
    color: '#404040',
    fontSize: 18,
    paddingLeft: 10,
  },
});
// react
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, Image, TouchableOpacity, Platform } from 'react-native';
import { Icon } from 'react-native-elements';

import LinearGradient from 'react-native-linear-gradient';

// utils
import iphone_helper from '../utils/is_iphonex';

// store
import store from '../redux/store';

// api
import { getRecos } from '../modules/api';

// styles
import cs from '../styles/common_styles'
import { deviceWidth, blueGradient, NAV_HEIGHT, STATUSBAR_HEIGHT, fontFamily } from '../styles/variables';

export default class Header extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      recos: store.getState().recos,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      recos: store.getState().recos,
    });
  }

  componentDidMount() {
    let _this = this;
    if (this.state.user) {
      let userId = this.state.user.id;

      if (this.state.recos === null) {
        try { getRecos(userId, 'N'); }
        catch (e) { _this.setState({ error: e }); }
      }

      if (this.timer === null) {
        let timer = setInterval(this.tick, 60000); // 1 min
        this.timer = timer;
      }
    }
  }
  
  componentWillUnmount() {
    this.state.unsubscribe();
    clearInterval(this.timer);
  }

  tick = () => {
    let _this = this;
    let userId = this.state.user.id;
    // get recos every 1 min
    try { getRecos(userId, 'N'); }
    catch (e) { _this.setState({ error: e }); }
  }

  menuClick = () => {
    this.props.navigate('settings');  //Menu button on header redirects to settings page
  }

  backClick = () => {
    this.props.navigate(this.props.back);
  }

  render() {
    let st = this.state;

    let recos = st.recos;

    return (
      <View style={styles.outerContainer}>
        <LinearGradient
          start={{ x: 0.0, y: 0.0 }}
          end={{ x: 1.0, y: 1.0 }}
          colors={blueGradient.colors}
          style={styles.innerContainer}>

          <View style={styles.leftcol}>
            {this.props.menu &&
              <TouchableOpacity
                onPress={this.menuClick}>
                <View style={styles.btn}>
                  <Image
                    source={require('../../images/menu.png')}
                    style={{ width: 26, height: 17 }}
                  />
                </View>
              </TouchableOpacity>
            }

            {this.props.back &&
              <TouchableOpacity onPress={this.backClick}>
                <View style={[styles.btn, cs.mb5]}>
                  <Image
                    source={require('../../images/menu-back.png')}
                    style={{ height: 30 }}
                  />
                </View>
              </TouchableOpacity>
            }

          

          </View>

          <View style={styles.titleCol}>
            <Text style={styles.titleText}>
              {this.props.title}
            </Text>
          </View>
          {this.props.hideRight && <View style={styles.rightcol} />}
          {!this.props.hideRight && <TouchableOpacity onPress={() => this.props.navigate('recos')}>
            <View style={styles.rightcol}>
              <Icon size={44} name='like' type='evilicon' color='#fff' />
              <View style={styles.circle} />
              <Text style={styles.alerttext}>{recos !== null ? recos.length : ''}</Text>
            </View>
          </TouchableOpacity>}
          {this.props.rightBack &&
          <TouchableOpacity onPress={() => this.props.navigate('credit_profile')}>
            <View style={styles.rightcol}>
              <Image
                    source={require('../../images/menu-back.png')}
                    style={{ height: 30,  
                    transform: [{ rotateY: '180deg' } ] }}
                  />
            </View>
          </TouchableOpacity>
            }
        
        </LinearGradient>
      </View>
    );
  }
}

Header.propTypes = {
  navigate: PropTypes.func.isRequired,
  title: PropTypes.string,
  menu: PropTypes.bool,
  back: PropTypes.string
};

const h = iphone_helper.isIphoneXsMax ? 25 : iphone_helper.iPhoneX ? 16 : 13;

const styles = StyleSheet.create({
  outerContainer: {
    width: deviceWidth,
    zIndex: 99,
    ...Platform.select({
      ios: {
        height: NAV_HEIGHT + STATUSBAR_HEIGHT + h,
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOffset: {
          width: 0,
          height: 12
        },
        shadowRadius: 5,
        shadowOpacity: 0.3
      },
      android: {
        height: NAV_HEIGHT + h,
        elevation: 20,
      },
    }),
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: NAV_HEIGHT + STATUSBAR_HEIGHT + h
  },
  leftcol: {
    flex: 3,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: h,
    ...Platform.select({
      ios: {
        paddingTop: STATUSBAR_HEIGHT,
      }
    }),
  },
  btn: {
    height: NAV_HEIGHT,
    paddingHorizontal: 10,
    marginTop: h,
  },
  titleCol: {
    flex: 6,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: h,
    ...Platform.select({
      ios: {
        paddingTop: STATUSBAR_HEIGHT + 5,
      }
    }),
  },
  titleText: {
    backgroundColor: 'transparent',
    fontFamily: fontFamily.light,
    fontSize: 20,
    letterSpacing: 1,
    color: '#fff'
  },
  rightcol: {
    flex: 2,
    marginTop: h,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    ...Platform.select({
      ios: {
        paddingTop: STATUSBAR_HEIGHT,
      }
    }),
  },
  container: {
    backgroundColor: '#3C5468',
    paddingTop: 15,
    paddingBottom: 0,
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 5,
    zIndex: 999
  },
  middle: {
    flex: 5,
    alignItems: 'center',
  },
  box: {
    flex: 1,
  },
  boxmenu: {
    flex: 1.5,
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  headertext: {
    color: '#fff',
    fontSize: 22,
  },
  alerttext: {
    color: '#fff',
    fontSize: 14,
    fontFamily: fontFamily.semiBold,
    marginLeft: -16,
    marginTop: -8,
    backgroundColor: '#0bb795',
    height: 16,
  },
  circle: {
    marginLeft: -5,
    marginTop: -3,
    width: 24,
    height: 24,
    borderRadius: 24 / 2,
    backgroundColor: '#0bb795',
  }
});
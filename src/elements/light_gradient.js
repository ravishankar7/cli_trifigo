import React from 'react';

import LinearGradient from 'react-native-linear-gradient';

import { deviceHeight } from '../styles/variables';

const LightGradient = () => (
	<LinearGradient
		colors={['#ECEFF1', '#78909C']}
		style={{
			position: 'absolute',
			left: 0,
			right: 0,
			top: 0,
			height: deviceHeight * 1,
		}}
	/>
);

export default LightGradient;
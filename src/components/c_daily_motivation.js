// react
import React from 'react';
import { Text, View } from 'react-native';
//import { Button } from 'react-native-elements';

// api
import { getRandomCreditTip } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import common_styles from '../styles/common_styles';
import Button from '../elements/button';
import { colors } from '../styles/variables';

export default class DailyMotivation extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      creditTip: 'It takes strong character and smart decisions to help you acheive your financial goals. Stay on the journey.',

      error: null,
      loading: null,
    };
  }

  componentDidMount() {
    let _this = this;

    if (this.state.creditTip === '') {
      getRandomCreditTip().then(function (res, err) {
        if (res !== null && res !== undefined) {
          _this.setState({ creditTip: res.text });
        }
        else if (err !== undefined) {
          _this.setState({ error: err });
        }
      });
    }
  }

    render() {
      let st = this.state;

      return (
          <View style={[cs.shadowBox]} >

          <Text style={[cs.title1, cs.mb5, { fontSize: 16, alignSelf: 'center' }]}> DAILY MOTIVATION </Text>

          <Text style={[cs.subtitle2gray, cs.m5, cs.mr10, cs.mb20, { flex:1, flexDirection: 'row', flexWrap: 'wrap', textAlign: 'center' }]}>
            {st.creditTip}
          </Text>
            <View  style={{ height: 10 }}/>
          </View>
         
      );
    }
  }
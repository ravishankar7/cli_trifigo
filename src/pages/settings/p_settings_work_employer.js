//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View, Image, TextInput } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import OkBox from '../../elements/ok_box';

// store
import store from '../../redux/store';

// api
import { updateUser } from '../../modules/api';

// styles
import cs from '../../styles/common_styles';

//loader
import Spinner from 'react-native-spinkit';
import { colors } from '../../styles/variables';

export default class PageSettingsWorkEmployer extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;

    this.state = {
      user: _user,

      unsubscribe: store.subscribe(this.updateState),
      address1: _user !== null ? _user.workAddress1 : null,
      err_address1: null,

      address2: _user !== null ? _user.workAddress2 : null,
      err_address2: null,


      city: _user !== null ? _user.workCity : null,
      err_city: null,


      state: _user !== null ? _user.workState : null,
      err_state: null,


      zipcode: _user !== null ? _user.workZip : null,
      err_zip: null,

      workphone: _user !== null ? _user.workPhone : null,
      err_workphone: null,

      country: _user !== null ? _user.workCountry : null,
      err_country: null,
      employer: _user !== null ? _user.employer : null,
      err_name: null,

      error: null,
      loading: null,
      success: null,
    };

    trackScreenView('Settings Name');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,

      employer: _user !== null ? _user.employer : null,
      err_employer: null,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  resetErrors = () => {
    this.setState({
      loading: null,
      error: null,
      success: null,

      err_employer: null,
    });
  }

  validateData = () => {
    this.resetErrors();

    let errors = 0;
    let err = null;

    let st = this.state;

    if (st.workphone === null || st.workphone === undefined || st.workphone == '') {
      errors++;
      this.setState({ err_workphone: 'Workphone required' });
      if (err === null) err = 'workphone required';
    }

    // Validate name
    if (st.employer === null || st.employer === undefined || st.employer == '') {
      errors++;
      this.setState({ err_employer: 'Employer required' });
      if (err === null) err = 'Employer required';
    }

    if (st.address1 === null || st.address1 === undefined || st.address1 == '') {
      errors++;
      this.setState({ err_name: 'Address 1 required' });
      if (err === null) err = 'Address 1 required';
    }

    // Validate Address 2
    if (st.address2 === null || st.address2 === undefined || st.address2 == '') {
      errors++;
      this.setState({ err_lname: 'Address 2 required' });
      if (err === null) err = 'Address 2  required';
    }


    if (st.city === null || st.city === undefined || st.city == '') {
      errors++;
      this.setState({ err_lname: 'City required' });
      if (err === null) err = 'City required';
    }

    if (st.state === null || st.state === undefined || st.state == '') {
      errors++;
      this.setState({ err_lname: 'State required' });
      if (err === null) err = 'State required';
    }

    if (st.zipcode === null || st.zipcode === undefined || st.zipcode == '') {
      errors++;
      this.setState({ err_lname: 'Zipcode required' });
      if (err === null) err = 'Zipcode required';
    }


    if (errors == 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  saveData = () => {
    let errors = this.validateData();

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    this.setState({
      loading :'loading'
    })

    // save data
    let _this = this;
    let st = this.state;

    let userId = this.state.user.id;
    let user = this.state.user;

    user.employer = st.employer;
    user.workPhone = st.workphone;

    user.workAddress1 = st.address1;
    user.workAddress2 = st.address2;
    user.workCity = st.city;
    user.workState = st.state;
    user.workZip = st.zipcode;
    user.workCountry = st.country

    try {
      updateUser(userId, user);
      _this.setState({ success: 'Saved' }, () => {
        _this.timer = setInterval(() => {
          const { navigate } = this.props.navigation;
          clearInterval(_this.timer);
          navigate('settings');
        }, 1000);
      });
    }
    catch (e) { _this.setState({ error: e }); }
  }

  cancel = () => {
    this.props.navigation.navigate('settings');
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Settings'
          back='settings'
        />

        <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center, cs.mt30]}>

            <Text style={[cs.headertextdarkgray, cs.mb10]}>Employer</Text>

            {/* Name textbox */}
            <View style={cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Employer name'
                value={st.employer}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ employer: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>


            {/* Address1 textbox */}
            <View style={cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Address 1 '
                value={st.address1}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ address1: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize="words"
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                returnKeyType="next"
                autoCorrect={false}
              />
            </View>

            {/* Address2 textbox */}
            <View style={cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Address 2'
                ref={(input) => { this.secondTextInput = input; }}
                value={st.address2}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ address2: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'words'}
                onSubmitEditing={() => { this.thirdTextInput.focus(); }}
                returnKeyType="next"
                autoCorrect={false}
              />
            </View>


            {/* City textbox */}
            <View style={cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='City'
                value={st.city}
                style={cs.textInput}
                ref={(input) => { this.thirdTextInput = input; }}
                onChangeText={(text) => this.setState({ city: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'words'}
                onSubmitEditing={() => { this.fourthTextInput.focus(); }}
                returnKeyType="next"
                autoCorrect={false}
              />
            </View>

            {/* State textbox */}
            <View style={cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='State'
                value={st.state}
                ref={(input) => { this.fourthTextInput = input; }}
                onSubmitEditing={() => { this.fifthTextInput.focus(); }}
                returnKeyType="next"
                style={cs.textInput}
                onChangeText={(text) => this.setState({ state: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'words'}
                autoCorrect={false}
              />
            </View>

            {/* country textbox */}
            <View style={cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Country'
                value={st.country}
                ref={(input) => { this.fifthTextInput = input; }}
                onSubmitEditing={() => { this.sixthTextInput.focus(); }}
                returnKeyType="next"
                style={cs.textInput}
                onChangeText={(text) => this.setState({ country: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'

                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            {/* Zip textbox */}
             <View style={cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Zip code'
                value={st.zipcode}
                ref={(input) => { this.sixthTextInput = input; }}
                returnKeyType="done"
                style={cs.textInput}
                onChangeText={(text) => this.setState({ zipcode: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                keyboardType="number-pad"
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

             <View style={st.err_workphone !== null ? cs.textInputFieldError : cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Work Phone'
                value={st.workphone}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ workphone: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            { this.state.loading 
            ?
            <Spinner
                  isVisible
                  style = {cs.mt2}
                  size={60}
                  type='ThreeBounce'
                  color={colors.green} />
            :
             <View style={[cs.flex_row_center, cs.mb10]}>

              <View style={cs.m5}>
                <Button
                  text='Cancel'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#CFD8DC'
                  textColor='#404040'
                  onClick={this.cancel} />
              </View>

              <View style={cs.m5}>
                <Button
                  text='Save'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#66BB6A'
                  onClick={this.saveData} />
              </View>

            </View>}

            <View style={[cs.flex_row_center, cs.mb10]}>

              {st.error !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              }

              {st.success !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              }

            </View>

          </View>

        </KeyboardAwareScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsWorkEmployer.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettingsWorkEmployer.defaultProps = {
  user: null,
};

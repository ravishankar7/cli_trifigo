// react
import React from 'react';
import { Text, View } from 'react-native';
//import { Button } from 'react-native-elements';

// api
import { getRandomBankingTip } from '../modules/api';

// styles
import cs from '../styles/common_styles';

export default class BankingTip extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      bankingTip: '',

      error: null,
      loading: null,
    };
  }

  componentDidMount() {
    let _this = this;

    if (this.state.bankingTip === '') {
      getRandomBankingTip().then(function (res, err) {
        if (res !== null && res !== undefined) {
          _this.setState({ bankingTip: res.text });
        }
        else if (err !== undefined) {
          _this.setState({ error: err });
        }
      });
    }
  }

    render() {
      let st = this.state;

      return (
        <View style={cs.shadowBox} >

          <Text style={[cs.title1, cs.mb5]}>Banking Tip</Text>

          <Text style={[cs.subtitle2gray, cs.m5, cs.mr10, cs.mb20]}>
            {st.bankingTip}
          </Text>

          {/*
          <View style={styles.breakline} />

          <Button
            small
            backgroundColor='#fff'
            color='#000'
            textStyle={cs.title2}
            title='Share' />
          */}
          
        </View>
      );
    }
  }
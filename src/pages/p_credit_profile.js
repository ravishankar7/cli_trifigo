import React from 'react';
import PropTypes from 'prop-types';

import { View, ScrollView, Keyboard, Text, StyleSheet, FlatList } from 'react-native';
import { getUserGoal } from '../modules/api';

// google analytics
import { trackScreenView } from '../modules/ga';
import Header from '../elements/header';
import Footer from '../elements/footer';
import Button from '../elements/button'

import UserInfo from '../components/c_user_info';
//import FicoBar from '../components/c_fico_bar';
//import CreditScoreBar from '../components/c_credit_score_bar';
//import NotificationBar from '../components/c_notification_bar';
//import Goals from '../components/c_goals';
import CreditBehavior from '../components/c_credit_behavior';
import CreditTip from '../components/c_credit_tip';
//import Rewards from '../components/c_rewards';
//import MarketUpdate from '../components/c_market_update';
//import Compare from '../components/c_compare';
import PaymentHistory from '../components/c_payment_history';
import CreditUtilization from '../components/c_credit_utilization';
import AccountAge from '../components/c_account_age';
import AccountMix from '../components/c_account_mix';
import CreditInquiries from '../components/c_credit_inquiries.js';
import OtherFactors from '../components/c_other_factors';
import CreditQuiz from '../components/c_credit_quiz';
import GotoFeedback from '../components/c_goto_feedback';

import store from '../redux/store';

import cs from '../styles/common_styles.js';
import { fontFamily, colors } from '../styles/variables';
import Icon from 'react-native-vector-icons/Ionicons';

import * as Progress from 'react-native-progress';

// api
import { getQuovoAccountSummary } from '../modules/api';
import CreditGrade from '../components/c_credit_grade';
import CreditFriends from '../components/c_credit_friends';
import CreditProgress from '../components/c_credit_progress';
import ScoreTracker from '../components/c_credit_tracker';
import DailyMotivation from '../components/c_daily_motivation';
import CreditSimulator from '../components/c_credit_simulator';

export default class PageCreditProfile extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      myGoals: store.getState().this_user_goals,
      quovoAccountSummary: store.getState().quovoAccountSummary,

      unsubscribe: store.subscribe(this.updateState),
      error: null,
      loading: '',
    };

    trackScreenView('Credit Profile');
  }

  async componentDidMount() {
    if (this.state.user === null) return;

    let userId = this.state.user.id;
    let _this = this;

    await getUserGoal((userId), '');

    if (!this.state.quovoAccountSummary) {
      try { getQuovoAccountSummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }

    Keyboard.dismiss();
    this.updateState;
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      myGoals: store.getState().this_user_goals,
      quovoAccountSummary: store.getState().quovoAccountSummary,
    });
  }

  renderMenuHeader = (title) => {
    return (
      <View style={cs.flex_row_space_between}>
        <Text style={[cs.title1, cs.m5]}>{title}</Text>
      </View>
    );
  }

  renderTitle = (title) => {
    return (
      <View style={cs.flex_row_space_between}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }

  calcProgress(start, target) {
    //console.log(start,target)
    if (start == 0 || start < 0) {
      return 0
    }
    else {
      var res = start / target
      return res.toFixed(2)
    }
  }

  render() {
    let st = this.state;

    let goals_navigation = 'goals_link';

    if (st.quovoAccountSummary && st.quovoAccountSummary.length > 0) {
      goals_navigation = 'goals_select';
    }

    const { navigation : { navigate }, navigation } = this.props;

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          navigation={navigation}
          menu
          title='Credit Profile'
        />

        <ScrollView keyboardShouldPersistTaps='always'>

          <UserInfo
            navigate={navigate}
            user={st.user}
            creditinfo
          />

          {/* for v 1.5
          <FicoBar navigate={navigate}/>
        
          <CreditScoreBar navigate={navigate}/>
          */}

          {/*
          <NotificationBar navigate={navigate}/>

          <Goals navigate={navigate}/>
          
          */}

          <DailyMotivation />

          <CreditGrade navigate={navigate} />

          <ScoreTracker navigate={navigate} />

          <CreditSimulator navigate={navigate} />

          <CreditFriends navigate={navigate} />

          <CreditQuiz />

          <CreditTip navigate={navigate} />

          <CreditBehavior navigate={navigate} />

          <CreditProgress navigate={navigate} />

          <View style={cs.shadowBox}>
            {this.renderMenuHeader('Goals')}

            <View style={[cs.flex_col_space_between, styles.subcontainer]}>
              {this.state.myGoals.length > 0 ?
                <View>
                  <FlatList
                    showsHorizontalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    data={this.state.myGoals}
                    renderItem={({ item }) => (
                      <View style={{ flexDirection: 'row', paddingVertical: 13 }}>
                        <View style={{ paddingHorizontal: 3, alignItems: 'center', justifyContent: 'center', width: 50, height: 50, borderRadius: 25, backgroundColor: '#3C5468' }}>

                          <Icon name="md-cash" size={24} color='white' />
                        </View>
                        <View style={{ width: '80%', paddingLeft: 5, justifyContent: 'center' }}>
                          {this.renderTitle(item.goalType)}
                          <View style={{ width: '100%', }}>
                            <Progress.Bar progress={this.calcProgress(item.currentAmount, item.targetAmount)} width={null} color={colors.seagreen} height={10} borderRadius={6} unfilledColor='#e8ecf1' borderColor='transparent' />
                            <View style={{ alignSelf: 'flex-end', flexDirection: 'row', alignItems: 'center' }}>
                              <Text style={styles.title}>${item.currentAmount}</Text>
                              <Text style={{ color: colors.darkGray }}> of </Text>
                              <Text style={styles.title}>${item.targetAmount}</Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    )} />
                  <View style={{ alignSelf: 'center', paddingVertical: 5 }}>
                    <Button
                      onClick={() => this.props.navigation.navigate(goals_navigation)}
                      text='Update Goals'
                      width={200}
                      height={30}
                      fontSize={13}
                      backgroundColor='#546E7A'
                    />
                    <View style={cs.mb10} />
                  </View>

                </View>
                :
                <View style={{ alignSelf: 'center', paddingVertical: 5 }}>
                  <Button
                    onClick={() => this.props.navigation.navigate(goals_navigation)}
                    text='Set Goals'
                    width={200}
                    height={30}
                    fontSize={13}
                    backgroundColor='#546E7A'
                  />
                  <View style={cs.mb10} />
                </View>
              }


            </View>
          </View>

          {/* for v 1.5
          <Rewards navigate={navigate}/>
          */}

          {/* See what we can get from Zillo API / BankRate API */}
          {/*
          <MarketUpdate navigate={navigate}/>
          */}

          {/* 
          <Compare navigate={navigate}/>
          */}

          <PaymentHistory navigate={navigate} />

          <CreditUtilization navigate={navigate} />

          <AccountAge navigate={navigate} />

          <AccountMix navigate={navigate} />

          <CreditInquiries navigate={navigate} />

          <OtherFactors navigate={navigate} />

          <GotoFeedback navigateTo = 'credit_profile' navigate={navigate} />

          <View style={cs.mt10} />

        </ScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageCreditProfile.propTypes = {
  navigation: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  subcontainer: {
    margin: 1,
    padding: 5,
    paddingHorizontal: 9,

  },
  subtitletext: {
    color: '#0bb795',
    fontSize: 16,
    letterSpacing: 0.75,
    fontFamily: fontFamily.regular
  },
  title: {
    fontSize: 14,
    fontFamily: fontFamily.semiBold,
    fontWeight: '500',
    justifyContent: 'flex-start',
    color: '#2C3E50',
    letterSpacing: 0.5,

  }
})
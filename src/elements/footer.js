import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { Icon } from 'react-native-elements';

export default class Footer extends React.Component {

	constructor(props) {
		super(props);
		const { isHidden } = this.props;
		this.state = {
			isHidden,
		}
	}

	render() {
		const { isHidden } = this.state;

		if (isHidden) {
			return (
			<View style={[styles.container]}>
				<Icon name='credit-card' type='font-awesome' color='#3C5468' onPress={() => this.props.navigate('credit_profile')} />
				<Icon name='university' type='font-awesome' color='#3C5468' onPress={() => this.props.navigate('banking_profile')} />
				{/*
				<Icon name='badge' type='simple-line-icon' color='#3C5468' onPress={() => this.props.navigate('rewards')} />
				*/}
				{/* <Icon name='newspaper-o' type='font-awesome' color='#3C5468' onPress={() => this.props.navigate('feed')} />
				<Icon name='tag' type='simple-line-icon' color='#3C5468' onPress={() => this.props.navigate('offers')} /> */}
			</View>
		);
		}

		return (
			<View />
		)
	}
}

Footer.defaultProps = {
	isHidden: false,
}

Footer.propTypes = {
	//======> Hiding Banking Profile
	isHidden: PropTypes.bool.isRequired,
	//======> Hiding Banking Profile
	navigate: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#fff',
		flexDirection: 'row',
		justifyContent: 'space-around',
		paddingTop: 10,
		paddingBottom: 22,
		borderTopWidth: 1,
		borderColor: '#f1f1f1'
	},
});
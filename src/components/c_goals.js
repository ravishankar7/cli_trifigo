import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';

import cs from '../styles/common_styles';

export default class Goals extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style={cs.shadowBox}>

        <Text style={cs.title1}>Top 3 Goals</Text>
        
        <View style={cs.mb5} />

        <View style={styles.row}>

          <View style={[styles.box]}>
            <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigate('goals')}>
              <Image source={require('../../images/plus.png')} style={{ width: 100, height: 70 }} />
            </TouchableOpacity>
          </View>

          <View style={[styles.box]}>
            <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigate('goals')}>
              <Image source={require('../../images/plus.png')} style={{ width: 100, height: 70 }} />
            </TouchableOpacity>
          </View>

          <View style={[styles.box]}>
            <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigate('goals')}>
              <Image source={require('../../images/plus.png')} style={{ width: 100, height: 70 }} />
            </TouchableOpacity>
          </View>

        </View>

      </View>
    );
  }
}

Goals.propTypes = {
  navigate: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 78
  },

  box: {
    flex: 1,
    backgroundColor: '#DADADA',
    height: 70,
    margin: 1,
    width: 100,
  },
  textbox: {
    flex: 1,
    margin: 1,
    width: 100,
    marginTop: 10,
  },
  rowtext: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 10,
  },
});
// react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View } from 'react-native';

// store
import store from '../redux/store';

// api    
import { getCreditUtilizationSummary } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import { fontFamily } from '../styles/variables';

export default class CreditUtilization extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      creditUtilizationSummary: store.getState().creditUtilizationSummary,
      creditUtilizationSentimentTypes: store.getState().creditUtilizationSentimentTypes,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      creditUtilizationSummary: store.getState().creditUtilizationSummary,
      creditUtilizationSentimentTypes: store.getState().creditUtilizationSentimentTypes
    });
  }

  componentDidMount() {
    if (this.state.user === null) return;
    
    let _this = this;
    let userId = this.state.user.id;

    if (this.state.creditUtilizationSummary === null) {
      try { getCreditUtilizationSummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }
  
  render() {
    let st = this.state;
    let title = 'Upload credit report to see utilization summary';
    let item = null;

    if (st.creditUtilizationSummary && st.creditUtilizationSummary.creditUtilization) {
      title = 'You are using ' + st.creditUtilizationSummary.creditUtilization + ' % of your open credit.';
    }

    if (st.creditUtilizationSentimentTypes !== null && st.creditUtilizationSentimentTypes.length > 0 && st.creditUtilizationSummary !== null) {
      st.creditUtilizationSentimentTypes.forEach(i => {
        if (i.min <= st.creditUtilizationSummary.creditUtilization && st.creditUtilizationSummary.creditUtilization <= i.max) {
          item = i;
        }
      });
    }

    return (
      <View style={cs.shadowBox}>

        <View style={cs.flex_row_space_between}>

          <Text style={cs.title1}>Credit Utilization</Text>

          {item !== null &&
            <View style={{ backgroundColor: item.colorBg, height: 24, borderRadius: 8, width: 80, alignItems: 'center', paddingVertical: 1 }}>
              <Text style={{ fontFamily: fontFamily.semiBold, fontSize: 13, color: item.colorFg, letterSpacing: 0.5 }}>
                {item.result}
              </Text>
            </View>
          }

        </View>

        <Text style={[cs.secondtitletext, cs.mb10]}>
          High Importance / 30%
        </Text>

        <View style={[cs.flex_row_space_between, cs.mr10]}>

          <Text style={[cs.subtitle2gray, cs.m5, cs.mr10]}>
            {title} {item !== null ? item.sentiment : ''}
          </Text>
          
          {/*
          <View style={[cs.mt10, cs.w10]}>
            <TouchableOpacity onPress={() => this.props.navigate('credit_util')}>
              <Image
                source={require('../../images/chevron-right.png')}
                style={{ height: 35 }}
              />
            </TouchableOpacity>
          </View>
          */}

        </View>

      </View>
    );
  }
}

CreditUtilization.propTypes = {
  navigate: PropTypes.func.isRequired
};
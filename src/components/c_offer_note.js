import React from 'react';
import { StyleSheet, Image, View, Linking, TouchableOpacity } from 'react-native';

import { Button } from 'react-native-elements';

import cs from '../styles/common_styles';

export default class OfferNote extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let pr = this.props;

    return (
      <TouchableOpacity onPress={() => Linking.openURL(pr.linkUrl)}>
        <View style={[styles.container, cs.shadowBox]}>

          <Image style={styles.logo} source={{ uri: pr.logoUrl }} />

          {pr.text &&
          <Button
            backgroundColor='#fff'
            color='#000000'
            buttonStyle={{ padding: 5, paddingBottom: 15, paddingTop: 5 }}
            textStyle={{ textAlign: 'justify', fontSize: 14, letterSpacing: 0.2 }}
            title={pr.text} />
          }
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
    padding: 15,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    margin: 10,
    height: 43,
    width: 200
  },
});
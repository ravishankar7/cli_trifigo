import React from 'react';

import LinearGradient from 'react-native-linear-gradient';

import { deviceHeight } from '../styles/variables';

const DarkGradient = () => (
	<LinearGradient
		colors={['#34495E', '#000']}
		style={{
			position: 'absolute',
			left: 0,
			right: 0,
			top: 0,
			height: deviceHeight * 1,
		}}
	/>
);

export default DarkGradient;
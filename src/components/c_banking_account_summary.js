// react
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';

// components
import Account from '../components/c_account';

// store
import store from '../redux/store';

// api
import { getQuovoAccountSummary } from '../modules/api';

// styles
import cs from '../styles/common_styles.js';

export default class BankingAccountSummary extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      quovoAccountSummary: store.getState().quovoAccountSummary,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,

      accountdata: {},
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      quovoAccountSummary: store.getState().quovoAccountSummary,
    });
  }

  componentDidMount() {
    if (this.state.user === null) return;
    
    let userId = this.state.user.id;
    let _this = this;

    if (!this.state.quovoAccountSummary) {
      try { getQuovoAccountSummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    let pr = this.props;

    return (
      <View style={cs.shadowBox}>

        <Text style={[cs.title1, cs.mb5]}>{pr.assets ? 'Assets' : 'Liabilities'}</Text>
      
        <View
          style={{
            borderBottomColor: '#202020',
            borderBottomWidth: StyleSheet.hairlineWidth,
            marginBottom: 5,
          }}
        />

        <View style={cs.mb5} />

        {this.renderAccounts()}

      </View>
    );
  }

  renderAccounts() {
    let st = this.state;
    let pr = this.props;

    let assets = [];
    let liabilities = [];

    if (st.quovoAccountSummary) {

      if (st.quovoAccountSummary.length == 0) {
        return (
          <View>
            <Text style={cs.subtitle2gray}>No connected accounts</Text>
          </View>
        );
      }
      else {
        for (let i = 0; i < st.quovoAccountSummary.length; i++) {
          const element = st.quovoAccountSummary[i];
          
          switch (element.type) {
            case 'Credit Card':
            case 'Auto Loan':
            case 'HELOC':
            case 'Loan':
            case 'Mortgage':
            case 'Student Loan':
              liabilities.push(element);
              break;
          
            default:
              assets.push(element);
              break;
          }
        }

        return st.quovoAccountSummary.map(a => {
          if (pr.assets && assets.indexOf(a) !== -1) {
            return <Account account={a} key={a.id} navigate={pr.navigate} />;
          }
          if (pr.liabilities && liabilities.indexOf(a) !== -1) {
            return <Account account={a} key={a.id} navigate={pr.navigate} />;
          }
          return null;
        });
      }
    }

    return null;
  }
}

BankingAccountSummary.propTypes = {
  navigate: PropTypes.func.isRequired
};
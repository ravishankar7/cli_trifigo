import React from 'react';
import PropTypes from 'prop-types';

import { Text, View, Dimensions, Image, TouchableOpacity } from 'react-native';

const deviceWidth = Dimensions.get('window').width;

import cs from '../styles/common_styles';

export default class Advertising extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={cs.shadowBox}>

        <Text style={[cs.title1, cs.mb5]}>Premium Credit</Text>

        <Image source={require('../../images/premium_credit.png')} style={{ width: deviceWidth - 40, height: 100 }} />

        <View style={[cs.flex_row_end, cs.mv5]}>
          <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigate('goals')}>
            <Text style={cs.subtitle2}>Read More</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

Advertising.propTypes = {
  navigate: PropTypes.func.isRequired
};
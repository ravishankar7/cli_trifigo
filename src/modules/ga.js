// google analytics
import {
	GoogleAnalyticsTracker,
	GoogleAnalyticsSettings
} from "react-native-google-analytics-bridge";

GoogleAnalyticsSettings.setDispatchInterval(30);

// The tracker must be constructed
let tracker = new GoogleAnalyticsTracker("UA-119547501-1");

export const trackScreenView = (name) => {
	tracker.trackScreenView(name);
};

export const trackEvent = (name, val) => {
	tracker.trackEvent(name, val);
};
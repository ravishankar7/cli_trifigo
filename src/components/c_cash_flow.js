// react
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
//import * as Progress from 'react-native-progress';

// store
import store from '../redux/store';
import { setCashFlowSummary } from '../redux/actions';

// router
import Route from '../modules/route.js';
// const route = new Route('https://trifigoapi.azurewebsites.net/');
const route = new Route(LiveUrl);

// styles
import { fontFamily } from '../styles/variables';
import cs from '../styles/common_styles.js';
import { DevUrl, LiveUrl } from '../modules/api';

export default class CashFlow extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      cashFlowSummary: store.getState().cashFlowSummary,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      cashFlowSummary: store.getState().cashFlowSummary,
    });
  }

  componentDidMount() {
    let userId = this.state.user.id;

    if (this.state.cashFlowSummary === null) {
      this.getCashFlowSummary(userId);
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  getCashFlowSummary = (id) => {
    let _this = this;
    route.post('cashflowsummary/' + id).then(function (res, err) {
      if (res !== undefined) {
        store.dispatch(setCashFlowSummary(res));
      }
      else if (err !== undefined) {
        _this.setState({
          error: 'Error',
          loading: null
        });
      }
    });
  }

  render() {
    let st = this.state;

    return (
      <View style={cs.shadowBox}>

        <Text style={[cs.title1, cs.mb5]}>Cash Flow</Text>

        <View style={[cs.flex_row_start, cs.p5]}>
          {/*}
          <Progress.Bar style={{ "marginTop": 20, "marginBottom": 25 }} height={10} borderColor='white' borderRadius={12} color='#58D68D' unfilledColor='#EAECEE' progress={1} width={160} borderWidth={0} />
          */}
          <Image
            source={require('../../images/icon_earned.png')}
            style={{ width: 35, height: 35, marginLeft: 5 }}
          />
          <Text style={[cs.subtitle2gray, cs.m5, cs.ml10, styles.boldtext]}>$ 
          {st.cashFlowSummary && st.cashFlowSummary.earned ? Math.round(st.cashFlowSummary.earned) : ''} 
            <Text style={cs.subtitle2gray}> earned last month</Text>
          </Text>
        </View>

        <View style={[cs.flex_row_start, cs.p5]}>
          {/*
          <Progress.Bar style={{ "marginTop": 20, "marginBottom": 25 }} height={10} borderColor='white' borderRadius={12} color='#EC7063' unfilledColor='#EAECEE' progress={1} width={160} borderWidth={0} />
          */}
          <Image
            source={require('../../images/icon_spend.png')}
            style={{ width: 35, height: 35, marginLeft: 5 }}
          />
          <Text style={[cs.subtitle2gray, cs.m5, cs.ml10, styles.boldtext]}>$ 
          {st.cashFlowSummary && st.cashFlowSummary.spent ? Math.round(st.cashFlowSummary.spent) : ''}
            <Text style={cs.subtitle2gray}> spent last month</Text>
          </Text>
        </View>

        <View style={[cs.flex_row_start, cs.p5]}>
          {/*}
          <Progress.Bar style={{ "marginTop": 20, "marginBottom": 25 }} height={10} borderColor='white' borderRadius={12} color='#3498DB' unfilledColor='#EAECEE' progress={1} width={160} borderWidth={0} />
          */}
          <Image
            source={require('../../images/icon_avgtm.png')}
            style={{ width: 35, height: 35, marginLeft: 5 }}
          />
          <Text style={[cs.subtitle2gray, cs.m5, cs.ml10, styles.boldtext]}>$ 
          {st.cashFlowSummary && st.cashFlowSummary.earnedLastMonth ? Math.round(st.cashFlowSummary.earnedLastMonth) : ''}
            <Text style={cs.subtitle2gray}> last month cash flow</Text>
          </Text>
        </View>

        <View style={[cs.flex_row_center, cs.m10]}>
          <Text style={[cs.title2]}>3 Month Avg Cash $
          <Text style={styles.boldtext}>
          {st.cashFlowSummary && st.cashFlowSummary.threeMonthAvgCash ? Math.round(st.cashFlowSummary.threeMonthAvgCash): ''}
          </Text>
          </Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  numtext: {
    fontSize: 15,
    fontWeight: '400',
    color: '#808B96',
    letterSpacing: 0.5,
    fontFamily: fontFamily.light,
  },
  boldtext: {
    fontWeight: '600',
  },
  smtext: {
    fontSize: 14,
    fontWeight: '400',
    color: '#808B96',
    letterSpacing: 0.5,
    fontFamily: fontFamily.light,
  },
  progress: {
    margin: 10,
  },
});

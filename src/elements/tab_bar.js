// react
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, TouchableOpacity, Platform } from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

// styles
import cs from '../styles/common_styles.js';
import { deviceWidth, blueGradient} from '../styles/variables';

export default class TabBar extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      error: null,
      loading: null,
      active: props.active,
    };
  }

  onPress = (tab) => {
    this.setState ({ active: tab });

    if (this.props.onSelect !== null && this.props.onSelect !== undefined)
      this.props.onSelect(tab);
  }

  render() {
    let _this = this;
    let pr = this.props;
    let st = this.state;

    let x = (pr.tabs !== null && pr.tabs !== undefined) ? pr.tabs.length : 1;

    const rect = {
      width: (deviceWidth / x),
      paddingBottom: 5
    };

    return (
      <View style={styles.outerContainer}>

        <LinearGradient
          colors={blueGradient.colors}
          style={styles.innerContainer}>
          {pr.tabs !== null && pr.tabs.length > 0 &&
            pr.tabs.map(function (tab, i) {
              return (
                <TouchableOpacity key={i} activeOpacity={0.9} onPress={() => _this.onPress(tab)} >
                  <View style={cs.flex_col_center}>
                    <Text style={[cs.tabtext, cs.p5]}>{tab}</Text>
                    { tab === st.active ?
                      <View key={i} style={[styles.rectangle, rect]} />
                      :
                      <View key={i} style={[styles.rectangleD, rect]} />
                    }
                  </View>
                </TouchableOpacity>
              );
            })
          }
        </LinearGradient>
        
      </View>
    );
  }
}

TabBar.propTypes = {
  tabs: PropTypes.array,
  active: PropTypes.string,
  onSelect: PropTypes.func
};

const styles = StyleSheet.create({
  outerContainer: {
    backgroundColor: '#003152',
    width: deviceWidth,
    zIndex: 99,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.9)',
        shadowOffset: {
          width: 0,
          height: 12
        },
        shadowRadius: 5,
        shadowOpacity: 0.3
      },
      android: {
        elevation: 20,
      },
    }),
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    height: 40
  },
  rectangle: {
    backgroundColor: '#0bb795'
  },
  rectangleD: {
    backgroundColor: 'transparent'
  }
});
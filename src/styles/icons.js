const icons = {
	university: require('../../images/university.png'),
	calendar: require('../../images/calendar.png'),
	question: require('../../images/question.png')
};

module.exports = icons;
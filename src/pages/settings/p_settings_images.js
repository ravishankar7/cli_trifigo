//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View, ScrollView } from 'react-native';
import { Avatar } from 'react-native-elements';

// google analytics
import { trackScreenView } from '../../modules/ga';

var ImagePicker = require('react-native-image-picker');

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import OkBox from '../../elements/ok_box';

// store
import store from '../../redux/store';

// api
import {
  uploadProfilePhoto,
  updateUser
}
from '../../modules/api';

import { setUser } from '../../redux/actions';

// styles
import cs from '../../styles/common_styles';

export default class PageSettingsImages extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;

    this.state = {
      user: _user,

      ppic: null,
      cpic: null,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
      success: null,
    };

    trackScreenView('Settings Images');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  cancel = () => {
    this.props.navigation.navigate('settings');
  }

  pickProfileImage = () => {
    let _this = this;

    // More info on all the options is below in the README...just some common use cases shown here
    var options = {
      title: 'Select Profile Image',
      takePhotoButtonTitle: null,
      storageOptions: {
        skipBackup: true,
        path: 'images'
      },
       maxWidth : 400,
       maxHeight  : 400,
       quality  : 0.4,
       allowsEditing  : true
    };

    // Open Image Library:
    ImagePicker.launchImageLibrary(options, (response) => {

      if (response.didCancel) {
        _this.setState({ error: 'Cancelled' });
      }
      else if (response.error) {
        _this.setState({ error: 'Error picking image' });
      }
      else {
        //console.log('response >>>', response);

        let userId = _this.state.user.id;

        let formData = new FormData();
        let exnParts = response.fileName.split('.');
        this.setState({
          ppic : response.uri
        })
        formData.append('file', {
          uri: response.uri,
          type: 'image/' + exnParts[exnParts.length - 1],
          name: response.fileName
        });

        //console.log('formData >>>', JSON.stringify(formData, null, 2));

        try {
          uploadProfilePhoto(userId, formData);

          let user = _this.state.user;
          user.profileImgUrl = 'https://s3-us-west-2.amazonaws.com/tfgimages/' + this.state.user.id + '/' + response.fileName;

          updateUser(user.id, user);

          _this.setState({ user: user });

          setUser(user);

          _this.setState({ success: 'Image uploaded' }, () => {
            _this.timer = setInterval(() => {
              const { navigate } = this.props.navigation;
              clearInterval(_this.timer);
              navigate('credit_profile');
            }, 2000);
          });
        }
        catch (e) { _this.setState({ error: e }); }
      }
    });
  }

  pickCoverImage = () => {

  }

  render() {
    let _this = this;
    let st = this.state;

    let ppic = this.props.navigation.getParam('image', 'https://s3-us-west-2.amazonaws.com/tfgimages/null/ppic.png' )
    
    let profileImgUrl = ppic;

    let user = st.user;

    // let profileImgUrl = 'https://s3-us-west-2.amazonaws.com/tfgimages/null/ppic.png';

    if (user.profileImgUrl !== null && user.profileImgUrl !== undefined) {
      profileImgUrl = user.profileImgUrl;
    }

    //let coverImgUrl = 'https://s3-us-west-2.amazonaws.com/tfgimages/null/cpic.png';

    //if (user.coverImgUrl !== null && user.coverImgUrl !== undefined) {
    //  coverImgUrl = user.coverImgUrl;
    //}

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Images'
          back='settings'
        />
        
        <ScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center, cs.mt20]}>

            <Text style={[cs.headertextdarkgray, {marginBottom : 7}]}>Profile Image</Text>

            <Avatar
              width={115}
              height={115}
              rounded
              source={{ uri: profileImgUrl }}
              activeOpacity={0.7}
            />
            
            <View style={cs.m5} />

            <View style={[cs.flex_row_center, cs.mb10]}>

              <View style={cs.m5}>
                <Button
                  text='Photos'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#66BB6A'
                  onClick={_this.pickProfileImage} />
              </View>

              <View style={cs.m5}>
                <Button
                  text='Camera'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#4DB6AC'
                  onClick={() => this.props.navigation.navigate('camera')} />
              </View>

            </View>

            {/*
            <View style={cs.m20} />

            <Text style={[cs.headertextdarkgray, cs.mb10]}>COVER IMAGE</Text>

            <Image
              source={{ uri: coverImgUrl }}
              style={styles.imageBackground} />

            <View style={cs.m5} />

            <View style={[cs.flex_row_center, cs.mb10]}>
              <View style={cs.m5}>
                <Button
                  text='Change'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#66BB6A'
                  onClick={_this.pickCoverImage} />
              </View>

            </View>

            */}

            <View style={[cs.flex_row_center, cs.mb10]}>

              {st.error !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              }

              {st.success !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              }

            </View>

          </View>

        </ScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsImages.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettingsImages.defaultProps = {
  user: null,
};

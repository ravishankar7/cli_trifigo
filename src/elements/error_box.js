import React from 'react';
import PropTypes from 'prop-types';

import { Text, View, StyleSheet } from 'react-native';

import { colors, fontSize, fontFamily } from '../styles/variables';

export default class ErrorBox extends React.Component {
	
	constructor(props) {
		super(props);
	}

	render = () => {
		let pr = this.props;

		if(pr.text === null || pr.text === undefined || pr.text === '') {
			return (
				<View style={[styles.emptyBox]} />
			);
		}
		return (
			<View style={[styles.errorBox]}>
				<Text style={styles.errorText}>
					{this.props.text}
				</Text>
			</View>
		);
	}
}

// Define button style
const styles = StyleSheet.create({
	emptyBox: {
		alignItems: 'center',
		backgroundColor: 'transparent',
		height: 24
	},
	errorBox: {
		alignItems: 'center',
		backgroundColor: '#AF0032',
		borderRadius: 6,
		height: 24	
	},
	errorText: {
		paddingVertical: 2,
		paddingHorizontal: 8,
		color: colors.white,
		fontSize: fontSize.small,
		letterSpacing: 3,
		backgroundColor: 'transparent',
		fontFamily: fontFamily.regular,
	},
});

ErrorBox.propTypes = {
	text: PropTypes.string
};
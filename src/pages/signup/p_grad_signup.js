// react
import React from 'react';
import PropTypes from 'prop-types';

import { Image, View, Text, TextInput, AsyncStorage, ScrollView, TouchableOpacity } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import DarkGradient from '../../elements/dark_gradient';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';

// modules
import Validator from '../../modules/validator.js';

// redux
import store from '../../redux/store';

// api
import { setSignupData } from '../../redux/actions';

// styles
import cs from '../../styles/common_styles.js';
import { colors, deviceWidth } from '../../styles/variables';

export default class PageGradSignup extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      confirmpass: '',

      err_username: null,
      err_userinvalid: null,
      err_pass: null,
      err_passinvalid: null,
      err_confirmpass: null,
      err_cpassinvalid: null,

      error: null,
      loading: ''
    };

    trackScreenView('GradSignup1');
  }

  signin = () => {
    this.props.navigation.navigate('login');
  }

  backClick = () => {
    this.props.navigation.navigate('signup_select');
  }
  
  submitForm = () => { 
    let st = this.state;

    this.setState({ error: null, loading: ' is-loading' });

    this.resetErrors();

    let errors = this.validateError();

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    var user = {
      username: st.username,
      password: st.password,
      email: st.username,
      status: 'A',
    };

    store.dispatch(setSignupData(user));

    this.props.navigation.navigate('grad_signup2');
  }

  validateError = () => {
    let errors = 0;
    let st = this.state;
    let err = null;

    //Validate Email
    if (st.username === null || st.username === undefined || st.username == '') {
      errors++;
      this.setState({ err_username: 'Username (Email) is required' });
      if (err === null) err = 'Username (Email) is required';
    }

    if (st.username) {
      if (!Validator.isEmail(st.username)) {
        errors++;
        this.setState({ err_userinvalid: 'Username (Email) is invalid' });
        if (err === null) err = 'Username (Email) is invalid';
      }
    }

    // Validate password
    if (st.password === null || st.password === undefined || st.password == '') {
      errors++;
      this.setState({ err_pass: 'Password is required' });
      if (err === null) err = 'Password is required';
    }

    if (st.password) {
      if (!Validator.isPassword(st.password)) {
        errors++;
        this.setState({ err_passinvalid: 'Password is invalid' });
        if (err === null) err = 'Password is invalid';
      }
    }

    //Confirm password validations
    if (st.confirmpass === null || st.confirmpass === undefined || st.confirmpass == '') {
      errors++;
      this.setState({ err_confirmpass: 'Confirm Password is required' });
      if (err === null) err = 'Confirm Password is required';
    }

    if (st.confirmpass && st.password) {
      if (!Validator.isConfirm(st.password, st.confirmpass)) {
        errors++;
        this.setState({ err_cpassinvalid: 'Password mismatch' });
        if (err === null) err = 'Password mismatch';
      }
    }
    if (errors === 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  resetErrors = () => {
    this.setState({
      err_username: null,
      err_userinvalid: null,
      err_pass: null,
      err_passinvalid: null,
      err_email: null,
      err_emailinvalid: null,
      err_confirmpass: null,
      err_cpassinvalid: null,
      error: null
    });
  }

  render() {
    let st = this.state;

    return (
      <View style={cs.flex_col_center}>

        <DarkGradient />

        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={false} 
          showsVerticalScrollIndicator={false} 
          keyboardShouldPersistTaps='always'>

          <View style={[cs.formbox]}>

            <View style={cs.mt30} />

            <View style={[cs.flex_row_start, cs.mb10]}>
              <TouchableOpacity onPress={this.backClick} style={{ flex: 1 }}>
                <View style={[{ marginTop: 8 }]}>
                  <Image
                    source={require('../../../images/menu-back.png')}
                    style={{ height: 30 }}
                  />
                </View>
              </TouchableOpacity>

              <Text style={[cs.headertext, cs.mb10, { flex: 2 }]}>Sign Up</Text>
            </View>

            <View style={cs.mt8} />
            
            <View style={st.err_username !== null ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Login (Email)'
                style={cs.textInputDark}
                onChangeText={(text) => this.setState({ username: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(255,255,255,0.7)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            <View style={(st.err_pass !== null || st.err_cpassinvalid !== null) ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/padlock.png')}
                style={{ position: 'absolute', bottom: 12, left: 20, width: 17, height: 22 }}
              />
              <TextInput
                placeholder='Password'
                style={cs.textInputDark}
                onChangeText={(text) => this.setState({ password: text })}
                placeholderTextColor='rgba(255,255,255,0.7)'
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize={'none'}
                autoCorrect={false}
                secureTextEntry
              />
            </View>

            <View style={(st.err_confirmpass !== null || st.err_cpassinvalid !== null) ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/padlock.png')}
                style={{ position: 'absolute', bottom: 12, left: 20, width: 17, height: 22 }}
              />
              <TextInput
                placeholder='Confirm Password'
                style={cs.textInputDark}
                placeholderTextColor='rgba(255,255,255,0.7)'
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize={'none'}
                autoCorrect={false}
                secureTextEntry
                onChangeText={(text) => this.setState({ confirmpass: text })}
              />
            </View>

            <View style={[cs.buttonBox, { marginBottom: 8 }]}>

              <Button
                text='Next'
                width={deviceWidth - 55}
                height={44}
                fontSize={18}
                backgroundColor={colors.green}
                onClick={this.submitForm} />

            </View>

            <ErrorBox text={st.error} />

            <View style={cs.p2} />

            {/* Sign up for account */}
            <View style={cs.noteBox}>
              <Text style={cs.signuptext}>
                {'Have an account?'}
              </Text>
              <Text style={cs.signuptext2}>
                <Text
                  style={cs.signuptextlink}
                  onPress={this.signin}>
                  Sign In
              </Text>
              </Text>
            </View>

            <View style={cs.mt60} />
          
          </View>

        </KeyboardAwareScrollView>

      </View>
    );
  }
}

PageGradSignup.propTypes = {
  navigation: PropTypes.object.isRequired
};
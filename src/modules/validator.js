module.exports = {

    isUsername: function (value) {
        return value.match(/^[a-zA-Z]{3,15}$/);
     // return value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    },

    isEmail: function (value) {
        return value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    },

    isNumber: function (value) {
        return value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
    },

    isPassword: function (value) {
        return value.match(/[a-zA-Z_][\w]*/);
    },

    isConfirm: function (value1, value2) {
        return (value1 === value2);
    },
};
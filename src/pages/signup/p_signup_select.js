// react
import React from 'react';
import PropTypes from 'prop-types';

import { Image, View, Text } from 'react-native';

// elements
import DarkGradient from '../../elements/dark_gradient';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';

// styles
import cs from '../../styles/common_styles.js';
import { colors, deviceWidth } from '../../styles/variables';

export default class PageSignupSelect extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      error: null,
      loading: ''
    };
  }

  signin = () => {
    this.props.navigation.navigate('login');
  }

  student_signup = () => {
    this.props.navigation.navigate('student_signup');
  }

  grad_signup = () => {
    this.props.navigation.navigate('grad_signup');
  }

  render() {
    let st = this.state;

    return (
      <View style={cs.flex_col_center}>

        <DarkGradient />

        <View style={[cs.formbox]}>

          <View style={cs.m15} />

          {/* Trifigo Logo */}
          <Image source={require('../../../images/t_icon.png')}
            style={{ width: 95, height: 85, marginTop: 10 }} />

          <Text style={[cs.headertext, cs.m10]}>Sign Up</Text>

          {/* <Text style={[cs.headertextgreen, cs.mb10]}>Sign Up</Text> */}

          <View style={[cs.buttonBox, { marginBottom: 30, marginTop: 10 }]}>

            <Button
              text='Student'
              width={deviceWidth - 55}
              height={44}
              fontSize={18}
              backgroundColor={colors.green}
              onClick={this.student_signup} />

          </View>

          <View style={[cs.buttonBox, { marginBottom: 8 }]}>

            <Button
              text='Graduate'
              width={deviceWidth - 55}
              height={44}
              fontSize={18}
              backgroundColor={colors.white}
              textColor={colors.black}
              onClick={this.grad_signup} />

          </View>

          <ErrorBox text={st.error} />

          <View style={cs.p2} />

          {/* Sign up for account */}
          <View style={cs.noteBox}>
            <Text style={cs.signuptext}>
              {'Have an account?'}
            </Text>
            <Text style={cs.signuptext2}>
              <Text
                style={cs.signuptextlink}
                onPress={this.signin}>
                Sign In
              </Text>
            </Text>
          </View>

          <View style={cs.mt60} />
        </View>

      </View>
    );
  }
}

PageSignupSelect.propTypes = {
  navigation: PropTypes.object.isRequired
};
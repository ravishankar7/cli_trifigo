// react
import React from 'react';
import PropTypes from 'prop-types';

import { View, Image, Text, TouchableOpacity } from 'react-native';

// elements
//import Button from '../../elements/button';
import LightGradient from '../../elements/light_gradient';

// styles
import cs from '../../styles/common_styles.js';
//import { colors } from '../../styles/variables';

export default class PageTourBehavior extends React.Component {

	static navigationOptions = {
		header: null,
	};

	render = () => {
		return (
			<View style={[cs.flex_col_center_top]}>

				<LightGradient />

				<View style={[cs.flex_row_center, cs.ph15, cs.mt10]}>

					<Text style={[cs.title1, cs.mh10, cs.mt7, { flex: 0.4 }]} >
					</Text>

					<View style={{ flex: 1 }} />

					<Text style={[cs.title1, cs.mh10, cs.mt7, { flex: 0.25 }]} onPress={() => this.props.navigation.navigate('credit_profile')}>
						Done
					</Text>
				</View>

				<View style={[cs.flex_row_center, cs.mt10]}>
					<View style={[cs.roundimage]}>
						<Image
							source={require('../../../images/behavior.jpg')}
							style={[{ borderRadius: 135, width: 270, height: 270 }]}
						/>
					</View>
				</View>

				<View style={[cs.flex_row_center, cs.mt10]}>
					<Text style={[cs.logotextlg]}>
						See Credit
					</Text>
				</View>

				<View style={[cs.flex_row_center]}>
					<Text style={[cs.logotextlg]}>
						Behavior
					</Text>
				</View>

				<View style={[cs.flex_row_center, cs.mt10]}>
					<Text style={[cs.title2white, cs.m5]}>
						What does your Credit Report
					</Text>
				</View>

				<View style={[cs.flex_row_center]}>
					<Text style={[cs.title2white, cs.m5]}>
						say about you
					</Text>
				</View>

			</View>
		);
	}
}

PageTourBehavior.propTypes = {
	navigation: PropTypes.object.isRequired
};
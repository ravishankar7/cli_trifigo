//react
import React from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, Text, TouchableOpacity, View, Platform, FlatList } from 'react-native';

import { getUserGoal } from '../../modules/api';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';


// store
import store from '../../redux/store';

// styles
import cs from '../../styles/common_styles';
import { fontFamily, fontSize, deviceWidth, colors } from '../../styles/variables';

import Icon from 'react-native-vector-icons/Ionicons';

import * as Progress from 'react-native-progress';

export default class GoalsProgress extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      my_goals: store.getState().this_user_goals,

      error: null,
      loading: null,
      goals: [
        { id: 0, linked: false, added: false, name: 'Loan', currentAmount: 0, totalAmount: 0, selected: false, icon: 'md-cash' },
        { id: 1, linked: false, added: false, name: 'House', currentAmount: 0, totalAmount: 0, selected: false, icon: 'md-home' },
        { id: 2, linked: false, added: false, name: 'Car', currentAmount: 0, totalAmount: 0, selected: false, icon: 'md-car' },
        { id: 3, linked: false, added: false, name: 'Stocks', currentAmount: 0, totalAmount: 0, selected: false, icon: 'md-trending-up' },
        { id: 4, linked: false, added: false, name: 'College', currentAmount: 0, totalAmount: 0, selected: false, icon: 'md-school' },
        { id: 5, linked: false, added: false, name: 'Credit Card', currentAmount: 0, totalAmount: 0, selected: false, icon: 'ios-card' },
        { id: 6, linked: false, added: false, name: 'Custom', currentAmount: 0, totalAmount: 0, selected: false, icon: 'ios-cog' },
      ]
    };

    trackScreenView('GoalsProgress');
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      my_goals: store.getState().this_user_goals,
    });
  }

  async componentDidMount() {
    await getUserGoal(this.state.user.id);

    this.updateState()
    //console.log('In progress view',this.state.my_goals)

    var thisgoals = this.state.goals
    for (let i = 0; i < thisgoals.length; i++) {
      for (let j = 0; j < this.state.my_goals.length; j++) {
        if (thisgoals[i].name == this.state.my_goals[j].goalType) {
          thisgoals[i].linked = true;
          thisgoals[i].added = true;
          thisgoals[i].totalAmount = (this.state.my_goals[j].targetAmount)
          thisgoals[i].currentAmount = (this.state.my_goals[j].currentAmount)
        }
      }
    }
    this.setState({ goals: thisgoals })
  }

  renderMenuHeader = (title) => {
    return (
      <View style={cs.flex_row_space_between}>
        <Text style={[cs.title1black, cs.m5]}>{title}</Text>
      </View>
    );
  }

  renderTitle = (title) => {
    return (
      <View style={cs.flex_row_space_between}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }

  calcProgress(start, target) {
    //console.log(start,target)
    if (start == 0 || start < 0) {
      return 0
    }
    else {
      var res = start / target
      return res.toFixed(2)
    }
  }
  
  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Set Goals'
          back='goals_detail'
        />

        { /*** Identity ***/}

        <View style={{ flex: 1, marginHorizontal: 6, marginVertical: 8, backgroundColor: 'white', paddingHorizontal: 15, borderRadius: 14 }}>
          <View style={{ width: '100%', height: '100%' }}>


            <View style={{ alignSelf: 'center', paddingVertical: 13 }}>
              {this.renderMenuHeader('Goal Progress')}
            </View>

            <FlatList
              style={{ alignSelf: 'center', width: '100%', }}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              data={this.state.goals}
              //numColumns={2}
              //contentContainerStyle={{alignItems:'center'}}
              renderItem={({ item }) => (

                <View style={{ marginHorizontal: 5, marginVertical: 5, flexDirection: 'row', alignItems: 'center',/* borderColor:'green',borderWidth:1 */ }}>

                  <View style={{ width: '85%', flexDirection: 'row', paddingVertical: 2,/* borderColor:'pink',borderWidth:1, *//* flexWrap:'wrap' */ }}>
                    <View style={{ paddingHorizontal: 3, alignItems: 'center', justifyContent: 'center', width: 40, height: 40, borderRadius: 20, backgroundColor: item.added ? '#3C5468' : 'white', borderWidth: 2, borderColor: item.added ? '#3C5468' : colors.seagreen }}>

                      <Icon name={item.icon} size={24} color={item.added ? 'white' : colors.seagreen} />
                    </View>

                    <View style={{ width: '100%', paddingLeft: 5, justifyContent: 'center' }}>
                      <Text
                        style={{
                          fontSize: 14,
                          fontFamily: fontFamily.semiBold,
                          fontWeight: '500',
                          justifyContent: 'flex-start',
                          color: item.added ? '#2C3E50' : '#e8e8e8',
                          letterSpacing: 0.5,
                        }}>

                        {item.name}
                      </Text>

                      <View style={{ width: '100%' }}>
                        <Progress.Bar progress={this.calcProgress(item.currentAmount, item.totalAmount)} width={null} color={item.added ? colors.seagreen : '#e8ecf1'} height={10} borderRadius={6} unfilledColor='#e8ecf1' borderColor='transparent' />

                        {item.added ?

                          <View style={{ alignSelf: 'flex-end', flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={styles.title}>${item.currentAmount}</Text>
                            <Text style={{ color: colors.darkGray }}> of </Text>
                            <Text style={styles.title}>${item.totalAmount}</Text>
                          </View>
                          :
                          <View style={{ alignSelf: 'flex-end', flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ color: '#e8ecf1' }}> 0 </Text>

                          </View>
                        }
                      </View>
                    </View>
                  </View>

                  {/*item.linked ?

                    <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}>
                      <View style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: '#3C5468', alignItems: 'center', justifyContent: 'center' }}>
                        <Icon name='md-checkmark' size={20} color='white' />
                      </View>
                    </View>
                    :

                    <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'center' }}>

                      <TouchableOpacity onPress={() => console.log('linked')} style={{ paddingHorizontal: 4, padding: 6, borderRadius: 15, borderColor: colors.seagreen, borderWidth: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: colors.seagreen, fontWeight: '400' }}>Link Account</Text>
                      </TouchableOpacity>
                    </View>
                  */}

                </View>
              )} />

            <TouchableOpacity onPress={() => this.props.navigation.navigate('credit_profile')} activeOpacity={0.9} style={{ bottom: 0, marginBottom: 7, marginVertical: 12, width: '100%', paddingVertical: 13, borderRadius: 29, alignSelf: 'center', borderColor: colors.seagreen, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.seagreen }}>
              <Text style={{ color: 'white', fontSize: 17 }}>NEXT</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Footer navigate={navigate} />

      </View>
    );
  }
}

GoalsProgress.propTypes = {
  navigation: PropTypes.object.isRequired
};

GoalsProgress.defaultProps = {
  user: null,
};

const styles = StyleSheet.create({
  imgedit: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: -45,
  },
  subtitletext: {
    color: '#0bb795',
    fontSize: 16,
    letterSpacing: 0.75,
    fontFamily: fontFamily.regular
  },
  subtitletext_s: {
    color: '#0bb795',
    fontSize: 12,
    letterSpacing: 0.75,
    fontFamily: fontFamily.regular
  },
  subcontainer: {
    margin: 1,
    padding: 2,
  },
  subcontainer2: {
    margin: 1,
    marginVertical: 6,
    padding: 6,
    height: 36
  },
  breakline: {
    borderColor: '#E0E0E0',
    borderBottomWidth: 0.8,
  },
  usericon: {
    zIndex: 100,
    marginTop: -30,
    backgroundColor: 'white',
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.1)',
        shadowOffset: {
          width: 0,
          height: 10
        },
        shadowRadius: 6,
        shadowOpacity: 0.4
      },
      android: {
        elevation: 6,
      },
    }),
    borderRadius: 100
  },
  usertype: {
    flex: 1,
    flexDirection: 'row',
    marginTop: -50,
  },
  ct: {
    minWidth: 135,
  },
  ac: {
    minWidth: 135,
  },
  vv: {
    width: 115
  },
  username: {
    marginTop: -10,
    flexDirection: 'row',
    justifyContent: 'center',
    color: '#000',
    fontSize: fontSize.title,
    fontFamily: fontFamily.medium,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.8)',
        shadowOffset: {
          width: 0,
          height: 10
        },
        shadowRadius: 6,
        shadowOpacity: 0.4
      },
      android: {
        elevation: 6,
      },
    }),
    backgroundColor: 'transparent'
  },
  imageBackground: {
    height: 120,
    width: deviceWidth,
  },
  title: {
    fontSize: 14,
    fontFamily: fontFamily.semiBold,
    fontWeight: '500',
    justifyContent: 'flex-start',
    color: '#2C3E50',
    letterSpacing: 0.5,

  }
});



  /*  */
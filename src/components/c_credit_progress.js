// react
import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
//import { Button } from 'react-native-elements';

// api
import { getRandomCreditTip } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import common_styles from '../styles/common_styles';
import Button from '../elements/button';
import { colors, deviceWidth } from '../styles/variables';

const credit_score = require('../../images/credit_page/cashflow.png')
const flow = require('../../images/credit_page/quiz.png')

export default class CreditProgress extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      creditTip: '',

      error: null,
      loading: null,
    };
  }

    render() {
      let st = this.state;

      return (
          <View style={[cs.shadowBox]} >

          <Text style={[cs.title1, cs.mb5]}> Track your progress  </Text>
          <View style={[styles.oval2, cs.flex_center, cs.shadow, { flexDirection: 'row' , padding: 10 } ]} >
              <View style={{ flex: 1 }} >
                  <View style={{ flexDirection: 'column', alignItems:'center' }} >
                      <Image style={{ width: 80, height: 80, marginBottom: 5 }} source={credit_score} />
                      <Text style={{ color: colors.progressBlue, marginBottom: 5 }} >
                          {'Credit Score'}
                      </Text>
                      <Text style={{ color: colors.progressBlue, fontWeight: '600', marginBottom: 5 }} >
                          {'+5'}
                      </Text>
                  </View>
              </View>
              <View style={{ flex: 1 }} >
                  <View style={{ flexDirection: 'column', alignItems:'center' }} >
                      <Image style={{ width: 80, height: 80, marginBottom: 5 }} source={flow} />
                      <Text style={{ color: colors.progressYellow, marginBottom: 5 }} >
                          {'Credit Score'}
                      </Text>
                      <Text style={{ color: colors.progressYellow, fontWeight: '600', marginBottom: 5 }} >
                          {'+5'}
                      </Text>
                  </View>
              </View>
          </View>
            <View style={{ alignSelf: 'center' }} >
              <Button
            //   onClick={() => this.props.navigate('credit_behavior')}
              text='Share'
              width={200}
              height={30}
              fontSize={13}
              backgroundColor={colors.green}
            />
            </View>
            <View  style={{ height: 10 }}/>
          </View>
         
      );
    }
  }

  const styles = StyleSheet.create({
    gradeView: {
        flex: 1,
        backgroundColor: colors.subtleGrey,
        height: 30,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderRadius: 10,
        marginVertical: 10,
    },
    gradeText: {
        fontSize: 20,
        letterSpacing: 2,
        fontWeight: 'bold',
        color: colors.textGrey
    },
    oval2: {
        width: deviceWidth * 90 / 100,
        flex: 1,
        borderRadius: 10,
    },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    }
})
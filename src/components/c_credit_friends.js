// react 
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View } from 'react-native';

// elements
import Button from '../elements/button';

// styles
import cs from '../styles/common_styles';
import { colors } from '../styles/variables';

export default class CreditFriends extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style={[cs.shadowBox, cs.flex_center]}>
      <Text style={[cs.title1, { fontSize: 20 }]}>
          {'45'}
        </Text>
        <Text style={[cs.title1, cs.mb5, { marginTop: -10, fontWeight: 'normal', color: colors.grey, fontSize: 16, letterSpacing: 0 }]}>
          {'Connects'}
        </Text>

        <Button
        //   onClick={() => this.props.navigate('credit_behavior')}
          text='Add Friends'
          width={200}
          height={30}
          fontSize={13}
          backgroundColor='#546E7A'
        />

        <View style={cs.mb10} />

      </View>
    );
  }
}

CreditFriends.propTypes = {
  navigate: PropTypes.func.isRequired
};
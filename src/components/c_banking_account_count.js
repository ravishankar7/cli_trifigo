// react
import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';

// elements
import Button from '../elements/button';

// store
import store from '../redux/store';

// api
import { getQuovoAccountSummary } from '../modules/api';

// style
import cs from '../styles/common_styles.js';

export default class BankingAccountCount extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      quovoAccountSummary: store.getState().quovoAccountSummary,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    //console.log('quovoAccountSummary >>>', store.getState().quovoAccountSummary)
    this.setState({
      user: store.getState().user,
      quovoAccountSummary: store.getState().quovoAccountSummary
    });
  }

  componentDidMount() {
    if (this.state.user === null) return;
    
    let userId = this.state.user.id;
    let _this = this;

    if (!this.state.quovoAccountSummary) {
      try { getQuovoAccountSummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    let _count = 0;
    let _providers = [];

    let st = this.state;

    if (st.quovoAccountSummary) {
      for (var i = 0; i < st.quovoAccountSummary.length; i++) {
        //if (st.quovoAccountSummary[i].includeInNetWorth) {
        _count++;

        if (_providers.indexOf(st.quovoAccountSummary[i].institution_id) > -1) continue;

        _providers.push(st.quovoAccountSummary[i].institution_id);
      }
    }

    if (_count === 0) {
      return (
        <View style={[cs.shadowBox, cs.flex_col_center]}>
          <Text style={[cs.title1, cs.ml15]}>
            No connected accounts
          </Text>
          <View style={styles.buttoncontainer}>
            <Button
              onClick={() => this.props.navigate('link_account')}
              text='Link Accounts'
              width={150}
              height={30}
              fontSize={13}
              backgroundColor='#546E7A'
            />
          </View>
        </View>
      );
    }

    return (
      <View style={[cs.shadowBox, cs.flex_col_center, cs.pb10]}>

        <Text style={[cs.title1]}>
          {_count}
          {' '}
          account{_count > 1 && 's'}{' '}
          from {_providers.length} institution{_providers.length > 1 && 's'}
        </Text>

        <View style={cs.m5} />

        <View style={cs.flex_row_space_between}>

          <Button
            text='Manage'
            width={150}
            height={30}
            fontSize={13}
            backgroundColor='#546E7A'
            onClick={() => this.props.navigate('link_account')}
          />

          {/*
          <View style={cs.m20} />

          <Button
            text='Refresh'
            width={150}
            height={30}
            fontSize={13}
            backgroundColor='#546E7A'
            onClick={() => this.props.navigate('link_account')}
          />
          */}

        </View>

      </View>
    );
  }
}

BankingAccountCount.propTypes = {
  navigate: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
  buttoncontainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    paddingBottom: 10
  },
});
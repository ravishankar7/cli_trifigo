// react
import React from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';

// styles
import cs from '../styles/common_styles';
import { fontFamily } from '../styles/variables';

export default class Button extends React.Component {

	constructor(props) {
		super(props);
	}

	onPress = () => {
		if (this.props.onClick !== null && this.props.onClick !== undefined)
			this.props.onClick();
	}

	render() {
		let pr = this.props;

		const buttonStyle = {
			backgroundColor: this.props.backgroundColor,
			width: this.props.width,
			height: this.props.height,
			borderRadius: this.props.width / 2
		};

		const textStyle = {
			color: this.props.textColor,
			fontSize: this.props.fontSize,
			fontFamily: fontFamily.regular,
			letterSpacing: 2,
		};

		return (
			<TouchableOpacity
				activeOpacity={0.8} 
				onPress={() => this.onPress()}>
				<View style={[cs.shadowDark, styles.button, buttonStyle]}>
					<Text style={[styles.buttontext, textStyle, cs.shadow]}>
						{pr.text}
					</Text>
				</View>
			</TouchableOpacity>
		);
	}
}

Button.propTypes = {
	text: PropTypes.string.isRequired,
	onClick: PropTypes.func,
	width: PropTypes.number,
	height: PropTypes.number,
	fontSize: PropTypes.number,
	backgroundColor: PropTypes.string,
	textColor: PropTypes.string,
	borderColor: PropTypes.string,
	borderWidth: PropTypes.number,
	busy: PropTypes.bool
};

Button.defaultProps = {
	width: 100,
	height: 30,
	fontSize: 14,
	backgroundColor: '#38c4a8',
	textColor: 'white',
	busy: false
};

const styles = StyleSheet.create({
	buttontext: {
		fontFamily: fontFamily.regular,
		margin: 1,
		letterSpacing: 1,
	},
	button: {
		paddingTop: 2,
		justifyContent: 'center',
		alignItems: 'center',
	}
});
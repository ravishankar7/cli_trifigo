import React from 'react';
import PropTypes from 'prop-types';

import { View, Image, TouchableOpacity, AsyncStorage, Text } from 'react-native';

import TouchID from 'react-native-touch-id';

import cs from '../styles/common_styles';

export default class AltLogin extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			supportedMode: null,

			error: null,
			loading: null,
			usefaceid: true
		};
	}

	componentDidMount() {
		let _this = this;

		AsyncStorage.getItem('usefaceid').then((item) => {
			if (item) {
				_this.setState({ usefaceid: item });
			}
		});

		TouchID.isSupported()
			.then(biometryType => {
				if (biometryType === 'TouchID') { // Touch ID is supported on iOS
					_this.setState({ supportedMode: 'touchid' });
				} else if (biometryType === 'FaceID') { // Face ID is supported on iOS
					_this.setState({ supportedMode: 'faceid' });
				} else if (biometryType === true) { // Touch ID is supported on Android
					_this.setState({ supportedMode: 'touchid' });
				}
			})
			.catch(() => {
				//console.log('Touch ID error', error);
				// User's device does not support Touch ID (or Face ID)
				// This case is also triggered if users have not enabled Touch ID on their device
			});
	}

	handleTouchIdLogin = () => {
		if (this.props.onTouchIdLogin) this.props.onTouchIdLogin();
	}

	handleFaceIdLogin = () => {
		if (this.props.onFaceIdLogin) this.props.onFaceIdLogin();
	}

	render() {
		let st = this.state;
		let pr = this.props;

		return (
			<View style={[cs.flex_row_center, cs.p5]}>

				{st.supportedMode &&
				<Text style={[cs.signuptext]}>
					Login with
        </Text>
				}

				{st.usefaceid !== 'false' && pr.showIdLogin && st.supportedMode === 'touchid' &&
					<TouchableOpacity
						activeOpacity={0.8}
						style={[{ backgroundColor: '#fff', borderRadius: 25 }, cs.mh10, cs.shadowDark]}
						onPress={this.handleTouchIdLogin}>
						<Image
							source={require('../../images/icon_touchid.png')}
							style={{ width: 50, height: 50 }}
						/>
					</TouchableOpacity>
				}

				{st.usefaceid !== 'false' && pr.showIdLogin && st.supportedMode === 'faceid' &&
					<TouchableOpacity
						activeOpacity={0.8}
						style={[{ backgroundColor: '#fff', borderRadius: 25 }, cs.mh10, cs.shadowDark]}
						onPress={this.handleFaceIdLogin}>
						<Image
							source={require('../../images/icon_faceid.png')}
							style={{ width: 50, height: 50 }}
						/>
					</TouchableOpacity>
				}
				
			</View>
		);
	}
}

AltLogin.props = {
	onFacebookLogin: PropTypes.func.isRequired,
	onGoogleLogin: PropTypes.func.isRequired,
	onFaceIdLogin: PropTypes.func,
	onTouchIdLogin: PropTypes.func,
	onError: PropTypes.func,
	showIdLogin: PropTypes.bool
};

AltLogin.defaultProps = {
	onFacebookLogin: null,
	onGoogleLogin: null,
	onError: null,
	showIdLogin: false,
	onTouchIdLogin: null
};

/*
const { type, user } = await Google.logInAsync({
	androidStandaloneAppClientId: '<ANDROID_CLIENT_ID>',
	iosStandaloneAppClientId: '<IOS_CLIENT_ID>',
	androidClientId: '1059632528681-20c4gg5d0rvfqhu3d6au058oniashbst.apps.googleusercontent.com',
	iosClientId: '1059632528681-qq9nev6flho4qopd7h1gs1epob5n0q37.apps.googleusercontent.com',
	scopes: ['profile', 'email']
});
*/

// FB Key hash kGx7o6QmikMYgfHYMk3cKR6hvNs=
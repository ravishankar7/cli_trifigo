//react
import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, ScrollView } from 'react-native';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import OkBox from '../../elements/ok_box';

// store
import store from '../../redux/store';

// api
import {
  refreshQuovoAccounts,
  refreshQuovoTransactions,

  deleteAccountAgeSummary,
  deleteAccountMixSummary,
  deleteAdditionalCreditSummary,
  deleteBankingTotalsSummary,
  // deleteCashFlowSummary,   //No api seems to be available for this in the /modules/api
  deleteConsumerTypeSummary,
  deleteCreditInquirySummary,
  deleteCreditUtilizationSummary,
  deleteOtherFactorsSummary,
  deletePaymentsSummary,

  runEngineForUser
}
from '../../modules/api';

// styles
import cs from '../../styles/common_styles';

export default class PageSettingsRefresh extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;

    this.state = {
      user: _user,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
      success: null,
    };

    trackScreenView('Settings Refresh');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  cancel = () => {
    this.props.navigation.navigate('settings');
  }

  refresh = () => {
    let _this = this;
    let userId = this.state.user.id;

    refreshQuovoAccounts(userId);
    refreshQuovoTransactions(userId);

    deleteAccountAgeSummary(userId);
    deleteAccountMixSummary(userId);
    deleteAdditionalCreditSummary(userId);
    deleteBankingTotalsSummary(userId);
    // deleteCashFlowSummary(userId);
    deleteConsumerTypeSummary(userId);
    deleteCreditInquirySummary(userId);
    deleteCreditUtilizationSummary(userId);
    deleteOtherFactorsSummary(userId);
    deletePaymentsSummary(userId);

    runEngineForUser(userId);

    _this.setState({ success: 'Refreshed' }, () => {
      _this.timer = setInterval(() => {
        const { navigate } = this.props.navigation;
        clearInterval(_this.timer);
        navigate('settings');
      }, 1000);
    });
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Settings'
          back='settings'
        />

        <ScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center, cs.mt30]}>

            <Text style={[cs.headertextdarkgray, cs.mb10]}>Refresh Data</Text>

            <View style={[cs.bluecontainer, cs.mb20]}>

              <Text style={cs.subtitle2white}>
                We will refresh all your financial data and re run our algorithms.
              </Text>

              <View style={cs.m5} />

              <Text style={cs.subtitle2white}>
                This might take a minute or so.
              </Text>

              <View style={cs.m5} />

              <Text style={cs.subtitle2white}>
                We are ready... click Refresh when you are.
              </Text>

            </View>

            <View style={[cs.flex_row_center, cs.mb10]}>

              <View style={cs.m5}>
                <Button
                  text='Cancel'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#CFD8DC'
                  textColor='#404040'
                  onClick={this.cancel} />
              </View>

              <View style={cs.m5}>
                <Button
                  text='Refresh'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#66BB6A'
                  onClick={this.refresh} />
              </View>

            </View>

            <View style={[cs.flex_row_center, cs.mb10]}>

              {st.error !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              }

              {st.success !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              }

            </View>

          </View>

        </ScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsRefresh.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettingsRefresh.defaultProps = {
  user: null,
};

// react
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, Image, Platform, TouchableOpacity } from 'react-native';
import { Avatar } from 'react-native-elements';

// elements
import Button from '../elements/button';

// components
import ConsumerType from '../components/c_consumer_type';
import AddtionalCredit from '../components/c_addtional_credit';
import CreditRank from '../components/c_credit_rank';
import SocialRank from '../components/c_social_rank';

// store
import store from '../redux/store';

// styles
import cs from '../styles/common_styles.js';
import { fontFamily, fontSize, deviceWidth, colors } from '../styles/variables';

export default class UserInfo extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      connectionStatus : store.getState().connectionStatus,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }
  
  updateState = () => {
    this.setState({
      user: store.getState().user,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    let st = this.state;
    let pr = this.props;

    let user = st.user;

    if (user === null || user === undefined) {
      return (
        <View style={cs.flex_center}>
        </View>
      );
    }

    let profileImgUrl = 'https://s3-us-west-2.amazonaws.com/tfgimages/null/ppic.png';

    if (user.profileImgUrl !== null && user.profileImgUrl !== undefined) {
      profileImgUrl = user.profileImgUrl;
    }

    let coverImgUrl = 'https://s3-us-west-2.amazonaws.com/tfgimages/null/bg.png';

    if (user.coverImgUrl !== null && user.coverImgUrl !== undefined) {
      coverImgUrl = user.coverImgUrl;
    }

    return (
      <View style={cs.flex_center}>

        <TouchableOpacity activeOpacity={0.9}
          // onPress={() => this.props.navigate('settings_images')}
          >
          <View>
            <Image
              source={{ uri: coverImgUrl }}
              style={styles.imageBackground} />
          </View>
        </TouchableOpacity>
      
        <TouchableOpacity activeOpacity={0.9} style={styles.usericon}
          onPress={() => this.props.navigate('settings_images', { image : profileImgUrl })}>
          <View>
            <Avatar
              width={105}
              height={105}
              rounded
              source={{ uri: profileImgUrl }}
              activeOpacity={0.7}
            />
          </View>
        </TouchableOpacity>

        {pr.bankinginfo &&
          <View style={styles.usertype}>
            <ConsumerType style={styles.ct} />
            <View style={styles.vv}> </View>
            <AddtionalCredit style={styles.ac} />
          </View>
        }
        
        {pr.creditinfo &&
          <View style={[styles.usertype, styles.userType1, { justifyContent: 'space-evenly', alignItems: 'center', borderColor: 'red' } ]}>
            <CreditRank style={styles.ct} />
            <View>
            <Text style={[ { paddingTop: 90, flex:1, fontSize: 12, color: 'black', fontWeight: 'bold', textAlign:'center' } ]}>
          {user.firstName} {user.lastName}
        </Text>
            </View>
            <SocialRank style={styles.ac} />
          </View>
        }

        {pr.bankinginfo &&
          <View>
            <Button
              onClick={() => this.props.navigate('link_account')}
              text='Link Accounts'
              width={250}
              height={36}
              fontSize={16}
              backgroundColor='#1ABC9C'
            />
            <View style={cs.m10} />
          </View>
        }
        
        {/* {pr.creditinfo &&
          <View>
            <Button
              onClick={() => this.props.navigate('upload_report')}
              text='Upload Report'
              width={250}
              height={36}
              fontSize={16}
              backgroundColor={colors.green}
            />
            <View style={cs.m10} />
          </View>
        } */}

      </View>
    );
  }
}

UserInfo.propTypes = {
  navigate: PropTypes.func.isRequired
};

UserInfo.defaultProps = {
  user: null,
};

const styles = StyleSheet.create({
  usericon: {
    zIndex: 100,
    marginTop: -50,
    backgroundColor: 'white',
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.1)',
        shadowOffset: {
          width: 0,
          height: 10
        },
        shadowRadius: 6,
        shadowOpacity: 0.4
      },
      android: {
        elevation: 6,
      },
    }),
    borderRadius: 100
  },
  usertype: {
    flex: 1,
    flexDirection: 'row',
    marginTop: -75,
  },
  userType1: {
    backgroundColor: colors.white,
    height: 120,
    width: deviceWidth,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOffset: {
          width: 0,
          height: 5
        },
        shadowRadius: 5,
        shadowOpacity: 0.4
      },
      android: {
        elevation: 5,
      },
    })
  },
  ct: {
    minWidth: 135,
  },
  ac: {
    minWidth: 135,
  },
  vv: {
    width: 115
  },
  username: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    color: '#000',
    fontSize: fontSize.title,
    fontFamily: fontFamily.medium,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.8)',
        shadowOffset: {
          width: 0,
          height: 10
        },
        shadowRadius: 6,
        shadowOpacity: 0.4
      },
      android: {
        elevation: 6,
      },
    }),
    backgroundColor: 'transparent'
  },
  imageBackground: {
    height: 135,
    width: deviceWidth,
  }
});
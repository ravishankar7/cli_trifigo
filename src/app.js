// react
import React from 'react';
import { View, StatusBar, Animated, Easing, AsyncStorage,StyleSheet, Button, ActivityIndicator, NetInfo, PanResponder, Text, Modal, TouchableHighlight } from 'react-native';

import { createStackNavigator , createSwitchNavigator, createAppContainer} from 'react-navigation';

import SplashScreen from 'react-native-splash-screen';

//import Landing from './pages/p_landing.js';
import Login from './pages/p_login.js';
import Signup from './pages/signup/p_signup.js';
import SignupSelect from './pages/signup/p_signup_select.js';
import StudentSignup from './pages/signup/p_student_signup.js';
import StudentSignup2 from './pages/signup/p_student_signup_2.js';
import GradSignup from './pages/signup/p_grad_signup.js';
import GradSignup2 from './pages/signup/p_grad_signup_2.js';

// forgot password pages
import ForgotPass from './pages/password/p_forgot_pass';
import ForgotPassToken from './pages/password/p_forgot_pass_token';
import ForgotPassReset from './pages/password/p_forgot_pass_reset';

// tour pages
import TourWelcome from './pages/tour/p_tour_welcome.js';
import TourAddPhoto from './pages/tour/p_tour_add_photo';
import TourChallenge from './pages/tour/p_tour_challenge';	
import TourBehavior from './pages/tour/p_tour_bhavior';
import TourGoals from './pages/tour/p_tour_goals';

// goals pages 
import GoalsLink from './pages/goals/p_goals_link';
import GoalsDetails from './pages/goals/p_goal_detail';
import GoalsProgress from './pages/goals/p_goals_progress';
import GoalsSelect from './pages/goals/p_select_goals';

import BankingProfile from './pages/p_banking_profile.js';
import CreditProfile from './pages/p_credit_profile.js';
import Feed from './pages/p_feed.js';
import Offers from './pages/p_offers.js';
import LinkAccount from './pages/p_link_account.js';
import Recos from './pages/p_recos.js';
import CreditBehavior from './pages/p_credit_behavior';
import UploadReport from './pages/p_upload_report';

import Settings from './pages/settings/p_settings';
import SettingsName from './pages/settings/p_settings_name';
import SettingsPrivacy from './pages/settings/p_settings_privacy';
import SettingsAddress from './pages/settings/p_settings_address';
import SettingsPhone from './pages/settings/p_settings_phone';
import SettingsEmail from './pages/settings/p_settings_email';
import SettingsDOB from './pages/settings/p_settings_dob';
import SettingsWorkEmployer from './pages/settings/p_settings_work_employer';
import SettingsSchool from './pages/settings/p_settings_school';
import SettingsHelp from './pages/settings/p_settings_help';
import SecurityQuestions from './pages/settings/p_security_questions';
import SettingsRefresh from './pages/settings/p_settings_refresh';
import SettingsImages from './pages/settings/p_settings_images';

import Camera from './pages/p_camera';
import Feedback from './pages/p_feedback';

import { YellowBox } from 'react-native';

// styles
import cs from './styles/common_styles.js';

import OneSignal from 'react-native-onesignal';

// redux
import store from './redux/store';
import { setOneSignalDevice, setNetworkStatus } from './redux/actions';
import { deviceWidth, fontFamily } from './styles/variables.js';

import { PushNotificationIOS } from 'react-native';
import Amplify from 'aws-amplify';
import Analytics from '@aws-amplify/analytics';	
import PushNotification from '@aws-amplify/pushnotification';

import awsconfig from './../aws-exports';

// send analytics events to Amazon Pinpoint
Amplify.configure(awsconfig);
// Analytics.configure(awsconfig);
PushNotification.configure(awsconfig);

function fromLeft(duration = 300, nav) {
	console.log('SCENS', nav.scene.route.routeName )
	if (nav.scene.route.routeName === 'settings') {
		return {
    transitionSpec: {
      duration,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: ({ layout, position, scene }) => {
      const { index } = scene;
      const { initWidth } = layout;

      const translateX = position.interpolate({
        inputRange: [index - 1, index, index + 1],
        outputRange: [-initWidth, 0, 0],
      });

      const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1],
        });

      return { opacity, transform: [{ translateX }] };
    },
  }
	} 
	
	if (nav.scene.route.routeName === 'credit_profile') {
		return {
    transitionSpec: {
      duration,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: ({ layout, position, scene }) => {
      const { index } = scene;
      const { initWidth } = layout;

      const translateX = position.interpolate({
        inputRange: [index - 1, index, index + 1],
        outputRange: [-initWidth, 0, 0],
      });

      const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1],
        });

      return { opacity, transform: [{ translateX }] };
    },
  }
	}
  
}

function fromRight(duration = 300) {
  return {
    transitionSpec: {
      duration,
      easing: Easing.out(Easing.poly(4)),
      timing: Animated.timing,
      useNativeDriver: true,
    },
    screenInterpolator: ({ layout, position, scene }) => {
      const { index } = scene;
      const { initWidth } = layout;

      const translateX = position.interpolate({
        inputRange: [index - 1, index, index + 1],
        outputRange: [initWidth, 0, 0],
      });

      const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1],
        });

      return { opacity, transform: [{ translateX }] };
    },
  };
}

export default class App extends React.Component {

	constructor(props) {
		super()
		let _user = store.getState().user;
		this.handleAnalyticsClick = this.handleAnalyticsClick.bind(this);

		this.state = {
			isLoggedIn : false,
			inactive: true,
			modalVisible: false,
			connectionInfo: '',
			user : _user,
			resultHtml: <Text></Text>, 
			eventsSent: 0
		}
		// this.handleFirstConnectivityChange = this.handleFirstConnectivityChange.bind(this);
	}
	
	// handleFirstConnectivityChange(connectionInfo) {
	// 	this.setState({
	// 		connectionInfo: connectionInfo.type
	// 	})
		
	// 	console.log('First change, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);

	// }

handleAnalyticsClick() {
      Analytics.record('AWS Amplify Event')
        .then( (evt, rj) => {
					console.log('Event >>>', evt, rj )
            const url = 'https://'+awsconfig.aws_project_region+'.console.aws.amazon.com/pinpoint/home/?region='+awsconfig.aws_project_region+'#/apps/'+awsconfig.aws_mobile_analytics_app_id+'/analytics/events';
            let result = (
              <View>
                <Text>Event Submitted.</Text>
                <Text>Events sent: {++this.state.eventsSent}</Text>
                <Text style={styles.link} onPress={() => Linking.openURL(url)}>
                  View Events on the Amazon Pinpoint Console
                </Text>
              </View>
            );
            this.setState({
                'resultHtml': result
            });
				});
    };
	
	componentDidMount() {

		PushNotification.onNotification((notification) => {
			console.log('in app notification', notification);
			notification.finish(PushNotificationIOS.FetchResult.NoData);
		});
		
		console.log('ENTER CDM >>>')
		PushNotification.onRegister((token) => {
			console.log('in app registration', token);
		});
		
		try {
			// init onesignal notifications
			OneSignal.init("f74bad3b-8a18-4cc5-a933-0c24e188ccf0");
			OneSignal.configure();
			//OneSignal.addEventListener('received', this.onReceived);
			//OneSignal.addEventListener('opened', this.onOpened);
			OneSignal.addEventListener('ids', this.onIds);
		} catch (error) {
			//console.error('error >>>', error);
		}
		
		//network status check
		// NetInfo.getConnectionInfo().then((connectionInfo) => {
		// 	this.setState({
		// 		connectionInfo: connectionInfo.type
		// 	})
		// 	setNetworkStatus(connectionInfo.type != 'none' ? true : false)
		// 	//console.log('Initial, type: ' + connectionInfo.type + ', effectiveType: ' + connectionInfo.effectiveType);
		// });

		// NetInfo.addEventListener(
		// 	'connectionChange',
		// 	this.handleFirstConnectivityChange
		// );
		
		// hide splash screen
		SplashScreen.hide();
		
	}
	
	componentWillUnmount() {
		// OneSignal.removeEventListener('received', this.onReceived);
		// OneSignal.removeEventListener('opened', this.onOpened);
		OneSignal.removeEventListener('ids', this.onIds);
		// NetInfo.removeEventListener(
		// 	'connectionChange',
		// 	handleFirstConnectivityChange
		// );
	}

	// onReceived(notification) {
	// 	console.log("Notification received: ", notification);
	// }

	// onOpened(openResult) {
	// 	console.log('Message: ', openResult.notification.payload.body);
	// 	console.log('Data: ', openResult.notification.payload.additionalData);
	// 	console.log('isActive: ', openResult.notification.isAppInFocus);
	// 	console.log('openResult: ', openResult);
	// }

	onIds = (device) => {
		console.log('ONE SIGNAL TOKEN ID >>>', device)
		store.dispatch(setOneSignalDevice(device));
	}

	render() {
		let { st } = this.state
		// Get rid of the warning for Debugger in background window globally
		/* eslint-disable no-console */
		console.ignoredYellowBox = ['Remote debugger'];
		YellowBox.ignoreWarnings([
			'Warning: componentWillMount is deprecated',
			'Warning: componentWillReceiveProps is deprecated',
			'Remote debugger'
		]);
		// /* eslint-disable no-console */
		// const Layout = this.state.isLoggedIn ? Main : Authentication

		if(0) {
			return (
				<View style={styles.container}>
          <Text>Welcome to your React Native App with Amplify!</Text>
          <Button title="Generate Analytics Event" onPress={this.handleAnalyticsClick} />
          {this.state.resultHtml}
        </View>
			)
		}

		return (
			<View style={cs.flex1} >
				<StatusBar hidden />
				<Main />
			</View>
		);
	}
}

const MainStack = createStackNavigator({
	//landing: { screen: Landing },
	login: { screen: Login },
	signup: { screen: Signup },
	signup_select: { screen: SignupSelect },
	student_signup: { screen: StudentSignup },
	student_signup2: { screen: StudentSignup2 },
	grad_signup: { screen: GradSignup },
	grad_signup2: { screen: GradSignup2 },

	forgot_pass: { screen: ForgotPass },
	forgot_pass_token: { screen: ForgotPassToken },
	forgot_pass_reset: { screen: ForgotPassReset },

	tour_welcome: { screen: TourWelcome },
	tour_add_photo: { screen: TourAddPhoto },
	tour_credit_challenge: { screen: TourChallenge },
	tour_goals: { screen: TourGoals },
	tour_credit_behaviour: { screen: TourBehavior },

	goals_link : { screen: GoalsLink},
	goals_detail : { screen: GoalsDetails},
	goals_progress : {screen : GoalsProgress},
	goals_select :{ screen: GoalsSelect},

	banking_profile: { screen: BankingProfile },
	credit_profile: { screen: CreditProfile },
	feed: { screen: Feed },
	offers: { screen: Offers },
	link_account: { screen: LinkAccount },
	recos: { screen: Recos },
	credit_behavior: { screen: CreditBehavior },
	upload_report: { screen: UploadReport },
	camera: { screen: Camera },
	feedback: { screen: Feedback },
	settings: { screen: Settings },
	settings_name: { screen: SettingsName },
	settings_address: { screen: SettingsAddress },
	settings_phone: { screen: SettingsPhone },
	settings_privacy: { screen: SettingsPrivacy },
	settings_refresh: { screen: SettingsRefresh },
	settings_dob: { screen: SettingsDOB },
	settings_images: { screen: SettingsImages },
	settings_school: { screen: SettingsSchool },
	settings_help: { screen: SettingsHelp },
	settings_work_employer: { screen: SettingsWorkEmployer },
	security_questions: { screen: SecurityQuestions },
	settings_email: { screen: SettingsEmail },
},
{		
		//======> Animations Disabled
		// transitionConfig: () => ({
		// 	transitionSpec: {
		// 		duration: 0,
		// 	},
		// }),
		//======> Animations Disabled
		transitionConfig: (nav) => fromLeft(300, nav),
		navigationOptions : {
			gesturesEnabled : false
		}
		
});

const Main = createAppContainer(MainStack)

const styles = StyleSheet.create({
	container: {
		flex : 1,
	},
	modal: {
	   flex: 1,
	   justifyContent : 'center',
	   alignItems: 'center',
	   backgroundColor: 'rgba(0,0,0,0.1)',
	   padding: 100
	},
	text: {
	   color: '#3f2949',
	   marginTop: 10
	},
	bottomModal: {
		backgroundColor: 'rgba(255,255,255,1)',
		justifyContent: "center",
		alignItems : 'center',
		width : deviceWidth - 40,
		height : 200,
		borderRadius : 25
	},
	  modalContent: {
		backgroundColor: 'rgba(0,0,0,0.5)',
		padding: 22,
		justifyContent: "center",
		alignItems: "center",
		borderRadius: 4,
		borderColor: "rgba(0, 0, 0, 0.1)"
	  },
	  subtitletext_s: {
    color: '#0bb795',
    fontSize: 12,
    letterSpacing: 0.75,
    fontFamily: fontFamily.regular
  },
  subcontainer: {
    margin: 1,
    padding: 2,
  },
});

/*
transitionConfig: () => ({
		screenInterpolator: sceneProps => {
			const { position, scene } = sceneProps;

			const thisSceneIndex = scene.index;

			const opacity = position.interpolate({
				inputRange: [thisSceneIndex - 1, thisSceneIndex],
				outputRange: [0.9, 1],
			});
			return { opacity };
		}
	}),
*/
//https://medium.com/async-la/custom-transitions-in-react-navigation-2f759408a053

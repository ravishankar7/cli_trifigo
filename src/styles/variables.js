import {
	Dimensions,
	Platform
} from 'react-native';

const NAV_HEIGHT = 45;
const TAB_HEIGHT = 50;
const dimen = Dimensions.get('window');
const STATUSBAR_HEIGHT = (Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS && (dimen.height === 812 || dimen.width === 812)) ? 22 : 0;
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
const shadowOpt = {
	btnWidth: deviceWidth - 55,
	btnHeight: 45
};

// Only for FindDoctors, FindHospital, Appointment screens
const spaceHeight = deviceHeight - 375 - 45;
// Only for Intro screens
const introSpaceHeight = deviceHeight - 364;

// Common gradient colors
const blueGradient = {
	colors: ['#3C5468', '#003152'],
};

const colors = {
	white: '#fff',
	offwhite: '#efefef',
	lightbluebg: '#EBF5FB',
	black: 'rgb(19,19,19)',
	darkWhite: 'rgba(255,255,255,0.6)',
	grey: '#404040',
	greyButton: '#546E7A',
	lightGrey: 'rgb(150,150,150)',
	subtleGrey: 'rgba(246, 246, 246, 1)',
	textGrey: 'rgba(202, 202, 202,1)',
	softBlue: 'rgb(75,102,234)',
	progressBlue: 'rgba(2, 168, 182,1)',
	progressYellow: 'rgba(245, 166, 35,1)',
	darkSkyBlue: 'rgb(63,103,230)',
	periBlue: 'rgb(79,109,230)',
	red: 'rgb(255,16,0)',
	borderColor: 'rgb(229,229,229)',
	green: '#17B890', //'#16A085',//'#1bb690',
	errorBorder: '#F50046',
	darkGray: '#404040',
	gold: 'rgba(220, 160, 0, 1)',
	//flat colors
	seagreen: '#14b7ac',//'#16A085',
	nephritis: '#27AE60',
};

let fontSize = {
	extraLarge: 32,
	title: 30,
	header: 18,
	itemHeader: 17,
	medium: 16,
	normal: 15,
	small: 13,
	xsmall: 12,
	xxsmall: 11
};

const fontFamily = {
	light: 'Poppins-Light',
	regular: 'Poppins-Regular',
	medium: 'Poppins-Medium',
	semiBold: 'Poppins-SemiBold',
	extraBold: 'Poppins-Bold',
};

let lineHeight = {
	title: 38,
	header: 30,
	itemHeader: 29,
	normal: 23,
	small: 30
};

if (deviceWidth <= 320) {
	fontSize = {
		extraLarge: 27,
		title: 20,
		header: 16,
		itemHeader: 14,
		medium: 12,
		normal: 11,
		small: 10
	};

	lineHeight = {
		title: 28,
		header: 20,
		itemHeader: 19,
		normal: 13,
		small: 20
	};
}

export {
	NAV_HEIGHT,
	TAB_HEIGHT,
	STATUSBAR_HEIGHT,
	deviceHeight,
	deviceWidth,
	shadowOpt,

	spaceHeight,
	introSpaceHeight,

	blueGradient,
	colors,
	fontSize,
	fontFamily,
	lineHeight,
};

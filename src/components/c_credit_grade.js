// react 
import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { Text, View , StyleSheet} from 'react-native';

// elements
import Button from '../elements/button';

// styles
import cs from '../styles/common_styles';
import { colors, deviceWidth } from '../styles/variables';
import common_styles from '../styles/common_styles';

export default class CreditGrade extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    return (
      <View style={[cs.shadowBox, cs.flex_center]}>
        <View style={[common_styles.flex_row_space_between, { alignItems : 'center' } ]} >
        <Text style={[cs.title1, cs.m5, { color: colors.black, letterSpacing: 2, fontSize: 16 }]}>
          {'Credit Grade'}
        </Text>
        <View style={styles.gradeView} >
            <Text style={styles.gradeText} >
                {'A'}
            </Text>
            <Text style={styles.gradeText} >
                {'B'}
            </Text>
            <Text style={styles.gradeText} >
                {'C'}
            </Text>
            <View style={{width: 40, height: 40, borderRadius: 20, backgroundColor: colors.gold, justifyContent: 'center', alignItems: 'center' }} >
                <Text style={[styles.gradeText, { color: colors.white, fontSize: 30, marginLeft: 4 } ]} >
                {'D'}
            </Text>
            </View>
            <Text style={styles.gradeText} >
                {'E'}
            </Text>
        </View>

        </View>

        <View style={[styles.oval2, cs.flex_center, cs.shadow, { flexDirection: 'row' , padding: 10 } ]} >
        <View style={styles.iconContainer} >
            <Icon 
            name='information-variant'
            color={colors.green}
            size={30}
            />
        </View>
          <Text style={[{ color: colors.white, fontWeight: 'bold' }]} > Explained:
              <Text style={[{ color: colors.white , fontWeight:'normal', flex:1, flexWrap: 'wrap'}]}>
                {' Low credit history and high credit utilization'}
              </Text>
            </Text>
        </View>

        <View style={cs.mb10} />

      </View>
    );
  }
}

CreditGrade.propTypes = {
  navigate: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    gradeView: {
        flex: 1,
        backgroundColor: colors.subtleGrey,
        height: 30,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        borderRadius: 10,
        marginVertical: 10,
    },
    gradeText: {
        fontSize: 20,
        letterSpacing: 2,
        fontWeight: 'bold',
        color: colors.textGrey
    },
    oval2: {
        width: deviceWidth * 90 / 100,
        flex: 1,
        borderRadius: 10,
        backgroundColor: '#38c4a8', //'#0bb795'
    },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    }
})
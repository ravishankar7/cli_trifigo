// react
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Platform, WebView } from 'react-native';

// google analytics
import { trackScreenView } from '../modules/ga';

// elements
import Header from '../elements/header';
import Footer from '../elements/footer';

// store
import store from '../redux/store';

// styles
import cs from '../styles/common_styles.js';
import { deviceWidth, deviceHeight } from '../styles/variables';

export default class PageFeedback extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: '',
    };

    trackScreenView('Feedback');
  }

  updateState = () => {
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    const { navigate } = this.props.navigation;
    // const navigateTo = this.props.navigation.getParam('navigateTo', 'banking_profile')
    const navigateTo = this.props.navigation.getParam('navigateTo', 'credit_profile')
    let scaleFit = false;

    if (Platform.OS === 'ios')
      scaleFit = false;
    else
      scaleFit = true;

    let url = 'https://www.surveymonkey.com/r/N8YGSXR';

    return (
      <View style={cs.normalPage}>

        <Header
          // menu
          back = { navigateTo }
          title='Feedback'
          navigate={navigate}
        />

        <WebView
          //ref={webview => { this.myWebView = webview; }}
          //onMessage={(e) => this._onMessage(e)}
          style={styles.webview}
          source={{ uri: url }}
          javaScriptEnabled
          domStorageEnabled
          scalesPageToFit={scaleFit}
        />

        {/* <Footer navigate={navigate} /> */}

      </View>
    );
  }
}

PageFeedback.propTypes = {
  navigation: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  webview:
    {
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
      width: deviceWidth,
      height: deviceHeight
    },
});
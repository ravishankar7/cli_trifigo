// react
import React from 'react';
import { Text, View } from 'react-native';

// styles
import cs from '../styles/common_styles';

export default class MarketUpdate extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={cs.shadowBox}>

        <Text style={[cs.title1, cs.mb10]}>Market Update</Text>

        <View style={cs.flex_row_space_between}>
          <Text style={cs.subtitle2gray}>Avg Credit Card Rate</Text>
          <Text style={cs.subtitle2green}>15%</Text>
        </View>

        <View style={cs.flex_row_space_between}>
          <Text style={cs.subtitle2gray}>Avg Mortgage Rate</Text>
          <Text style={cs.subtitle2green}>5%</Text>
        </View>
        
        <View style={cs.flex_row_space_between}>
          <Text style={cs.subtitle2gray}>Avg Auto Loan Rate</Text>
          <Text style={cs.subtitle2green}>10%</Text>
        </View>

      </View>
    );
  }
}
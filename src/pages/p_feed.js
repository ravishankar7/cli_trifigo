import React from 'react';
import PropTypes from 'prop-types';

import { View, ScrollView } from 'react-native';

// google analytics
import { trackScreenView } from '../modules/ga';

import cs from '../styles/common_styles.js';

import Header from '../elements/header';
import Footer from '../elements/footer';

import FeedElements from '../components/c_feed_elements';
import FeedElements2 from '../components/c_feed_elements2';
import FeedElements3 from '../components/c_feed_elements3';

export default class PageFeed extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      error: null,
      loading: '',
    };
    
    trackScreenView('Feed');
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={cs.normalPage}>

        <Header
          menu
          title='News Feed'
          navigate={navigate}
        />

        <ScrollView keyboardShouldPersistTaps='always'>

          <View style={cs.mt10} />

          <FeedElements />

          <FeedElements2 />

          <FeedElements3 />

        </ScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageFeed.propTypes = {
  navigation: PropTypes.object.isRequired
};
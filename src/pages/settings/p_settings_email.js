//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View, Image, TextInput } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import OkBox from '../../elements/ok_box';

// store
import store from '../../redux/store';

// api
import { updateUser } from '../../modules/api';

// styles
import cs from '../../styles/common_styles';
import Spinner from 'react-native-spinkit';
import { colors } from '../../styles/variables';

export default class PageSettingsEmail extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;
    this.state = {
      user: _user,

      unsubscribe: store.subscribe(this.updateState),

      email1: _user !== null ? _user.email1 : null,
      err_email1: null,

      email2: _user !== null ? _user.email2 : null,
      err_email2: null,

      error: null,
      loading: null,
      success: null,
    };

    trackScreenView('Settings Email');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,

      email1: _user !== null ? _user.email1 : null,
      err_email1: null,

      email2: _user !== null ? _user.email2 : null,
      err_email2: null,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  resetErrors = () => {
    this.setState({
      loading: null,
      error: null,
      success: null,

      err_email1: null,
      err_email2: null,
    });
  }

  validateData = () => {
    this.resetErrors();

    let errors = 0;
    let err = null;

    let st = this.state;

    // Validate name
    if (st.email1 === null || st.email1 === undefined || st.email1 == '') {
      errors++;
      this.setState({ err_email1: 'Email 1 required.' });
      if (err === null) err = 'Email 1 required.';
    }

    // Validate last name
    if (st.email2 === null || st.email2 === undefined || st.email2 == '') {
      errors++;
      this.setState({ err_email2: 'Email 2 required' });
      if (err === null) err = 'Email 2 required';
    }

    if (errors == 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  saveData = () => {
    let errors = this.validateData();

    this.setState({
      loading : 'loading'
    })

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    // save data
    let _this = this;
    let st = this.state;

    let userId = this.state.user.id;
    let user = this.state.user;

    user.email1 = st.email1;
    user.email2 = st.email2;

    try {
      updateUser(userId, user);
      _this.setState({ success: 'Saved' }, () => {
        _this.timer = setInterval(() => {
          const { navigate } = this.props.navigation;
          clearInterval(_this.timer);
          navigate('settings');
        }, 1000);
      });
    }
    catch (e) { _this.setState({ error: e }); }
  }

  cancel = () => {
    this.props.navigation.navigate('settings');
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Settings'
          back='settings'
        />

        <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center, cs.mt30]}>

            <Text style={[cs.headertextdarkgray, cs.mb10]}>Email</Text>

            {/* Name textbox */}
            <View style={st.err_email1 !== null ? cs.textInputFieldError : cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Email 1'
                value={st.email1}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ email1: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            {/* Last Name textbox */}
            <View style={st.err_email2 !== null ? cs.textInputFieldError : cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Email 2'
                value={st.email2}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ email2: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            { st.loading 
            ?
            <Spinner
                  isVisible
                  size={60}
                  type='ThreeBounce'
                  color={colors.green} />
            :
             <View style={[cs.flex_row_center, cs.mb10]}>

              <View style={cs.m5}>
                <Button
                  text='Cancel'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#CFD8DC'
                  textColor='#404040'
                  onClick={this.cancel} />
              </View>

              <View style={cs.m5}>
                <Button
                  text='Save'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#66BB6A'
                  onClick={this.saveData} />
              </View>

            </View>}

            <View style={[cs.flex_row_center, cs.mb10]}>

              {st.error &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              }

              {st.success &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              }

            </View>

          </View>

        </KeyboardAwareScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsEmail.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettingsEmail.defaultProps = {
  user: null,
};

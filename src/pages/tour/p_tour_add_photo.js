// react
import React from 'react';
import PropTypes from 'prop-types';

import { View, Image, Text } from 'react-native';

// elements
import LightGradient from '../../elements/light_gradient';
//import Button from '../../elements/button';

// styles
import cs from '../../styles/common_styles';
//import { colors } from '../../styles/variables';

export default class PageTourPhoto extends React.Component {

	static navigationOptions = {
		header: null,
	};

	render = () => {
		return (
			<View style={[cs.flex_col_center_top]}>

				<LightGradient />

				<View style={[cs.flex_row_center, cs.ph15, cs.mt10]}>

					<Text style={[cs.title1, cs.mh10, cs.mt7, { flex: 0.4 }]} onPress={() => this.props.navigation.navigate('credit_profile')}>
						Skip Tour
					</Text>

					<View style={{ flex: 1 }} />

					<Text style={[cs.title1, cs.mh10, cs.mt7, { flex: 0.25 }]} onPress={() => this.props.navigation.navigate('tour_credit_challenge')}>
						Next
					</Text>
				</View>

				<View style={[cs.flex_row_center, cs.mt10]}>
					<View style={[cs.roundimage]}>
						<Image
							source={require('../../../images/addpic.jpg')}
							style={[{ borderRadius: 135, width: 270, height: 270 }]}
						/>
					</View>
				</View>

				<View style={[cs.flex_row_center, cs.mt10]}>
					<Text style={[cs.logotextlg]}>
						Add Your Profile
					</Text>
				</View>

				<View style={[cs.flex_row_center]}>
					<Text style={[cs.logotextlg]}>
						Photo
					</Text>
				</View>

				<View style={[cs.flex_row_center, cs.mt10]}>
					<Text style={[cs.title2white, cs.m5]}>
						Personalize your profile
					</Text>
				</View>

			</View>
		);
	}
}

PageTourPhoto.propTypes = {
	navigation: PropTypes.object.isRequired
};
// react
import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Image, View, Text, TextInput, AsyncStorage, Platform, Linking } from 'react-native';

import TouchID from 'react-native-touch-id';
//import * as Keychain from 'react-native-keychain';

// external
import Spinner from 'react-native-spinkit';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

// google analytics
import { trackScreenView, trackEvent } from '../modules/ga';

// elements
import DarkGradient from '../elements/dark_gradient';
import ErrorBox from '../elements/error_box';
import Button from '../elements/button';

import AltLogin from '../components/c_alt_login';

// validator
import Validator from '../modules/validator.js';

// redux
import store from '../redux/store';
import { login } from '../redux/actions';

// api
import Route from '../modules/route.js';
// const route = new Route('https://trifigoapi.azurewebsites.net/');
const route = new Route(LiveUrl);

import {
  getRefConsumerTypes,
  getRefPaymentSentimentTypes,
  getRefCreditUtilizationSentimentTypes,
  getRefAccountAgeSentimentTypes,
  getRefAccountMixSentimentTypes,
  getRefCreditInquirySentimentTypes,
  getRefOtherFactorsSentimentTypes,
  getRecoTemplates,
  getDealItems,
  LiveUrl
} from '../modules/api';

// styles
import cs from '../styles/common_styles.js';
import { colors, fontFamily, fontSize, deviceWidth } from '../styles/variables';

export default class PageLogin extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      onesignal_device: store.getState().onesignal_device,
      consumerTypes: store.getState().consumerTypes,
      paymentSentimentTypes: store.getState().paymentSentimentTypes,
      creditUtilizationSentimentTypes: store.getState().creditUtilizationSentimentTypes,
      accountAgeSentimentTypes: store.getState().accountAgeSentimentTypes,
      accountMixSentimentTypes: store.getState().accountMixSentimentTypes,
      creditInquirySentimentTypes: store.getState().creditInquirySentimentTypes,
      otherFactorsSentimentTypes: store.getState().otherFactorsSentimentTypes,
      recoTemplates: store.getState().recoTemplates,
      dealItems: store.getState().dealItems,

      unsubscribe: store.subscribe(this.updateState),

      login: '',
      err_login: null,
      password: '',
      err_password: null,

      error: null,
      loading: null,

      //connection
      connectionStatus : store.getState().connectionStatus
    };

    trackScreenView('Login');
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      onesignal_device: store.getState().onesignal_device,
      consumerTypes: store.getState().consumerTypes,
      paymentSentimentTypes: store.getState().paymentSentimentTypes,
      creditUtilizationSentimentTypes: store.getState().creditUtilizationSentimentTypes,
      accountAgeSentimentTypes: store.getState().accountAgeSentimentTypes,
      accountMixSentimentTypes: store.getState().accountMixSentimentTypes,
      creditInquirySentimentTypes: store.getState().creditInquirySentimentTypes,
      otherFactorsSentimentTypes: store.getState().otherFactorsSentimentTypes,
      recoTemplates: store.getState().recoTemplates,
      dealItems: store.getState().dealItems,
    });
  }

  handleOpenURL = (event) => {
    this.navigateTo(event.url);
  }

  navigateTo = (url) => {
    if(!url) return;

    const route = url.replace(/.*?:\/\//g, '');
    //const id = route.match(/\/([^\/]+)\/?$/)[1];
    
    if(!route) return;

    try {
      if(route !== null) {
        const routeName = route.split('/')[0];

        if (routeName === 'signup')
          this.props.navigation.navigate('signup_select');
        else if (routeName === 'gradsignup')
          this.props.navigation.navigate('grad_signup');
        else if (routeName === 'studentsignup')
          this.props.navigation.navigate('student_signup');
        else if (routeName === 'forgotpass')
          this.props.navigation.navigate('forgot_pass');
        }
      } 
      catch (e) { 
      //console.error(e);
    }
  }

  componentDidMount() {
    let _this = this;
    let st = this.state;

    // deep linking
    if (Platform.OS === 'android') {
      Linking.getInitialURL().then(url => {
        if(url) _this.props.navigation.navigate(url); 
      });
    }
    else {
      Linking.addEventListener('url', _this.handleOpenURL);
    }

    //store.dispatch(reset());

    AsyncStorage.getItem('facebookid').then((item) => {
      if (item) {
        _this.facebookid = item;
      }
    });

    AsyncStorage.getItem('userId').then((item) => {
      if (item) {
        _this.userId = item;
        this.setState({
          login : item
        })
        // AsyncStorage.getItem('password').then((item) => {
        //   if (item) {
        //     _this.password = item;
        //     this.setState({
        //       password : item
        //     })
        //   }
        // });
      }
    });

    if (!st.consumerTypes) getRefConsumerTypes();

    if (!st.paymentSentimentTypes) getRefPaymentSentimentTypes();

    if (!st.creditUtilizationSentimentType) getRefCreditUtilizationSentimentTypes();

    if (!st.accountAgeSentimentTypes) getRefAccountAgeSentimentTypes();

    if (!st.accountMixSentimentTypes) getRefAccountMixSentimentTypes();

    if (!st.creditInquirySentimentTypes) getRefCreditInquirySentimentTypes();

    if (!st.otherFactorsSentimentTypes) getRefOtherFactorsSentimentTypes();

    if (!st.recoTemplates) getRecoTemplates();

    if (!st.dealItems) getDealItems();
  }

  componentWillUnmount() {
    this.state.unsubscribe();

    Linking.removeEventListener('url', this.handleOpenURL);
  }

  signup = () => {
    this.props.navigation.navigate('signup_select');
  }

  forgot = () => {
    this.props.navigation.navigate('forgot_pass');
  }

  login = (userId, password) => {
    let _this = this;

    var user = {
      login: userId,
      password: password
    };

    route.postdata('login', user).then(function (res, err) {
      if (res) {
        if (res.status == 'A') {
          store.dispatch(login(res));
          // console.log("Login >>>", res)
          trackEvent('Login', userId);

          AsyncStorage.setItem('userId', userId);
          AsyncStorage.setItem('password', password);
          AsyncStorage.setItem('isLoggedIn', JSON.stringify(true));

          // update one signal device 
          if (_this.state.onesignal_device) {
            let d = {
              id: userId,
              onesignalId: _this.state.onesignal_device.userId,
              pushToken: _this.state.onesignal_device.pushToken
            }
            route.postdata('onesignal/update', d);
          }
          _this.setState({
            loading: null
          })
          _this.props.navigation.navigate('credit_profile');
        }
        else {
          _this.setState({
            error: 'Invalid credentials',
            loading: null
          });
        }
      }
      else if (err !== undefined) {
        _this.setState({
          error: err,
          loading: null
        });
      }
    });
  }

  facebook_login = (facebookid) => {
    let _this = this;

    var user = {
      facebookid: facebookid,
    };

    route.postdata('flogin', user).then(function (res, err) {
      if (res !== undefined) {
        if (res.status == 'A') {
          store.dispatch(login(res));
          trackEvent('FacebookLogin', facebookid);
          AsyncStorage.setItem('userId', res.id);
          AsyncStorage.setItem('facebookid', facebookid);
          _this.props.navigation.navigate('banking_profile');
        }
        else {
          _this.setState({
            error: 'Invalid credentials',
            loading: null
          });
        }
      }
      else if (err !== undefined) {
        _this.setState({
          error: err,
          loading: null
        });
      }
    });
  }

  submit = () => {
    let st = this.state;

    // Reset current error and set button to loading which shows a spinner on the button
    this.setState({ error: null, loading: 'loading' });

    this.reset();

    let errors = this.validate();

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    this.login(st.login, st.password);
  }

  validate = () => {
    let errors = 0;
    let err = null;

    let st = this.state;

    // Validate login
    if (st.login === null || st.login === undefined || st.login == '') {
      errors++;
      this.setState({ err_login: 'Login required' });
      if (err === null) err = 'Login required';
    }

    if (this.state.connectionStatus == false) {
      errors++;
      this.setState({ err_login: 'No internet found' });
      if (err === null) err = 'No internet found';
    }

    /*
    if (st.login) {
      if (!Validator.isUsername(st.login)) {
        errors++;
        this.setState({ err_login: 'Username is invalid' });
        if (err === null) err = 'Username is invalid';
      }
    }
    */

    // Validate password
    if (st.password === null || st.password === undefined || st.password == '') {
      errors++;
      this.setState({ err_password: 'Password required' });
      if (err === null) err = 'Password required';
    }

    if (st.password) {
      if (!Validator.isPassword(st.password)) {
        errors++;
        this.setState({ err_password: 'Password is invalid' });
        if (err === null) err = 'Password is invalid';
      }
    }

    if (errors == 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  reset = () => {
    this.setState({
      error: null,
      err_login: null,
      err_logininvalid: null,
      err_password: null,
      err_passinvalid: null,
    });
  }

  onFacebookLogin = (profile) => {
    let _this = this;

    //console.log('facebook profile >>>', profile);
    if (!profile) {
      return;
    }

    let imgUrl = null;
    if (profile.picture !== null && profile.picture !== undefined) {
      if (profile.picture.data !== null && profile.picture.data !== undefined) {
        if (profile.picture.data.url !== null && profile.picture.data.url !== undefined) {
          imgUrl = profile.picture.data.url;
        }
      }
    }

    var user = {
      facebookid: profile.id,
      firstName: profile.first_name,
      lastName: profile.last_name,
      email: profile.email,
      profileImgUrl: imgUrl
    };

    route.postdata('flogin', user).then(function (res, err) {
      if (res) {
        if (res.status === 'A') {
          store.dispatch(login(res));
          AsyncStorage.setItem('userId', res.id);
          AsyncStorage.setItem('facebookid', res.facebookid);
          _this.props.navigation.navigate('banking_profile');
        }
        else {
          _this.setState({
            error: 'Invalid credentials',
            loading: null
          });
        }
      }
      else if (err) {
        _this.setState({
          error: err,
          loading: null
        });
      }
    });
  }

  onGoogleLogin = (profile) => {
    let _this = this;

    if (profile === null || profile === undefined) {
      return;
    }

    var user = {
      googleid: profile.id,
      email: profile.email,
      firstName: profile.givenName,
      lastName: profile.familyName,
      profileImgUrl: profile.photoUrl
    };

    route.postdata('glogin', user).then(function (res, err) {
      if (res !== undefined) {
        if (res.status == 'A') {
          store.dispatch(login(res));
          AsyncStorage.setItem('userId', res.id);
          AsyncStorage.setItem('googleid', res.googleid);
          _this.props.navigation.navigate('landing');
        }
        else {
          _this.setState({
            error: 'Invalid credentials',
            loading: null
          });
        }
      }
      else if (err !== undefined) {
        _this.setState({
          error: err,
          loading: null
        });
      }
    });
  }

  loginWithId = () => {
    this.reset();
    if (this.userId && this.password) {
      this.setState({ error: null, loading: 'loading' });
      this.login(this.userId, this.password);
    }
    else if (this.facebookid) {
      this.setState({ error: null, loading: 'loading' });
      this.facebook_login(this.facebookid);
    }
  }

  onIdLogin = () => {
    let _this = this;
    if (!_this.facebookid && !this.userId) {
      _this.setState({ error: 'Login at least once' });
    }
    else {
      TouchID.isSupported()
        .then(biometryType => {
          if (biometryType === 'TouchID') { // Touch ID is supported on iOS
            TouchID.authenticate('Authenticate with Touch Id') // Show the Touch ID prompt
              .then(() => _this.loginWithId())
              .catch(() => {
                // Touch ID Authentication failed (or there was an error)!
                // Also triggered if the user cancels the Touch ID prompt
                // On iOS and some Android versions, `error.message` will tell you what went wrong
              });
          } else if (biometryType === 'FaceID') {
            // Face ID is supported on iOS
            TouchID.authenticate('Authenticate with Face Id') // Show the Face Id prompt
              .then(() => _this.loginWithId())
              .catch(() => {
                // Touch ID Authentication failed (or there was an error)!
                // Also triggered if the user cancels the Touch ID prompt
                // On iOS and some Android versions, `error.message` will tell you what went wrong
              });
          } else if (biometryType === true) {
            // Touch ID is supported on Android
            TouchID.authenticate('Authenticate with Touch Id') // Show the Touch ID prompt
              .then(() => _this.loginWithId())
              .catch(() => {
                // Touch ID Authentication failed (or there was an error)!
                // Also triggered if the user cancels the Touch ID prompt
                // On iOS and some Android versions, `error.message` will tell you what went wrong
              });
          }
        })
        .catch(() => {
          //console.log('Touch ID error', error);
          // User's device does not support Touch ID (or Face ID)
          // This case is also triggered if users have not enabled Touch ID on their device
        });
    }
  }

  onAltLoginError = (msg) => {
    this.setState({
      error: msg,
      loading: null
    });
  }

  render = () => {
    let st = this.state;

    if (st.loading) {
      return (
        <View style={[cs.darkSinglePage, cs.flex_center]}>

          <DarkGradient />

          <Spinner
            isVisible
            size={60}
            type='ThreeBounce'
            color={colors.green} />

          <Text
            style={styles.signuptextlink}>
            loading
          </Text>

        </View>
      );
    }

    return (
      <View style={cs.darkSinglePage}>

        <DarkGradient />

        <View style={cs.m20} />

        <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center]}>

            {/* Trifigo Logo */}
            <Image source={require('../../images/t_icon.png')}
              style={{ width: 95, height: 85, marginTop: 10 }} />

            <Text style={[cs.headertext, cs.m5]}>Login</Text>

            <View style={styles.formbox}>

              {/* Login textbox */}
              <View style={st.err_login !== null ? cs.textInputFieldError : cs.textInputFieldDark}>
                <Image
                  source={require('../../images/avatar.png')}
                  style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
                />
                <TextInput
                  placeholder='Email'
                  value = {this.state.login != '' ? this.state.login : ''}
                  style={cs.textInputDark}
                  onChangeText={(text) => this.setState({ login: text })}
                  underlineColorAndroid='rgba(0,0,0,0)'
                  placeholderTextColor='rgba(255,255,255,0.7)'
                  autoCapitalize={'none'}
                  autoCorrect={false}
                />
              </View>

              {/* Password textbox */}
              <View style={st.err_password !== null ? cs.textInputFieldError : cs.textInputFieldDark}>

                <Image
                  source={require('../../images/padlock.png')}
                  style={{ position: 'absolute', bottom: 11, left: 20, width: 17, height: 22 }}
                />

                <TextInput
                  placeholder='Password'
                  value = {this.state.password}
                  style={cs.textInputDark}
                  onChangeText={(text) => this.setState({ password: text })}
                  placeholderTextColor='rgba(255,255,255,0.7)'
                  underlineColorAndroid='rgba(0,0,0,0)'
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  secureTextEntry
                />
              
              </View>

              {/* Sign in button */}
              <View style={[cs.buttonBox, { marginBottom: 5 }]}>
                <Button
                  text='Login'
                  width={deviceWidth - 55}
                  height={44}
                  fontSize={18}
                  backgroundColor={colors.green}
                  onClick={this.submit} />
              </View>

              <Text
                style={[styles.signuptextlink, styles.signuptext2]}
                onPress={this.forgot}>
                forgot password?
              </Text>

            </View>

            {/* Error box */}
            <ErrorBox text={st.error} />

            <AltLogin
              onFacebookLogin={this.onFacebookLogin}
              onGoogleLogin={this.onGoogleLogin}
              onError={this.onAltLoginError}
              onTouchIdLogin={this.onIdLogin}
              onFaceIdLogin={this.onIdLogin}
              showIdLogin
            />

            {/* Sign up for account */}
            <View style={styles.noteBox}>
              <Text style={styles.signuptext}>
                {'Don\'t have an account?'}
              </Text>
              <Text style={cs.signuptext2}>
                <Text
                  style={cs.signuptextlink}
                  onPress={this.signup}>
                  Sign Up
                </Text>
              </Text>
            </View>

            <View style={[styles.noteBox, cs.mt5]}>
              <Text style={cs.xsmallText}>
                Bank level Encryption
            </Text>
            </View>

          </View>

        </KeyboardAwareScrollView>
      </View>
    );
  }
}

PageLogin.propTypes = {
  navigation: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    marginTop: 20,
  },
  headertext: {
    color: colors.white,
    fontSize: 24,
    fontFamily: fontFamily.semiBold,
    letterSpacing: 4,
    backgroundColor: 'transparent',
  },
  signuptext: {
    paddingTop: 15,
    color: colors.white,
    fontSize: fontSize.small,
    letterSpacing: 4,
    backgroundColor: 'transparent',
    fontFamily: fontFamily.light
  },
  signuptext2: {
    padding: 5,
    fontSize: 18,
    letterSpacing: 2,
    backgroundColor: 'transparent',
    fontFamily: fontFamily.light
  },
  signuptextlink: {
    letterSpacing: 2,
    color: colors.green,
    fontFamily: fontFamily.medium
  },
  formbox: {
    padding: 10,
    alignItems: 'center',
  },
  noteBox: {
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
});
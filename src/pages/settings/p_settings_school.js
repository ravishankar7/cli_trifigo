//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

// google analytics
import { trackScreenView } from '../../modules/ga';
import AutoCompleteSelectLight from '../../elements/autocomplete_select_light';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import OkBox from '../../elements/ok_box';

// store
import store from '../../redux/store';

// api
import { updateUser } from '../../modules/api';

// styles
import cs from '../../styles/common_styles';

//spinner
import Spinner from 'react-native-spinkit';

import { getRefUniversities } from '../../modules/api';
import { colors } from '../../styles/variables';

export default class PageSettingsSchool extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;

    this.state = {
      user: _user,
      universities: store.getState().universities,

      unsubscribe: store.subscribe(this.updateState),
      school: _user !== null ? _user.univId.toString() : null,
      err_school: null,

      year: _user !== null ? _user.graduationYear.toString() : null,
      err_year: null,

      graduated: _user !== null ? _user.userType.toString() : null,
      err_graduated: null,

      error: null,
      loading: null,
      success: null,
    };

    trackScreenView('Settings School');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,
      universities: store.getState().universities,

      school: _user !== null ? _user.univId : null,
      err_school: null,

      year: _user !== null ? _user.graduationYear : null,
      err_year: null,

      graduated: _user !== null ? _user.userType : null,
      err_graduated: null,
    });
  }

  componentDidMount() {
    let st = this.state;
    if (!st.universities) getRefUniversities();
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  resetErrors = () => {
    this.setState({
      loading: null,
      error: null,
      success: null,

      err_school: null,
      err_year: null,
      err_date: null,
    });
  }

  validateData = () => {
    this.resetErrors();

    let errors = 0;
    let err = null;

    let st = this.state;

    // Validate name
    if (st.school === null || st.school === undefined || st.school == '') {
      errors++;
      this.setState({ err_school: 'School required' });
      if (err === null) err = 'School required';
    }

    // Validate last name
    if (st.year === null || st.year === undefined || st.year == '') {
      errors++;
      this.setState({ err_year: 'Year required' });
      if (err === null) err = 'Year required';
    }

    if (st.graduated === null || st.graduated === undefined || st.graduated == '') {
      errors++;
      this.setState({ err_date: 'Graduated required' });
      if (err === null) err = 'Graduated required';
    }

    if (errors == 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  saveData = () => {
    
            let errors = this.validateData();

            if (errors > 0) {
              this.setState({ loading: null });
              return;
            }

            this.setState({
              loading : 'loading'
            })
    
    // save data
    let _this = this;
    let st = this.state;

    let userId = this.state.user.id;
    let user = this.state.user;

    var grad_status = ''

    if (st.graduated === 'Student') {
      grad_status = 'S'
    }
    else if (st.graduated === 'Graduate') {
      grad_status = 'G'
    }

    user.univId = st.school
    user.userType = grad_status
    user.graduationYear = Number(st.year)

    // console.log("grad_status", grad_status, "userType",st.year, "univId", st.school)

    try {
      updateUser(userId, user);
      _this.setState({ success: 'Saved' }, () => {
        _this.timer = setInterval(() => {
          const { navigate } = this.props.navigation;
          clearInterval(_this.timer);
          navigate('settings');
        }, 1000);
      });
    }
    catch (e) { _this.setState({ error: e }); }
  }

  cancel = () => {
    this.props.navigation.navigate('settings');
  }

  selectYear = (data) => {
    if(data) this.setState({ year: data.name });
  }

  changeYear = (data) => {
    if(data)this.setState({ year: null, selected_year: null });
  }

  selectSchool = (data) => {

    if(data) this.setState({ school: data.name });
  }

  changeSchool = (data) => {
    if(data) this.setState({ school: null, selected_school: null });
  }

  selectGraduated = (data) => {

    if(data) this.setState({ graduated: data.name });
  }

  changeGraduated = (data) => {
    if(data) this.setState({ graduated: null, selected_graduated: null });
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    let current_year = (new Date()).getFullYear();

    let years = [];
    for (let i = -10; i < 6; i++) {
      years.push({ id: i, name: String(current_year + i) });
    }

    let months = [];
    for (let i = 1; i < 13; i++) {
      months.push({ id: i, name: String(i) });
    }

    let graduated = [{ id: 1, name: 'Student' }, { id: 2, name: 'Graduate' }];

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Settings'
          back='settings'
        />

        <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center, cs.mt30]}>

            <Text style={[cs.headertextdarkgray, cs.mb10]}>School</Text>

            <AutoCompleteSelectLight
              value = {this.state.school}
              data={st.universities}
              onSelect={this.selectSchool}
              onChange={this.changeSchool}
              placeHolder=  'School'
              icon='university'
            />

            <AutoCompleteSelectLight
              value = {this.state.year}
              data={years}
              onSelect={this.selectYear}
              onChange={this.changeYear}
              placeHolder='Graduation Year'
              icon='university'
            />

            <AutoCompleteSelectLight
            value = {this.state.graduated}
              data={graduated}
              onSelect={this.selectGraduated}
              onChange={this.changeGraduated}
              placeHolder='Graduated'
              icon='university'
            />

            { this.state.loading 
            ?
            <Spinner
                  isVisible
                  style = {cs.mt2}
                  size={60}
                  type='ThreeBounce'
                  color={colors.green} />
            
            : <View style={[cs.flex_row_center, cs.mb10]}>

              <View style={cs.m5}>
                <Button
                  text='Cancel'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#CFD8DC'
                  textColor='#404040'
                  onClick={this.cancel} />
              </View>

              <View style={cs.m5}>
                <Button
                  text='Save'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#66BB6A'
                  onClick={this.saveData} />
              </View>

            </View>}

            <View style={[cs.flex_row_center, cs.mb10]}>

              {st.error !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              }

              {st.success !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              }

            </View>

          </View>

        </KeyboardAwareScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsSchool.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettingsSchool.defaultProps = {
  user: null,
};

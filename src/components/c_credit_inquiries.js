// react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View } from 'react-native';

// store
import store from '../redux/store';

// api
import { getCreditInquirySummary } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import { fontFamily } from '../styles/variables';

export default class CreditInquiries extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      creditInquirySummary: store.getState().creditInquirySummary,
      creditInquirySentimentTypes: store.getState().creditInquirySentimentTypes,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      creditInquirySummary: store.getState().creditInquirySummary,
      creditInquirySentimentTypes: store.getState().creditInquirySentimentTypes,
    });
  }

  componentDidMount() {
    if (this.state.user === null) return;
    
    let _this = this;
    let userId = this.state.user.id;

    if (this.state.creditInquirySummary === null) {
      try { getCreditInquirySummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    let st = this.state;

    let title = 'Upload credit report to see credit enquiries summary';
    let item = null;

    if (st.creditInquirySummary && st.creditInquirySummary.numberOfInquiries) {
      title = 'You have ' + st.creditInquirySummary.numberOfInquiries + ' credit inquiries in the last 24 months.';
    }

    if (st.creditInquirySummary !== null && st.creditInquirySentimentTypes !== null && st.creditInquirySentimentTypes.length > 0) {
      st.creditInquirySentimentTypes.forEach(i => {
        if (i.min <= st.creditInquirySummary.numberOfInquiries && st.creditInquirySummary.numberOfInquiries <= i.max) {
          item = i;
        }
      });
    }

    return (
      <View style={cs.shadowBox}>

        <View style={cs.flex_row_space_between}>

          <Text style={cs.title1}>Credit Inquiries</Text>

          {item !== null &&
            <View style={{ backgroundColor: item.colorBg, height: 24, borderRadius: 8, width: 80, alignItems: 'center', paddingVertical: 1 }}>
              <Text style={{ fontFamily: fontFamily.semiBold, fontSize: 13, color: item.colorFg, letterSpacing: 0.5 }}>
                {item.result}
              </Text>
            </View>
          }

        </View>

        <Text style={[cs.secondtitletext, cs.mb10]}>
          Low Importance / 10%
        </Text>

        <View style={[cs.flex_row_space_between, cs.mr10]}>

          <Text style={[cs.subtitle2gray, cs.m5, cs.mr10]}>
            {title} {item !== null ? item.sentiment : ''}
          </Text>

          {/*
          <View style={[cs.mt20, cs.w10]}>
            <TouchableOpacity onPress={() => this.props.navigate('enquiries')}>
              <Image
                source={require('../../images/chevron-right.png')}
                style={{ height: 35 }}
              />
            </TouchableOpacity>
          </View>
          */}
          
        </View>

      </View>
    );
  }
}

CreditInquiries.propTypes = {
  navigate: PropTypes.func.isRequired
};
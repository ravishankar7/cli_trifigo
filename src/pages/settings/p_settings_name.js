//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, View, Image, TextInput } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import OkBox from '../../elements/ok_box';

// store
import store from '../../redux/store';

// api
import { updateUser } from '../../modules/api';

// styles
import cs from '../../styles/common_styles';

// external
import Spinner from 'react-native-spinkit';
import { colors } from '../../styles/variables';

export default class PageSettingsName extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;
    
    this.state = {
      user: _user,

      unsubscribe: store.subscribe(this.updateState),

      name: _user !== null ? _user.firstName : null,
      err_name: null,

      lname: _user !== null ? _user.lastName : null,
      err_lname: null,

      error: null,
      loading: null,
      success: null,
    };

    trackScreenView('Settings Name');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,

      name: _user !== null ? _user.firstName : null,
      err_name: null,

      lname: _user !== null ? _user.lastName : null,
      err_lname: null,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  resetErrors = () => {
    this.setState({
      loading: null,
      error: null,
      success: null,

      err_name: null,
      err_lname: null,
    });
  }

  validateData = () => {
    this.resetErrors();

    let errors = 0;
    let err = null;

    let st = this.state;

    // Validate name
    if (st.name === null || st.name === undefined || st.name == '') {
      errors++;
      this.setState({ err_name: 'Name required' });
      if (err === null) err = 'Name required';
    }

    // Validate last name
    if (st.lname === null || st.lname === undefined || st.lname == '') {
      errors++;
      this.setState({ err_lname: 'Last name required' });
      if (err === null) err = 'Last name required';
    }

    if (errors == 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  saveData = () => {
    let errors = this.validateData();
    this.setState({
      loading : 'loading'
    })
    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    // save data
    let _this = this;
    let st = this.state;

    let userId = this.state.user.id;
    let user = this.state.user;

    user.firstName = st.name;
    user.lastName = st.lname;

    try {
      updateUser(userId, user);
      _this.setState({ success: 'Saved' }, () => {
        _this.timer = setInterval(() => {
          const { navigate } = this.props.navigation;
          clearInterval(_this.timer);
          navigate('settings');
        }, 1000);
      });
    }
    catch (e) { _this.setState({ error: e }); }
  }

  cancel = () => {
    this.props.navigation.navigate('settings');
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Settings'
          back='settings'
        />

        <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

          <View style={[cs.flex_center, cs.mt30]}>

            <Text style={[cs.headertextdarkgray, cs.mb10]}>Name</Text>

            {/* Name textbox */}
            <View style={st.err_name !== null ? cs.textInputFieldError : cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='First name'
                value={st.name}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ name: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            {/* Last Name textbox */}
            <View style={st.err_lname !== null ? cs.textInputFieldError : cs.textInputField}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Last name'
                value={st.lname}
                style={cs.textInput}
                onChangeText={(text) => this.setState({ lname: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(0,0,0,0.3)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            { this.state.loading ? 
                <Spinner
                  isVisible
                  size={60}
                  type='ThreeBounce'
                  color={colors.green} />
                   :
                    <View style={[cs.flex_row_center, cs.mb10]}>

              <View style={cs.m5}>
                <Button
                  text='Cancel'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#CFD8DC'
                  textColor='#404040'
                  onClick={this.cancel} />
              </View>

              <View style={cs.m5}>

                <Button
                  text='Save'
                  width={140}
                  height={35}
                  fontSize={16}
                  backgroundColor='#66BB6A'
                  onClick={this.saveData} />
                

                  
              </View>

            </View>}

            <View style={[cs.flex_row_center, cs.mb10]}>

              {st.error !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <ErrorBox text={st.error} />
                </View>
              }

              {st.success !== null &&
                <View style={[cs.flex_center, cs.mt10]}>
                  <OkBox text={st.success} />
                </View>
              }

            </View>

          </View>

        </KeyboardAwareScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsName.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettingsName.defaultProps = {
  user: null,
};

// react
import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, TouchableOpacity, FlatList, ScrollView, Image, TextInput } from 'react-native';

// styles
import cs from '../styles/common_styles';
import icons from '../styles/icons';

export default class AutoCompleteSelectLight extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      selectedValue: this.props.value,
      filtered_data: [],
      open: false
    };
  }

  filterOptions = (filter) => {
    let st = this.state;
    
    if(!st.open) {
      this.setState({ filtered_data: [] });
      return;
    }

    if (!filter) {
      this.setState({ filtered_data: this.props.data });
    }
    else {
      let _data = [];
      // this.props.data.forEach(element => {
      //   if (element.name && element.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1) _data.push(element);
      // });
      this.setState({ filtered_data: this.props.data });
      // this.setState({ filtered_data: _data });
    }
  }
  
  selectOption = (data) => {
    //console.log('selected >>>', data);
    this.setState({ selectedValue: data.name, open: false, filtered_data: [] });
    if(this.props.onSelect) this.props.onSelect(data);
  }
  
  changeText = (text) => {
    this.setState({ open: true, selectedValue: null }, () => this.filterOptions(text));
    if (this.props.onSelect) this.props.onSelect(null);
    if (this.props.onChange) this.props.onChange(text);
  }

  render() {
    let st = this.state;

    let icon = this.props.icon;

    return (
      <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='always'>
        
        <View style={[cs.textInputFieldLight, cs.mb0]}>
          <Image
            source={icons[icon]}
            style={{ position: 'absolute', top: 7, left: 15, width: 23, height: 23 }}
          />
          <TextInput
            placeholder={this.props.placeHolder}
            style={cs.textInputLight}
            placeholderTextColor='black'
            underlineColorAndroid='rgba(0,0,0,0)'
            autoCapitalize={'none'}
            autoCorrect={false}
            value={st.selectedValue}
            onChangeText={(text) => this.changeText(text)}
            onFocus={() => this.setState({ open: true }, () => this.filterOptions(null))}
            onBlur={() => this.setState({ open: false }, () => this.filterOptions(null))}
          />
        </View>
            
        <View style={cs.listInputFieldLight}>
          <FlatList
              keyboardShouldPersistTaps="always"
              data={st.filtered_data}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => (
              <TouchableOpacity style={cs.listviewInputDark} onPress={() => this.selectOption(item)}>
                  <Text style={cs.listInputLight}>{item.name}</Text>
              </TouchableOpacity>
            )}
          />
        </View>
        
      </ScrollView>
    );
  }
}

AutoCompleteSelectLight.propTypes = {
  data: PropTypes.array,
  onSelect: PropTypes.func,
  onChange: PropTypes.func,
  placeHolder: PropTypes.string,
  icon: PropTypes.string,
};

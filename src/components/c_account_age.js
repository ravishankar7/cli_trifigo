// react
import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';

// store
import store from '../redux/store';

// api
import { getAccountAgeSummary } from '../modules/api';

// styles
import cs from '../styles/common_styles';
import { fontFamily } from '../styles/variables';

export default class AccountAge extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      accountAgeSummary: store.getState().accountAgeSummary,
      accountAgeSentimentTypes: store.getState().accountAgeSentimentTypes,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      accountAgeSummary: store.getState().accountAgeSummary,
      accountAgeSentimentTypes: store.getState().accountAgeSentimentTypes,
    });
  }

  componentDidMount() {
    if (this.state.user === null) return;
    
    let _this = this;
    let userId = this.state.user.id;

    if (this.state.accountAgeSummary === null) {
      try { getAccountAgeSummary(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  render() {
    let st = this.state;

    let title = 'Upload credit report to see account age summary';
    let item = null;

    if (st.accountAgeSummary && st.accountAgeSummary.averageAgeYears) {
      title = 'Your credit age is ' + st.accountAgeSummary.averageAgeYears + ' years and ' + st.accountAgeSummary.averageAgeMonths + ' months.';
    }

    if (st.accountAgeSummary !== null && st.accountAgeSentimentTypes !== null && st.accountAgeSentimentTypes.length > 0) {
      st.accountAgeSentimentTypes.forEach(i => {
        if (i.minYear <= st.accountAgeSummary.averageAgeYears && st.accountAgeSummary.averageAgeYears <= i.maxYear) {
          item = i;
        }
      });
    }

    return (
      <View style={cs.shadowBox}>

        <View style={cs.flex_row_space_between}>

          <Text style={cs.title1}>Account Age</Text>

          {item !== null &&
            <View style={{ backgroundColor: item.colorBg, height: 24, borderRadius: 8, width: 80, alignItems: 'center', paddingVertical: 1 }}>
              <Text style={{ fontFamily: fontFamily.semiBold, fontSize: 13, color: item.colorFg, letterSpacing: 0.5 }}>
                {item.result}
              </Text>
            </View>
          }

        </View>

        <Text style={[cs.secondtitletext, cs.mb10]}>
          Moderate Importance / 15%
        </Text>

        <View style={[cs.flex_row_space_between, cs.mr10]}>

          <Text style={[cs.subtitle2gray, cs.m5, cs.mr10]}>
            {title} {item !== null ? item.sentiment : ''}
          </Text>

          {/*
          <View style={[cs.mt20, cs.w10]}>
            <TouchableOpacity onPress={() => this.props.navigate('account_age')}>
              <Image
                source={require('../../images/chevron-right.png')}
                style={{ height: 35 }}
              />
            </TouchableOpacity>
          </View>
          */}
          
        </View>

      </View>
    );
  }
}

AccountAge.propTypes = {
  navigate: PropTypes.func.isRequired
};
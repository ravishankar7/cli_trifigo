// react
import React from 'react';
import PropTypes from 'prop-types';

import { Image, View, Text, TextInput, AsyncStorage, ScrollView } from 'react-native';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import DarkGradient from '../../elements/dark_gradient';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';

// components
import AltLogin from '../../components/c_alt_login';

// modules
import Validator from '../../modules/validator.js';

// redux
import { login } from '../../redux/actions';
import store from '../../redux/store';

// api
import Route from '../../modules/route.js';
// const route = new Route('https://trifigoapi.azurewebsites.net/');
const route = new Route(LiveUrl);


// api
import { getRefUniversities, LiveUrl } from '../../modules/api';

// styles
import cs from '../../styles/common_styles.js';
import { colors, deviceWidth } from '../../styles/variables';

export default class PageSignup extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {

      universities: store.getState().universities,

      unsubscribe: store.subscribe(this.updateState),

      username: '',
      password: '',
      confirmpass: '',
      cellPhone: '',
      firstName: '',
      lastName: '',

      err_username: null,
      err_userinvalid: null,
      err_pass: null,
      err_passinvalid: null,
      err_confirmpass: null,
      err_cpassinvalid: null,
      err_firstname: null,
      err_lastname: null,

      error: null,
      loading: ''
    };

    trackScreenView('Signup');
  }

  updateState = () => {
    this.setState({
      universities: store.getState().universities
    });
  }

  componentDidMount () {
    let st = this.state;
    if (!st.universities) getRefUniversities();
  }

  login = () => {
    this.props.navigation.navigate('banking_profile');
  }

  signin = () => {
    this.props.navigation.navigate('login');
  }

  submitForm = () => {
    let _this = this;
    let st = this.state;
    
    // Reset current error and set button to loading which shows a spinner on the button
    this.setState({ error: null, loading: ' is-loading' });

    this.resetErrors();

    let errors = this.validateError();

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    var user = {
      login: st.username,
      password: st.password,
      email: st.username,
      cellPhone: st.cellPhone,
      firstName: st.firstName,
      lastName: st.lastName,
      status: 'A',
    };

    route.postdata('user/create', user).then(function (res, err) {
      if (res !== undefined) {

        store.dispatch(login(res));

        AsyncStorage.setItem('userId', res.id);

        let email = {
          "subject": "Welcome to Trifigo",
          "html": "<center><p>Thank you for registering for the Trifigo Platform Early Access. <br>  <center> <p>Login Credentials : </p></center> <center> <p> UserName :" + st.username + "</p> </center><center> <p> Passsword :" + st.password + "</p> </center>  </center>",
          "name": st.username,
          "fromEmail": "contact@trifigo.com",
          "toEmail": st.username
        };

        route.postdata('mail', email);

        _this.props.navigation.navigate('banking_profile');
      }
      else if (err !== undefined) {
        _this.setState({
          error: 'Error',
          loading: null
        });
      }
    });
  }

  validateError = () => {
    let errors = 0;
    let st = this.state;
    let err = null;

    // Validate First Name
    if (st.firstName === null || st.firstName === undefined || st.firstName == '') {
      errors++;
      this.setState({ err_firstname: 'First Name is required' });
    }

    // Validate Last Name
    if (st.lastName === null || st.lastName === undefined || st.lastName == '') {
      errors++;
      this.setState({ err_lastname: 'Last Name is required' });
    }

    //Validate Email
    if (st.username === null || st.username === undefined || st.username == '') {
      errors++;
      this.setState({ err_username: 'Username (Email) is required' });
      if (err === null) err = 'Username (Email) is required';
    }

    if (st.username) {
      if (!Validator.isEmail(st.username)) {
        errors++;
        this.setState({ err_userinvalid: 'Username (Email) is invalid' });
        if (err === null) err = 'Username (Email) is invalid';
      }
    }

    // Validate password
    if (st.password === null || st.password === undefined || st.password == '') {
      errors++;
      this.setState({ err_pass: 'Password is required' });
      if (err === null) err = 'Password is required';
    }

    if (st.password) {
      if (!Validator.isPassword(st.password)) {
        errors++;
        this.setState({ err_passinvalid: 'Password is invalid' });
        if (err === null) err = 'Password is invalid';
      }
    }

    //Confirm password validations
    if (st.confirmpass === null || st.confirmpass === undefined || st.confirmpass == '') {
      errors++;
      this.setState({ err_confirmpass: 'Confirm Password is required' });
      if (err === null) err = 'Confirm Password is required';
    }

    if (st.confirmpass && st.password) {
      if (!Validator.isConfirm(st.password, st.confirmpass)) {
        errors++;
        this.setState({ err_cpassinvalid: 'Password mismatch' });
        if (err === null) err = 'Password mismatch';
      }
    }
    if (errors == 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  resetErrors = () => {
    this.setState({
      err_username: null,
      err_userinvalid: null,
      err_pass: null,
      err_passinvalid: null,
      err_email: null,
      err_emailinvalid: null,
      err_confirmpass: null,
      err_cpassinvalid: null,
      err_firstname: null,
      err_lastname: null,
      error: null
    });
  }

  onFacebookLogin = (profile) => {
    let _this = this;

    if (profile === null || profile === undefined) {
      return;
    }

    let _firstName = profile.name;
    let _lastName = null;

    if (_firstName.indexOf(' ') > 0) {
      _firstName = _firstName.substring(0, _firstName.indexOf(' ')).trim();
      _lastName = profile.name.replace(_firstName, '').trim();
    }

    let imgUrl = null;
    if (profile.picture !== null && profile.picture !== undefined) {
      if (profile.picture.data !== null && profile.picture.data !== undefined) {
        if (profile.picture.data.url !== null && profile.picture.data.url !== undefined) {
          imgUrl = profile.picture.data.url;
        }
      }
    }

    var user = {
      facebookId: profile.id,
      firstName: _firstName,
      lastName: _lastName,
      email: profile.email,
      profileImgUrl: imgUrl
    };

    route.postdata('flogin', user).then(function (res) {
      if (res.status == 200) { //Login was success
        res.json().then(function (data) {
          if (data !== null && data !== undefined && data.id !== null && data.status == 'A') {
            store.dispatch(login(data));
            AsyncStorage.setItem('userId', data.id);
            _this.props.navigation.navigate('banking_profile');
          }
          else {
            _this.setState({
              error: 'Invalid credentials',
              loading: null
            });
          }
        });
      }
      else { //Login failed
        if (res._bodyText.startsWith('ERR:')) {
          res._bodyText = res._bodyText.substring(8);
        }
        _this.setState({
          error: res._bodyText,
          loading: null
        });
      }
    }).catch(function () {
      this.setState({
        error: 'Invalid credentials',
        loading: null
      });
    });
  }

  onGoogleLogin = (profile) => {
    let _this = this;

    if (profile === null || profile === undefined) {
      return;
    }

    var user = {
      googleId: profile.id,
      email: profile.email,
      firstName: profile.givenName,
      lastName: profile.familyName,
      profileImgUrl: profile.photoUrl
    };

    route.postdata('glogin', user).then(function (res) {
      if (res.status == 200) { //Login was success
        res.json().then(function (data) {
          if (data !== null && data !== undefined && data.id !== null && data.status == 'A') {
            store.dispatch(login(data));
            AsyncStorage.setItem('userId', data.id);
            _this.props.navigation.navigate('banking_profile');
          }
          else {
            _this.setState({
              error: 'Invalid credentials',
              loading: null
            });
          }
        });
      }
      else { //Login failed
        if (res._bodyText.startsWith('ERR:')) {
          res._bodyText = res._bodyText.substring(8);
        }
        _this.setState({
          error: res._bodyText,
          loading: null
        });
      }
    }).catch(function () {
      this.setState({
        error: 'Invalid credentials',
        loading: null
      });
    });
  }

  onAltLoginError = (msg) => {
    this.setState({
      error: msg,
      loading: null
    });
  }

  render() {
    let st = this.state;

    return (
      <View style={cs.flex_col_center}>

        <DarkGradient />

        <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='always'>

          <View style={[cs.formbox]}>

            <View style={cs.mt30} />

            <Text style={[cs.headertext, cs.mb10]}>Sign Up</Text>

            <View style={st.err_username !== null ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Login (Email)'
                style={cs.textInputDark}
                onChangeText={(text) => this.setState({ username: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(255,255,255,0.7)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>

            <View style={(st.err_pass !== null || st.err_cpassinvalid !== null) ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/padlock.png')}
                style={{ position: 'absolute', bottom: 12, left: 20, width: 17, height: 22 }}
              />
              <TextInput
                placeholder='Password'
                style={cs.textInputDark}
                onChangeText={(text) => this.setState({ password: text })}
                placeholderTextColor='rgba(255,255,255,0.7)'
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize={'none'}
                autoCorrect={false}
                secureTextEntry
              />
            </View>

            <View style={(st.err_confirmpass !== null || st.err_cpassinvalid !== null) ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/padlock.png')}
                style={{ position: 'absolute', bottom: 12, left: 20, width: 17, height: 22 }}
              />
              <TextInput
                placeholder='Confirm Password'
                style={cs.textInputDark}
                placeholderTextColor='rgba(255,255,255,0.7)'
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize={'none'}
                autoCorrect={false}
                secureTextEntry
                onChangeText={(text) => this.setState({ confirmpass: text })}
              />
            </View>

            <View style={(st.err_firstname !== null) ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 12, left: 20, width: 17, height: 22 }}
              />
              <TextInput
                placeholder='First Name'
                style={cs.textInputDark}
                placeholderTextColor='rgba(255,255,255,0.7)'
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize={'none'}
                autoCorrect={false}
                onChangeText={(text) => this.setState({ firstName: text })}
              />
            </View>

            <View style={(st.err_lastname !== null) ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 12, left: 20, width: 17, height: 22 }}
              />
              <TextInput
                placeholder='Last Name'
                style={cs.textInputDark}
                placeholderTextColor='rgba(255,255,255,0.7)'
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize={'none'}
                autoCorrect={false}
                onChangeText={(text) => this.setState({ lastName: text })}
              />
            </View>

            <View style={cs.textInputFieldDark}>
              <Image
                source={require('../../../images/phone.png')}
                style={{ position: 'absolute', bottom: 8, left: 15, width: 25, height: 25 }}
              />
              <TextInput
                placeholder='Phone'
                style={cs.textInputDark}
                placeholderTextColor='rgba(255,255,255,0.7)'
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize={'none'}
                autoCorrect={false}
                onChangeText={(text) => this.setState({ cellPhone: text })}
              />
            </View>

            <View style={[cs.buttonBox, { marginBottom: 8 }]}>

              <Button
                text='Sign Up'
                width={deviceWidth - 55}
                height={44}
                fontSize={18}
                backgroundColor={colors.green}
                onClick={this.submitForm} />
                
            </View>

            <ErrorBox text={st.error} />

            <Text style={[cs.signuptext, cs.mb10]}>
              {'login with'}
            </Text>

            <AltLogin
              onFacebookLogin={this.onFacebookLogin}
              onGoogleLogin={this.onGoogleLogin}
              onError={this.onAltLoginError}
            />

            <View style={cs.p2} />

            {/* Sign up for account */}
            <View style={cs.noteBox}>
              <Text style={cs.signuptext}>
                {'Have an account ?'}
              </Text>
              <Text style={cs.signuptext2}>
                <Text
                  style={cs.signuptextlink}
                  onPress={this.signin}>
                  Sign In
              </Text>
              </Text>
            </View>

            <View style={cs.mt60} />
          </View>
        </ScrollView>

      </View>
    );
  }
}

PageSignup.propTypes = {
  navigation: PropTypes.object.isRequired
};
// react
import React from 'react';
import PropTypes from 'prop-types';

import { Image, View, Text, TextInput, AsyncStorage, ScrollView, TouchableOpacity } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import DarkGradient from '../../elements/dark_gradient';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';
import AutoCompleteSelect from '../../elements/autocomplete_select';

// modules
//import Validator from '../../modules/validator.js';

// redux
import { login } from '../../redux/actions';
import store from '../../redux/store';

// api
import Route from '../../modules/route.js';
// const route = new Route('https://trifigoapi.azurewebsites.net/');
const route = new Route(LiveUrl);
import { getRefUniversities, LiveUrl } from '../../modules/api';

// styles
import cs from '../../styles/common_styles.js';
import { colors, deviceWidth } from '../../styles/variables';

export default class PageStudentSignup2 extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      signup_data: store.getState().signup_data,
      universities: store.getState().universities,

      unsubscribe: store.subscribe(this.updateState),

      firstName: '',
      lastName: '',

      err_firstname: null,
      err_lastname: null,

      error: null,
      loading: ''
    };

    trackScreenView('StudentSignup2');
  }

  updateState = () => {
    this.setState({
      signup_data: store.getState().signup_data,
      universities: store.getState().universities
    });
  }

  componentDidMount() {
    let st = this.state;
    if (!st.universities) getRefUniversities();
  }
  
  componentWillUnmount() {
    this.state.unsubscribe();
  }
  
  signin = () => {
    this.props.navigation.navigate('login');
  }

  backClick = () => {
    this.props.navigation.navigate('student_signup');
  }
  
  submitForm = () => {
    let _this = this;
    let st = this.state;

    this.setState({ error: null, loading: ' is-loading' });

    this.resetErrors();

    let errors = this.validateError();

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    var user = {
      login: st.signup_data.username,
      password: st.signup_data.password,
      email: st.signup_data.username,
      firstName: st.firstName,
      lastName: st.lastName,
      status: 'A',
    };

    user.userType = 'S';
    
    try {
      if (st.selected_year) user.graduationYear = Number(st.selected_year.name);
      if (st.year) user.graduationYear = Number(st.year);
    } catch(e) { /* no op */ }

    if(st.university) user.univId = st.university;
    if(st.selected_university) user.univId = st.selected_university.id;
      
    //console.log('user >>>', JSON.stringify(user, null, 2));

    route.postdata('user/create', user).then(function (res, err) {

      if (res && res.id) {

        store.dispatch(login(res));

        AsyncStorage.setItem('userId', res.id);

        let email = {
          "subject": "Welcome to Trifigo",
          "html": "<center><p>Thank you for registering for the Trifigo Platform. <br>  <center> <p>Login Credentials : </p></center> <center> <p> UserName :" + st.username + "</p> </center><center> <p> Passsword :" + st.password + "</p> </center>  </center>",
          "name": st.username,
          "fromEmail": "contact@trifigo.com",
          "toEmail": st.username
        };

        route.postdata('mail', email);

        _this.props.navigation.navigate('tour_welcome');
      }
      else {
        _this.setState({
          error: 'User exists',
          loading: null
        });
      }
    });
  }

  validateError = () => {
    let errors = 0;
    let st = this.state;
    let err = null;

    // Validate First Name
    if (st.firstName === null || st.firstName === undefined || st.firstName == '') {
      errors++;
      this.setState({ err_firstname: 'First Name is required' });
    }

    // Validate Last Name
    if (st.lastName === null || st.lastName === undefined || st.lastName == '') {
      errors++;
      this.setState({ err_lastname: 'Last Name is required' });
    }

    if (errors === 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  resetErrors = () => {
    this.setState({
      err_firstname: null,
      err_lastname: null,
      error: null
    });
  }

  selectUniv = (data) => {
    this.setState({ selected_university: data, university : null });
  }

  changeUniv = (data) => {
    this.setState({ university: data, selected_university: null });
  }
  
  selectYear = (data) => {
    this.setState({ selected_year: data, year: null });
  }

  changeYear = (data) => {
    this.setState({ year: data, selected_year: null });
  }
  
  render() {
    let st = this.state;

    let current_year = (new Date()).getFullYear();
    
    let years  = [];
    for (let i = 0; i < 10; i++) {
      years.push({ id: i, name: String(current_year + i) });   
    }
    
    return (
      <View style={cs.flex_col_center}>

        <DarkGradient />

        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='always'>

          <View style={[cs.formbox]}>

            <View style={cs.mt30} />

            <View style={[cs.flex_row_start, cs.mb10]}>
              <TouchableOpacity onPress={this.backClick} style={{ flex: 1 }}>
                <View style={[{ marginTop: 8 }]}>
                  <Image
                    source={require('../../../images/menu-back.png')}
                    style={{ height: 30 }}
                  />
                </View>
              </TouchableOpacity>

              <Text style={[cs.headertext, cs.mb10, { flex: 2 }]}>Sign Up</Text>
            </View>

            <View style={cs.mt8} />
            
            <View style={(st.err_firstname !== null) ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 12, left: 20, width: 17, height: 22 }}
              />
              <TextInput
                placeholder='First Name'
                style={cs.textInputDark}
                placeholderTextColor='rgba(255,255,255,0.7)'
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize={'none'}
                autoCorrect={false}
                onChangeText={(text) => this.setState({ firstName: text })}
              />
            </View>

            <View style={(st.err_lastname !== null) ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 12, left: 20, width: 17, height: 22 }}
              />
              <TextInput
                placeholder='Last Name'
                style={cs.textInputDark}
                placeholderTextColor='rgba(255,255,255,0.7)'
                underlineColorAndroid='rgba(0,0,0,0)'
                autoCapitalize={'none'}
                autoCorrect={false}
                onChangeText={(text) => this.setState({ lastName: text })}
              />
            </View>

            <AutoCompleteSelect
              data={years}
              onSelect={this.selectYear}
              onChange={this.changeYear}
              placeHolder='Graduation Year'
              icon='calendar'
            />

           <AutoCompleteSelect
              data={st.universities}
              onSelect={this.selectUniv}
              onChange={this.changeUniv}
              placeHolder='Affiliated University'
              icon='university'
            />

            <View style={[cs.buttonBox, { marginBottom: 8 }]}>

              <Button
                text='Sign Up'
                width={deviceWidth - 55}
                height={44}
                fontSize={18}
                backgroundColor={colors.green}
                onClick={this.submitForm} />

            </View>

            <ErrorBox text={st.error} />

            <View style={cs.p2} />

            {/* Sign up for account */}
            <View style={cs.noteBox}>
              <Text style={cs.signuptext}>
                {'Have an account?'}
              </Text>
              <Text style={cs.signuptext2}>
                <Text
                  style={cs.signuptextlink}
                  onPress={this.signin}>
                  Sign In
              </Text>
              </Text>
            </View>

            <View style={cs.mt60} />

          </View>

        </KeyboardAwareScrollView>

      </View>
    );
  }
}

PageStudentSignup2.propTypes = {
  navigation: PropTypes.object.isRequired
};
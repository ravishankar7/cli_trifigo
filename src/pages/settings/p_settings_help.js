//react
import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, TextInput, Image, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';

// google analytics
import { trackScreenView } from '../../modules/ga';
import { deviceWidth, blueGradient, fontFamily, colors } from '../../styles/variables';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';

// store
import store from '../../redux/store';

// styles
import cs from '../../styles/common_styles';

export default class PageSettingsHelp extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    let _user = store.getState().user;

    this.state = {
      user: _user,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
      success: null,
    };

    trackScreenView('Settings Help');
  }

  updateState = () => {
    let _user = store.getState().user;

    this.setState({
      user: _user,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  cancel = () => {
    this.props.navigation.navigate('settings');
  }


  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title=''
          back='settings'
        />

        <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

          <View style={{ flex: 1, width: '100%', height: '100%', backgroundColor: 'transparent' }}>

            <View style={{ width: '100%', height: '30%' }}>
              <LinearGradient
                start={{ x: 0.0, y: 0.0 }}
                end={{ x: 1.0, y: 1.0 }}
                colors={blueGradient.colors}
              >

                <View style={{ alignSelf: 'center' }}>
                  <Text numberOfLines={2} style={[cs.headertexthelp, cs.mv10]}>Help & Support</Text>
                </View>

                <View style={{
                  flexDirection: 'row',
                  alignSelf: 'center',
                  width: deviceWidth - 45,
                  height: 44,
                  marginVertical: 7,
                  marginHorizontal: 10,
                  borderWidth: 1,
                  borderColor: 'transparent',
                  borderStyle: 'solid',
                  borderRadius: 50,
                  backgroundColor: 'white'
                }}>

                  <TextInput
                    placeholder='How can we help?'
                    //value={st.name}
                    style={cs.textInput}
                    //onChangeText={(text) => this.setState({ name: text })}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholderTextColor='rgba(0,0,0,0.3)'
                    autoCapitalize={'none'}
                    autoCorrect={false}
                  />
                  <Image
                    source={require('../../../images/search.png')}
                    style={{ position: 'absolute', bottom: 11, right: 20, width: 19, height: 22 }}
                  />

                </View>


                <View style={{ padding: 5, alignSelf: 'center', flexDirection: 'row' }}>
                  <View style={{ marginRight: 4 }}>

                    <Button
                      //onClick={() => this.props.navigate('upload_report')}
                      text='Using Trifigo'
                      width={190}
                      height={49}
                      fontSize={12}
                      backgroundColor={colors.green}
                    />
                  </View>

                  <Button
                    //onClick={() => this.props.navigate('upload_report')}
                    text='Managing Your Account'
                    width={190}
                    height={49}
                    fontSize={10}
                    backgroundColor={colors.green}
                  />
                </View>

                <View style={{ padding: 5, flexDirection: 'row', alignSelf: 'center', marginBottom: 7 }}>
                  <View style={{ marginRight: 4 }}>
                    <Button
                      //onClick={() => this.props.navigate('upload_report')}
                      text='Privacy and Safety'
                      width={190}
                      height={49}
                      fontSize={12}
                      backgroundColor={colors.green}
                    />
                  </View>

                  <Button
                    //onClick={() => this.props.navigate('upload_report')}
                    text="Policies and Reporting"
                    width={190}
                    height={49}
                    fontSize={10}
                    backgroundColor={colors.green}
                  />
                </View>

              </LinearGradient>


              <View style={{
                marginHorizontal: 10,
                top: 1,
                marginVertical: 5,
                marginBottom: 10,
                paddingHorizontal: 0,
                paddingVertical: 5,
                borderRadius: 8,
                backgroundColor: '#fff',
                ...Platform.select({
                  ios: {
                    shadowColor: 'rgba(0,0,0,0.2)',
                    shadowOffset: {
                      width: 0,
                      height: 5
                    },
                    shadowRadius: 5,
                    shadowOpacity: 0.4
                  },
                  android: {
                    elevation: 5,
                  },
                }),
              }} >

                <Text style={{ fontSize: 16, color: '#d3d3d3', margin: 5 }}>Popular Questions</Text>

                <View style={{ alignSelf: 'center', width: '100%', paddingHorizontal: 10 }}>

                  <Text style={{ paddingLeft: 10, fontSize: 16, color: '#7f7f7f', marginVertical: 10 }}>Where can i find my settings?</Text>
                  <View style={{ borderColor: '#E0E0E0', borderBottomWidth: 0.5, }} />
                  <Text style={{ paddingLeft: 10, fontSize: 16, color: '#7f7f7f', marginVertical: 10 }}>How do i change or reset my password?</Text>
                  <View style={{ borderColor: '#E0E0E0', borderBottomWidth: 0.5, }} />
                  <Text style={{ paddingLeft: 10, fontSize: 16, color: '#7f7f7f', marginVertical: 10 }}>How do i find my credit score?</Text>
                  <View style={{ borderColor: '#E0E0E0', borderBottomWidth: 0.5, }} />
                  <Text style={{ paddingLeft: 10, fontSize: 16, color: '#7f7f7f', marginVertical: 10 }}>How do i recommend friends to trifigo?</Text>

                </View>

              </View>

              <View style={{
                marginHorizontal: 10,
                marginVertical: 5,
                marginBottom: 10,
                paddingHorizontal: 0,
                paddingVertical: 5,
                borderRadius: 8,
                backgroundColor: '#fff',
                ...Platform.select({
                  ios: {
                    shadowColor: 'rgba(0,0,0,0.2)',
                    shadowOffset: {
                      width: 0,
                      height: 5
                    },
                    shadowRadius: 5,
                    shadowOpacity: 0.4
                  },
                  android: {
                    elevation: 5,
                  },
                }),
              }} >

                <Text style={{ fontSize: 16, color: '#d3d3d3', margin: 5 }}>Popular Topics</Text>

                <View style={{ alignSelf: 'center', width: '100%', paddingHorizontal: 10, left: 10 }}>

                  <Text style={{
                    color: '#0bb795',
                    fontSize: 16,
                    letterSpacing: 0.75,
                    fontFamily: fontFamily.regular
                  }}>Credit</Text>
                  <Text style={{ fontSize: 13, color: '#7f7f7f' }}>Credit Score</Text>
                  <Text style={{ fontSize: 13, color: '#7f7f7f' }}>Payment History</Text>
                  <Text style={{ fontSize: 13, color: '#7f7f7f' }}>Credit Utilization</Text>
                  <Text style={{ fontSize: 13, color: '#7f7f7f' }}>Account Age</Text>
                  <Text style={{ fontSize: 13, color: '#7f7f7f' }}>Account Mix</Text>
                  <Text style={{ fontSize: 13, color: '#7f7f7f' }}>Credit Inquiries</Text>

                </View>

                <View style={{ height: 10 }} />

                <View style={{ alignSelf: 'center', width: '100%', paddingHorizontal: 10, left: 10 }}>

                  <Text style={{
                    color: '#0bb795',
                    fontSize: 16,
                    letterSpacing: 0.75,
                    fontFamily: fontFamily.regular
                  }}>Banking</Text>
                  <Text style={{ fontSize: 13, color: '#7f7f7f' }}>Checking</Text>
                  <Text style={{ fontSize: 13, color: '#7f7f7f' }}>Savings</Text>
                </View>
                <View style={{ height: 10 }} />

              </View>

            </View>

          </View>
          
        </KeyboardAwareScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettingsHelp.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettingsHelp.defaultProps = {
  user: null,
};

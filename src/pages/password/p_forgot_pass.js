// react
import React from 'react';
import PropTypes from 'prop-types';

import { Image, View, Text, TextInput, TouchableOpacity } from 'react-native';

// external
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

// google analytics
import { trackScreenView } from '../../modules/ga';

// elements
import DarkGradient from '../../elements/dark_gradient';
import Button from '../../elements/button';
import ErrorBox from '../../elements/error_box';

// modules
import Validator from '../../modules/validator.js';

// api
import { getUserByEmail, setUserToken, send_email } from '../../modules/api';

// styles
import cs from '../../styles/common_styles.js';
import { colors, deviceWidth } from '../../styles/variables';

export default class PageForgotPass extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {

      username: '',
      password: '',
      confirmpass: '',

      err_username: null,
      err_userinvalid: null,
      err_pass: null,
      err_passinvalid: null,
      err_confirmpass: null,
      err_cpassinvalid: null,

      error: null,
      loading: ''
    };

    trackScreenView('ForgotPassword1');
  }

  backClick = () => {
    this.props.navigation.navigate('login');
  }

  submitForm = () => {
    let st = this.state;

    this.setState({ error: null, loading: ' is-loading' });

    this.resetErrors();

    let errors = this.validateError();

    if (errors > 0) {
      this.setState({ loading: null });
      return;
    }

    getUserByEmail(st.username).then((res) => {
      let _token = this.randomString(6);
      if(res && res.id) {
        let user = {
          id: res.id,
          token: _token
        }
        setUserToken(user).then(() => {
          let _html = `<h5 style=font-family:courier;>Dear ` + res.firstName + `</h5><p style=font-family:courier;><h5 style=font-family:courier;>Here is your password reset token: ` + _token + `</h5><h5 style=font-family:courier;>Please follow the instrctions on the app to reset your password.</h5></p><br><h5 style=font-family:courier;>Team Trifigo</h5>`;

          let email = {
            subject: 'Reset your Password',
            fromEmail: 'donotreply@trifigo.com',
            toEmail: st.username,
            html: _html
          };

          send_email(email).then(() => {
            this.props.navigation.navigate('forgot_pass_token');
          });
        });
      }
      else {
        this.setState({ error: 'User not found' });
      }
    });
  }

  validateError = () => {
    let errors = 0;
    let st = this.state;
    let err = null;

    //Validate Email
    if (!st.username) {
      errors++;
      this.setState({ err_username: 'Username (Email) is required' });
      if (err === null) err = 'Username (Email) is required';
    }

    if (st.username) {
      if (!Validator.isEmail(st.username)) {
        errors++;
        this.setState({ err_userinvalid: 'Email is invalid' });
        if (err === null) err = 'Email is invalid';
      }
    }

    if (errors === 1) {
      this.setState({ error: err });
    }
    else if (errors > 1) {
      this.setState({ error: 'Correct all errors' });
    }

    return errors;
  }

  resetErrors = () => {
    this.setState({
      err_username: null,
      err_userinvalid: null,
      err_email: null,
      err_emailinvalid: null,
      error: null
    });
  }

  randomString = (length, chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ') => {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }

  render() {
    let st = this.state;

    return (
      <View style={cs.flex_col_center}>

        <DarkGradient />

        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps='always'>

          <View style={[cs.formbox]}>

            <View style={cs.mt30} />

            <View style={[cs.flex_row_start, cs.mb10]}>
              <TouchableOpacity onPress={this.backClick} style={{ flex: 1 }}>
                <View style={[{ marginTop: 8 }]}>
                  <Image
                    source={require('../../../images/menu-back.png')}
                    style={{ height: 30 }}
                  />
                </View>
              </TouchableOpacity>

              <Text style={[cs.headertext, { flex: 5 }]}>
                Reset password
              </Text>
            </View>

            <View style={[cs.bluecontainer]}>
              <Text style={cs.subtitle2white}>
                Enter your login (same as the email you signed up with) and we will send you an email with a unique verification code.
                {'\n'}{'\n'}
                Enter this code in the next step and you can reset your password.
              </Text>
            </View>

            <View style={cs.mt20} />

            <View style={st.err_username !== null ? cs.textInputFieldError : cs.textInputFieldDark}>
              <Image
                source={require('../../../images/avatar.png')}
                style={{ position: 'absolute', bottom: 11, left: 20, width: 19, height: 22 }}
              />
              <TextInput
                placeholder='Your email (login)'
                style={cs.textInputDark}
                onChangeText={(text) => this.setState({ username: text })}
                underlineColorAndroid='rgba(0,0,0,0)'
                placeholderTextColor='rgba(255,255,255,0.7)'
                autoCapitalize={'none'}
                autoCorrect={false}
              />
            </View>


            <View style={[cs.buttonBox, { marginBottom: 8 }]}>

              <Button
                text='Next'
                width={deviceWidth - 55}
                height={44}
                fontSize={18}
                backgroundColor={colors.green}
                onClick={this.submitForm} />

            </View>

            <ErrorBox text={st.error} />

          </View>

        </KeyboardAwareScrollView>

      </View>
    );
  }
}

PageForgotPass.propTypes = {
  navigation: PropTypes.object.isRequired
};
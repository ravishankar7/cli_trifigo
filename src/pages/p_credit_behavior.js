//react
import React from 'react';
import PropTypes from 'prop-types';

import { View, ScrollView, Switch, Text, StyleSheet, Image, TouchableOpacity, Platform } from 'react-native';
//import * as Progress from 'react-native-progress';

// google analytics
import { trackScreenView } from '../modules/ga';

// elements
import Header from '../elements/header';
import Footer from '../elements/footer';

// components

// store
import store from '../redux/store';

// styles
import cs from '../styles/common_styles.js';
import { fontFamily, colors } from '../styles/variables';

export default class PageCreditBehavior extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,

      unsubscribe: store.subscribe(this.updateState),

      switchValue: false, //false -> Main

      error: null,
      loading: null,
    };
    
    trackScreenView('Credit Behavior');
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  handleToggleSwitch = () => {
    this.setState(state => ({
      switchValue: !state.switchValue,
    }));
  }

  renderHairLine() {
    return (
      <View
        style={{
          borderBottomColor: '#202020',
          borderBottomWidth: StyleSheet.hairlineWidth,
          marginVertical: 10,
        }}
      />
    );
  }
  render() {
    const { navigate } = this.props.navigation;
    let st = this.state;

    return (
      <View style={cs.normalPage}>

        <Header
          menu
          title='Credit Behavior'
          navigate={navigate}
        />

        <ScrollView keyboardShouldPersistTaps='always'>
          <View style={[cs.shadowBox, cs.mt20]}>

            {/*** Toggle switch ***/}
            <View style={[cs.flex_row_center]}>
              <TouchableOpacity disabled = {Platform.OS === 'ios'} onPress = {st.switchValue ? this.handleToggleSwitch : null } >
                <Text style={[ !st.switchValue ? cs.title1 : cs.title1darkgray, cs.m5]}>Main</Text>
              </TouchableOpacity>
              <Switch
                style ={cs.mh5}
                onValueChange={this.handleToggleSwitch}
                value={this.state.switchValue}
              />
              <TouchableOpacity disabled = {Platform.OS === 'ios'} onPress = {!st.switchValue ? this.handleToggleSwitch : null } >
                <Text style={[ st.switchValue ? cs.title1 : cs.title1darkgray, cs.m5]}>Personal</Text>
              </TouchableOpacity>
              

            </View>

            <View style={[cs.flex_row_center, cs.mv10]}>
              <Text style={cs.subtitle2black}>
                What your report says about you
              </Text>
            </View>

            <View style={[cs.flex_row_space_between, cs.m5]}>
              <Text style={[cs.title1darkgray]}>CATEGORY</Text>
              <Text style={[cs.title1darkgray]}>BEHAVIOR / PATTERN</Text>
            </View>

            {this.renderHairLine()}

            {/*** Payment History ***/}
            <View style={[cs.flex_row_space_between]}>
              <Text style={[styles.cdCellTitle]}>Payment History</Text>

              <View style={cs.flex_col_center}>

                {st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Got Receipts</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Slacking</Text>
                  </View>
                }

                {!st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Consistent</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Inconsistent</Text>
                  </View>
                }
                {/*
                <Progress.Bar
                  style={{ "marginTop": 5, "marginBottom": 5 }}
                  height={10}
                  borderColor='white'
                  borderRadius={12}
                  borderWidth={0}
                  color='#58D68D'
                  unfilledColor='#EAECEE'
                  progress={0.75}
                  width={200} />
                */}
                <Image style={[styles.creditbar, cs.mt5]} width={200} height={10}
                  source={require('../../images/credit_bar.png')} />

                <View width={200} style={[cs.flex_row_space_between]}>
                  <Text style={[cs.subtitle2gray, cs.mr5]}>Low</Text>
                  <Text style={[cs.subtitle2gray]}>High</Text>
                </View>
              </View>
            </View>

            {this.renderHairLine()}

            {/*** Credit Util ***/}
            <View style={[cs.flex_row_space_between]}>
              <Text style={[styles.cdCellTitle]}>Credit Utilization</Text>

              <View style={cs.flex_col_center}>
                
                {st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Got Options</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Tight</Text>
                  </View>
                }
                
                {!st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Flexible</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Inflexible</Text>
                  </View>
                }

                {/*
                <Progress.Bar
                  style={{ "marginTop": 5, "marginBottom": 5 }}
                  height={10}
                  borderColor='white'
                  borderRadius={12}
                  borderWidth={0}
                  color='#58D68D'
                  unfilledColor='#EAECEE'
                  progress={0.75}
                  width={200} />
                */}
                
                <Image style={[styles.creditbar, cs.mt5]} width={200} height={10}
                  source={require('../../images/credit_bar.png')} />

                <View width={200} style={[cs.flex_row_space_between]}>
                  <Text style={[cs.subtitle2gray, cs.mr5]}>Low</Text>
                  <Text style={[cs.subtitle2gray]}>High</Text>
                </View>
              </View>
            </View>

            {this.renderHairLine()}

            {/*** Credit Age ***/}
            <View style={[cs.flex_row_space_between]}>
              <Text style={[styles.cdCellTitle]}>Credit Age</Text>

              <View style={cs.flex_col_center}>
                
                {st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>OG</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>No Cred</Text>
                  </View>
                }

                {!st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Longevity</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Starting</Text>
                  </View>
                }

                {/*
                <Progress.Bar
                  style={{ "marginTop": 5, "marginBottom": 5 }}
                  height={10}
                  borderColor='white'
                  borderRadius={12}
                  borderWidth={0}
                  color='#58D68D'
                  unfilledColor='#EAECEE'
                  progress={0.75}
                  width={200} />
                */}

                <Image style={[styles.creditbar, cs.mt5]} width={200} height={10}
                  source={require('../../images/credit_bar.png')} />

                <View width={200} style={[cs.flex_row_space_between]}>
                  <Text style={[cs.subtitle2gray, cs.mr5]}>Low</Text>
                  <Text style={[cs.subtitle2gray]}>High</Text>
                </View>
              </View>
            </View>

            {this.renderHairLine()}

            {/*** Credit Inquiries ***/}
            <View style={[cs.flex_row_space_between]}>
              <Text style={[styles.cdCellTitle]}>Credit Inquiries</Text>

              <View style={cs.flex_col_center}>
                
                {st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>No Stress</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Pressed</Text>
                  </View>
                }

                {!st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Calm</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Desperation</Text>
                  </View>
                }

                {/*
                <Progress.Bar
                  style={{ "marginTop": 5, "marginBottom": 5 }}
                  height={10}
                  borderColor='white'
                  borderRadius={12}
                  borderWidth={0}
                  color='#58D68D'
                  unfilledColor='#EAECEE'
                  progress={0.75}
                  width={200} />
                */}

                <Image style={[styles.creditbar, cs.mt5]} width={200} height={10}
                  source={require('../../images/credit_bar.png')} />

                <View width={200} style={[cs.flex_row_space_between]}>
                  <Text style={[cs.subtitle2gray, cs.mr5]}>Low</Text>
                  <Text style={[cs.subtitle2gray]}>High</Text>
                </View>
              </View>
            </View>

            {this.renderHairLine()}

            {/*** Account Mix ***/}
            <View style={[cs.flex_row_space_between]}>
              <Text style={[styles.cdCellTitle]}>Total Accounts</Text>

              <View style={cs.flex_col_center}>
                
                {st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Connected</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Low Low</Text>
                  </View>
                }

                {!st.switchValue &&
                  <View style={[styles.bluecontainerslim, cs.flex_row_space_between, cs.p2]} width={200}>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Plenty</Text>
                  <Text style={[cs.subtitle2white, cs.mh5]}>Few</Text>
                  </View>
                }

                {/* 
                <Progress.Bar
                  style={{ "marginTop": 5, "marginBottom": 5 }}
                  height={10}
                  borderColor='white'
                  borderRadius={12}
                  borderWidth={0}
                  color='#58D68D'
                  unfilledColor='#EAECEE'
                  progress={0.75}
                  width={200} />
                */}

                <Image style={[styles.creditbar, cs.mt5]} width={200} height={10}
                  source={require('../../images/credit_bar.png')} />

                <View width={200} style={[cs.flex_row_space_between]}>
                  <Text style={[cs.subtitle2gray, cs.mr5]}>Low</Text>
                  <Text style={[cs.subtitle2gray]}>High</Text>
                </View>
              </View>
            </View>
            
          </View>

          <View style={cs.bluecontainer}>
            <Text style={cs.subtitle2white}>
              These illustrate the types of underlying behaviors associated with each credit scoring category. 
            </Text>
            <Text style={[cs.subtitle2white, cs.mt10]}>
              Understanding and improving your behavior leads to improvement in your financial decision making.
            </Text>
          </View>

          <View style={cs.m10} />

        </ScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageCreditBehavior.propTypes = {
  navigation: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  creditbar: {
    flex: 1,
    resizeMode: 'stretch',
    width: undefined,
    height: 20,
    transform: [{ rotate: '180 deg' }]
  },
  bluecontainerslim: {
    backgroundColor: '#4E6D87',
    borderRadius: 4,
  },
  cdCellTitle : {
      flex:1,
			fontSize: 14,
			fontFamily: fontFamily.semiBold,
			fontWeight: '600',
			justifyContent: 'flex-start',
			color: colors.green,
			letterSpacing: 0.5
  }
});

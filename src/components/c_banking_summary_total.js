// react
import React from 'react';
import { Text, View } from 'react-native';

// store
import store from '../redux/store';

// api
import { getBankingTotalsSummary } from '../modules/api';

// styles
import cs from '../styles/common_styles.js';

export default class BankingAccountSummaryTotal extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			user: store.getState().user,
			bankingTotalsSummary: store.getState().bankingTotalsSummary,

			unsubscribe: store.subscribe(this.updateState),

			error: null,
			loading: null,

			accountdata: {},
		};
	}

	updateState = () => {
		this.setState({
			user: store.getState().user,
			bankingTotalsSummary: store.getState().bankingTotalsSummary,
		});
	}

	componentDidMount() {
		if (this.state.user === null) return;
		
		let _this = this;
		let userId = this.state.user.id;

		if (this.state.bankingTotalsSummary === null) {
			try { getBankingTotalsSummary(userId); }
			catch (e) { _this.setState({ error: e }); }
		}
	}

	componentWillUnmount() {
		this.state.unsubscribe();
	}

	render() {
		let st = this.state;

		return (
			<View style={[cs.shadowBox]}>
				<View style={[cs.flex_row_space_between]}>
					<Text style={[cs.title1, cs.mt2, cs.ml10]}>
						Assets
					</Text>
					<Text style={[cs.title1, cs.mt2, cs.mr10]}>
						$ 0 {st.bankingTotalsSummary !== null ? st.bankingTotalsSummary.assets : '0'}
					</Text>
				</View>

				<View style={[cs.flex_row_space_between]}>
					<Text style={[cs.title1, cs.mt10, cs.ml10]}>
						Liabilities
					</Text>
					<Text style={[cs.title1, cs.mt10, cs.mr10]}>
						$ 0 {st.bankingTotalsSummary !== null ? st.bankingTotalsSummary.liabilities : '0'}
					</Text>
				</View>

				<View style={[cs.flex_row_space_between]}>
					<Text style={[cs.title1, cs.mt10, cs.ml10]}>
						Net Worth
					</Text>
					<Text style={[cs.title1, cs.mt10, cs.mr10]}>
						$ 0 {st.bankingTotalsSummary !== null ? st.bankingTotalsSummary.total : '0'}
					</Text>
				</View>
			</View>
		);
	}
}
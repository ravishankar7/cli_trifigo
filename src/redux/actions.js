//#region login, logout, reset >>
export const setUserId = (payload) => {
	const action = {
		type: 'SET_USER_ID',
		payload: payload
	};
	return action;
};

export const getSecurityQuestion = (payload) => {
	const action = {
		type:'GET_SECURITY_QUESTIONS',
		payload: payload
	};
	return action;
};

export const storeGoals = (payload) => {
	//console.log('In actions:',payload )
	const action = {
		type:'STORE_GOALS',
		payload: payload
	};
	return action;
};

export const getUserGoals = (payload) => {
	//console.log('Get user goal:',payload )
	const action = {
		type:'GET_USER_GOALS',
		payload: payload
	};
	return action;
};

export const reset = () => {
	const action = {
		type: 'RESET',
	};
	return action;
};

export const login = (payload) => {
	const action = {
		type: 'LOGIN',
		payload: payload
	};
	return action;
};

export const logout = () => {
	const action = {
		type: 'LOGOUT'
	};
	return action;
};

export const setUser = (payload) => {
	const action = {
		type: 'SET_USER',
		payload: payload
	};
	return action;
};


export const setSignupData = (payload) => {
	const action = {
		type: 'SET_SIGNUP',
		payload: payload
	};
	return action;
};
//#endregion

//#region onesignal device
export const setOneSignalDevice = (payload) => {
	const action = {
		type: 'SET_ONESIGNAL_DEVICE',
		payload: payload
	};
	return action;
};
//#endregion

//#region summary apis
export const setYodleeAccountSummary = (payload) => {
	const action = {
		type: 'SET_YODLEE_ACCOUNT_SUMMARY',
		payload: payload
	};
	return action;
};

export const setQuovoAccountSummary = (payload) => {
	const action = {
		type: 'SET_QUOVO_ACCOUNT_SUMMARY',
		payload: payload
	};
	return action;
};

export const setConsumerTypeSummary = (payload) => {
	const action = {
		type: 'SET_CONSUMER_TYPE_SUMMARY',
		payload: payload
	};
	return action;
};

export const setAdditionalCreditSummary = (payload) => {
	const action = {
		type: 'SET_ADDTIONAL_CREDIT_SUMMARY',
		payload: payload
	};
	return action;
};

export const setPaymentSummary = (payload) => {
	const action = {
		type: 'SET_PAYMENT_SUMMARY',
		payload: payload
	};
	return action;
};

export const setCreditUtilizationSummary = (payload) => {
	const action = {
		type: 'SET_CREDIT_UTILIZATION_SUMMARY',
		payload: payload
	};
	return action;
};

export const setAccountAgeSummary = (payload) => {
	const action = {
		type: 'SET_ACCOUNT_AGE_SUMMARY',
		payload: payload
	};
	return action;
};

export const setOtherFactorsSummary = (payload) => {
	const action = {
		type: 'SET_OTHER_FACTORS_SUMMARY',
		payload: payload
	};
	return action;
};

export const setCreditBossRatingSummary = (payload) => {
	const action = {
		type: 'SET_CREDIT_BOSS_RATING_SUMMARY',
		payload: payload
	};
	return action;
};

export const setAccountMixSummary = (payload) => {
	const action = {
		type: 'SET_ACCOUNT_MIX_SUMMARY',
		payload: payload
	};
	return action;
};

export const setCreditInquirySummary = (payload) => {
	const action = {
		type: 'SET_CREDIT_INQUIRY_SUMMARY',
		payload: payload
	};
	return action;
};

export const setPastCashFlowSummary = (payload) => {
	const action = {
		type: 'SET_PAST_CASH_FLOW_SUMMARY',
		payload: payload
	};
	return action;
};

export const setCurrentCashFlowSummary = (payload) => {
	const action = {
		type: 'SET_CURRENT_CASH_FLOW_SUMMARY',
		payload: payload
	};
	return action;
};

export const setBankingTotalsSummary = (payload) => {
	const action = {
		type: 'SET_BANKING_TOTALS_SUMMARY',
		payload: payload
	};
	return action;
};

export const setRecos = (payload) => {
	const action = {
		type: 'SET_RECOS',
		payload: payload
	};
	return action;
};

export const setAllRecos = (payload) => {
	const action = {
		type: 'SET_ALL_RECOS',
		payload: payload
	};
	return action;
};
//#endregion

//#region ref data
export const setRefUniversities = (payload) => {
	const action = {
		type: 'SET_REF_UNIVERSITIES',
		payload: payload
	};
	return action;
};


export const setRefConsumerTypes = (payload) => {
	const action = {
		type: 'SET_REF_CONSUMER_TYPES',
		payload: payload
	};
	return action;
};

export const setRefPaymentSentimentTypes = (payload) => {
	const action = {
		type: 'SET_PAYMENT_SENTIMENT_TYPES',
		payload: payload
	};
	return action;
};

export const setRefCreditUtilizationSentimentTypes = (payload) => {
	const action = {
		type: 'SET_CREDIT_UTILIZATION_SENTIMENT_TYPES',
		payload: payload
	};
	return action;
};

export const setRefCreditInquirySentimentTypes = (payload) => {
	const action = {
		type: 'SET_CREDIT_INQUIRY_SENTIMENT_TYPES',
		payload: payload
	};
	return action;
};

export const setRefOtherFactorsSentimentTypes = (payload) => {
	const action = {
		type: 'SET_OTHER_FACTORS_SENTIMENT_TYPES',
		payload: payload
	};
	return action;
};

export const setRefAccountAgeSentimentTypes = (payload) => {
	const action = {
		type: 'SET_ACCOUNT_AGE_SENTIMENT_TYPES',
		payload: payload
	};
	return action;
};

export const setRefAccountMixSentimentTypes = (payload) => {
	const action = {
		type: 'SET_ACCOUNT_MIX_SENTIMENT_TYPES',
		payload: payload
	};
	return action;
};

export const setCreditQuiz = (payload) => {
	const action = {
		type: 'SET_REF_CREDIT_QUIZ',
		payload: payload
	};
	return action;
};

export const setRecoTemplates = (payload) => {
	const action = {
		type: 'SET_RECO_TEMPLATES',
		payload: payload
	};
	return action;
};

export const setDealItems = (payload) => {
	const action = {
		type: 'SET_DEAL_ITEMS',
		payload: payload
	};
	return action;
};

//changes 26 oct 2018
export const setNetworkStatus = (payload) => {
	console.log("CALLED ???", payload)
	const action = {
		type: 'SET_NETWORK_STATUS',
			payload: payload
	};
	return action;
}
//#endregion
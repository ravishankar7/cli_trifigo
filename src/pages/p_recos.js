//react
import React from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView } from 'react-native';

// google analytics
import { trackScreenView } from '../modules/ga';

// elements
import Header from '../elements/header';
import Footer from '../elements/footer';
import TabBar from '../elements/tab_bar';

// components
import Reco from '../components/c_reco';

// store
import store from '../redux/store';

// api
import { getRecos, getAllRecos } from '../modules/api';

// styles
import cs from '../styles/common_styles.js';

export default class PageRecos extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      recos: store.getState().recos,
      allRecos: store.getState().allRecos,

      unsubscribe: store.subscribe(this.updateState),

      selextedTab: 'ACTIVE',

      error: null,
      loading: null,
    };

    trackScreenView('Recos');
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      recos: store.getState().recos,
      allRecos: store.getState().allRecos,
    });
  }

  componentDidMount() {
    let _this = this;
    let userId = this.state.user.id;

    if (this.state.recos === null) {
      try { getRecos(userId, 'N'); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  tabSelected = (tab) => {
    let _this = this;
    let userId = this.state.user.id;

    this.setState({ selextedTab: tab });

    if(tab == 'ACTIVE') {
      try { getRecos(userId, 'N'); }
      catch (e) { _this.setState({ error: e }); }
    }
    else {
      try { getAllRecos(userId); }
      catch (e) { _this.setState({ error: e }); }
    }
  }

  render() {
    let st = this.state;

    let recos = st.selextedTab == 'ACTIVE' ? st.recos : st.allRecos;

    const { navigation : { navigate }, navigation } = this.props;

    return (
      <View style={cs.normalPage}>

        <Header
          // menu
          hideRight = {true}
          // back='settings'
          navigation={navigation}
          back='credit_profile'
          title='Recos'
          navigate={navigate}
        />

        <TabBar 
          tabs={['ACTIVE', 'PAST']} 
          active={'ACTIVE'} 
          onSelect={(tab) => this.tabSelected(tab)} />

        <ScrollView keyboardShouldPersistTaps='always'>
          
          <View style={cs.mt10}/>

          {recos !== null && recos.length > 0 && 
            recos.map(function (reco, i) {
              return <Reco key={i} reco={reco} />;
            })
          }

        </ScrollView>

        {/* <Footer navigate={navigate} /> */}

      </View>
    );
  }
}

PageRecos.propTypes = {
  navigation: PropTypes.object.isRequired
};
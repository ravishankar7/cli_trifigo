// react
import React from 'react';
import PropTypes from 'prop-types';

import { View, ScrollView, Keyboard, Text, StyleSheet } from 'react-native';

// google analytics
import { trackScreenView } from '../modules/ga';

import UserInactivity from 'react-native-user-inactivity';

// elements
import Header from '../elements/header';
import Footer from '../elements/footer';

// redux
import store from '../redux/store';
import { login } from '../redux/actions';
//Modal class
import PrimeModal from '../elements/prime_modal'
import Button from '../elements/button';
// components
import UserInfo from '../components/c_user_info';
//import Goals from '../components/c_goals';
import BankingTip from '../components/c_banking_tip';
import CurrentCashFlow from '../components/c_current_cash_flow';
import PastCashFlow from '../components/c_past_cash_flow';
import GotoDeals from '../components/c_goto_deals';
import BankingAccountCount from '../components/c_banking_account_count';
import BankingAccountSummary from '../components/c_banking_account_summary';
import BankingAccountSummaryTotal from '../components/c_banking_summary_total';
import Advertising from '../components/c_advertising';
import GotoFeedback from '../components/c_goto_feedback';

// styles
import cs from '../styles/common_styles.js';
import { fontFamily, fontSize, colors, deviceWidth } from '../styles/variables';

export default class PageBankingProfile extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      error: null,
      loading: '',
      active: true,
      text: '',
      timer: null
    };

    trackScreenView('Banking Profile');
  }

  componentDidMount () {
    Keyboard.dismiss();
  }
  
  onAction = (active) => {
    this.setState({
      active,
    });
  }

  startTimer = () => {
    console.log('Timer started')
    this.timer = setTimeout(() => {
      this.props.navigation.navigate('login');
    }, 5000);
  }

  render() {
    const st = this.state;
    const { navigate } = this.props.navigation;

    return (
      <UserInactivity
          style={cs.normalPage} 
          timeForInactivity={120000}
          onAction={this.onAction}>
          
        <Header
          menu
          title='Banking Profile'
          navigate={navigate}
        />
        
        <ScrollView keyboardShouldPersistTaps='always'>

          <UserInfo
            navigate={navigate}
            bankinginfo
          />
          
          {/* <Text>
          Put your app here: 
          {
            st.active ? 'Active' : 'Inactive'
          }
        </Text> */}
          <BankingTip navigate={navigate} />
          
          {/*
          <Goals
            navigate={navigate}
          />
          */}
          
          <CurrentCashFlow
            navigate={navigate}
          />
          
          <PastCashFlow
            navigate={navigate}
          />

          <GotoDeals
            navigate={navigate}
          />

          <BankingAccountCount
            navigate={navigate}
          />

          <BankingAccountSummary
            navigate={navigate}
            assets
          />

          <BankingAccountSummary
            navigate={navigate}
            liabilities
          />

          <BankingAccountSummaryTotal
            navigate={navigate}
          />

          <GotoFeedback navigateTo = 'banking_profile' navigate={navigate} />
          
          <Advertising navigate={navigate} />

          <View style={cs.mt10} />

        </ScrollView>

        <PrimeModal
            modalVisible={!st.active}
            onRequestClose={() => { this.setState({ active: true }) }}
            onModalShow = {() => this.startTimer()}
            onDismissProp={clearTimeout(this.timer)}
            >
            <View style={[cs.shadowBox, styles.wp]}>
              <View style={[cs.flex_row_start, cs.p5]}>
                <Text style={styles.headertextpopup}>Inactivity detected</Text>
              </View>
              <Text style={[cs.p10]}>Auto Logout in 5 seconds</Text>
              <View style={[cs.flex_center, cs.p10, cs.mb20]}>
                <Button
                  text='Close'
                  width={100}
                  height={28}
                  fontSize={13}
                  backgroundColor='#546E7A'
                  onClick={() => this.setState({ active: true })} />
              </View>
            </View>
          </PrimeModal>

        <Footer navigate={navigate} />

      </UserInactivity>
    );
  }
}

PageBankingProfile.propTypes = {
  navigation: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  headertext: {
    fontFamily: fontFamily.normal,
    fontSize: fontSize.xsmall
  },
  headertextpopup: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.normal
  },
  subtext: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.xsmall,
    color: colors.green,
  },
  subtextpopup: {
    fontFamily: fontFamily.normal,
    fontSize: fontSize.normal,
    color: colors.green,
  },
  w: {
    width: deviceWidth / 3
  },
  wp: {
    width: deviceWidth - 50
  }
});
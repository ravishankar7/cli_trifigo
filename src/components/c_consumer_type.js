// react
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

// elements
import PrimeModal from '../elements/prime_modal';
import Button from '../elements/button';

// store
import store from '../redux/store';
import { setConsumerTypeSummary } from '../redux/actions';

// router
import Route from '../modules/route.js';
// const route = new Route('https://trifigoapi.azurewebsites.net/');
const route = new Route('http://trifigoapi.us-west-2.elasticbeanstalk.com/');

// styles
import cs from '../styles/common_styles';
import { colors, fontFamily, fontSize, deviceWidth } from '../styles/variables';

export default class ConsumerType extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      consumerTypes: store.getState().consumerTypes,
      consumerTypeSummary: store.getState().consumerTypeSummary,

      unsubscribe: store.subscribe(this.updateState),

      modalVisible: false,

      error: null,
      loading: null,
    };
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      consumerTypes: store.getState().consumerTypes,
      consumerTypeSummary: store.getState().consumerTypeSummary
    });
  }

  componentDidMount() {
    let userId = this.state.user.id;

    if (this.state.consumerTypeSummary === null) {
      this.getConsumerTypeSummary(userId);
    }
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }
  
  getConsumerTypeSummary = (id) => {
    let _this = this;
    route.post('consumertypesummary/' + id).then(function (res, err) {
      if(res !== undefined) {
        store.dispatch(setConsumerTypeSummary(res));
      } 
      else if (err !== undefined) {
        _this.setState({
          error: 'Error',
          loading: null
        });
      }
    });
  }

  render() {
    let st = this.state;

    let _id = (st.consumerTypeSummary !== null 
      && st.consumerTypeSummary !== undefined 
      && st.consumerTypeSummary !== '') 
      ? st.consumerTypeSummary.consumerType : 0;
    let consumerType = null;

    let _rationale = 'Link accounts to see information';
    let _consumerType = 'New';
    //TODO
    // let _consumerType = '-';
    //let _riskCategory = '-';
    //let _cashFlow = '-';

    if (_id > 0 && st.consumerTypes !== null) {
      st.consumerTypes.forEach(element => {
        if (element.id == _id) {
          consumerType = element;
        }
      });
    }

    if(consumerType !== null) {
      _rationale = consumerType.rationale;
      _consumerType = consumerType.consumerType;
      //_riskCategory = consumerType.riskCategory;
      //_cashFlow = consumerType.cashFlow;
    }

    return (
      <TouchableOpacity style={styles.w} activeOpacity={0.8} onPress={() => this.setState({ modalVisible: true })}>
        <View style={cs.shadowBox}>

          <View style={cs.flex_center}>
            <Text style={[styles.headertext]}>Consumer Type</Text>
          </View>

          <View style={cs.flex_center}>
            <Text style={styles.subtext}>{_consumerType}</Text>
          </View>

          <PrimeModal
            modalVisible={st.modalVisible}
            onRequestClose={() => { this.setState({ modalVisible: false })}}>
            <View style={[cs.shadowBox, styles.wp]}>
              <View style={[cs.flex_row_start, cs.p5]}>
                <Text style={styles.headertextpopup}>Consumer Type 
                  <Text style={styles.subtextpopup}> {_consumerType}</Text>
                </Text>
              </View>
              <Text style={[cs.p10]}>{_rationale}</Text>
              <View style={[cs.flex_center, cs.p10, cs.mb20]}>
                <Button
                  text='Close'
                  width={100}
                  height={28}
                  fontSize={13}
                  backgroundColor='#546E7A'
                  onClick={() => this.setState({ modalVisible: false })} />
                </View>
            </View> 
          </PrimeModal>

        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  headertext: {
    fontFamily: fontFamily.normal,
    fontSize: fontSize.xsmall
  },
  headertextpopup: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.normal
  },
  subtext: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.xsmall,
    color: colors.green,
  },
  subtextpopup: {
    fontFamily: fontFamily.normal,
    fontSize: fontSize.normal,
    color: colors.green,
  },
  w: {
    width: deviceWidth / 3,
  },
  wp: {
    width: deviceWidth - 50
  }
});
//react
import React from 'react';
import PropTypes from 'prop-types';

import { StyleSheet, Text, TouchableOpacity, View, ScrollView, Image, Platform, Switch, AsyncStorage } from 'react-native';
import { Avatar } from 'react-native-elements';

import TouchID from 'react-native-touch-id';
import { getSecurityQuestions } from '../../modules/api';

// google analytics
import { trackScreenView, trackEvent } from '../../modules/ga';

// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';
import Button from '../../elements/button';

// store
import store from '../../redux/store';

// styles
import cs from '../../styles/common_styles';
import { fontFamily, fontSize, deviceWidth } from '../../styles/variables';

export default class PageSettings extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      usefaceid: false,
      supportedMode: null,

      unsubscribe: store.subscribe(this.updateState),

      error: null,
      loading: null,
    };

    trackScreenView('Settings');
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
    });
  }

  componentWillUnmount() {
    this.state.unsubscribe();
  }

  componentDidMount() {
    getSecurityQuestions();

    let _this = this;
    AsyncStorage.getItem('usefaceid').then((item) => {
      if (item) {
        _this.setState({ usefaceid: item === 'true' });
      }
    });

    TouchID.isSupported()
      .then(biometryType => {
        if (biometryType === 'TouchID') { // Touch ID is supported on iOS
          _this.setState({ supportedMode: 'touchid' });
        }
        else if (biometryType === 'FaceID') { // Face ID is supported on iOS
          _this.setState({ supportedMode: 'faceid' });
        }
        else if (biometryType === true) { // Touch ID is supported on Android
          _this.setState({ supportedMode: 'touchid' });
        }
      })
      .catch(() => {
        //console.log('Touch ID error', error);
        // User's device does not support Touch ID (or Face ID)
        // This case is also triggered if users have not enabled Touch ID on their device
      });
  }

  handleFaceIdSwitch = () => {
    this.setState(state => ({
      usefaceid: !state.usefaceid,
    }));
    AsyncStorage.setItem('usefaceid', String(!this.state.usefaceid));
  }

  logout = () => {
    let user = this.state.user;
    //console.log('user >>>', user);
    trackEvent('Logout', user.id);
    this.props.navigation.navigate('login');
  }

  renderMenuHeader = (title) => {
    return (
      <View style={cs.flex_row_space_between}>
        <Text style={[cs.title1black, cs.m5]}>{title}</Text>
      </View>
    );
  }

  renderMenu = (title, nav) => {
    return (
      <TouchableOpacity activeOpacity={0.9}
        onPress={() => this.props.navigation.navigate(nav)}>
        <View style={cs.shadowBox}>
          <View style={[cs.flex_row_space_between, styles.subcontainer]}>
            <Text style={[styles.subtitletext, cs.ml5]}>{title}</Text>
            <Image
              source={require('../../../images/chevron-right.png')}
              style={{ height: 15, marginTop: 6, marginRight: 4 }}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  renderInnerMenu = (title, nav) => {
    return (
      <TouchableOpacity activeOpacity={0.9}
        onPress={() => this.props.navigation.navigate(nav)}>
        <View style={[cs.flex_row_space_between, styles.subcontainer2]}>
          <Text style={[styles.subtitletext, cs.ml5]}>{title}</Text>
          <Image
            source={require('../../../images/chevron-right.png')}
            style={{ height: 15, marginTop: 6, marginRight: 4 }}
          />
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;

    if (!user) { return (<View style={cs.normalPage} />); }

    let profileImgUrl = 'https://s3-us-west-2.amazonaws.com/tfgimages/null/ppic.png';

    if (user.profileImgUrl) {
      profileImgUrl = user.profileImgUrl;
    }

    let coverImgUrl = 'https://s3-us-west-2.amazonaws.com/tfgimages/null/bg.png';

    if (user.coverImgUrl) {
      coverImgUrl = user.coverImgUrl;
    }

    return (
      <View style={cs.normalPage}>

        <Header
          // menu
          rightBack
          hideRight
          // back
          title='Settings'
          navigate={navigate}
        />

        <ScrollView keyboardShouldPersistTaps='always'>

          {/*** Avatar and Cover Image ***/}
          <View style={cs.flex_center}>
            <TouchableOpacity activeOpacity={0.9}
              // onPress={() => this.props.navigation.navigate('settings_images')}
              >
              <View>
                <Image
                  source={{ uri: coverImgUrl }}
                  style={styles.imageBackground} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.9} style={styles.usericon}
              onPress={() => this.props.navigation.navigate('settings_images')}>
              <View>
                <Avatar
                  width={105}
                  height={105}
                  rounded
                  source={{ uri: profileImgUrl }}
                  activeOpacity={0.7}
                />
              </View>
            </TouchableOpacity>

          </View>
          <View style = {{ height :50 }}/>
          <View style={styles.imgedit}>
            <TouchableOpacity activeOpacity={0.9}
              onPress={() => this.props.navigation.navigate('settings_images')}>
              <View style={cs.shadowBox}>
                <View style={[cs.flex_row_space_between, styles.subcontainer]}>
                  <Text style={styles.subtitletext}>Images</Text>
                  <Image
                    source={require('../../../images/chevron-right.png')}
                    style={{ height: 15, marginTop: 6, marginRight: 4 }}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>

          {this.renderMenu('Link Accounts', 'link_account')}

          {/* this.renderMenu('Invite Friends', 'refer_friends') */}

          {this.renderMenu('Refresh Data', 'settings_refresh')}

          {this.renderMenu('Product Tour', 'tour_welcome')}

          {this.renderMenu('App Feedback', 'feedback')}

          {/*** Face ID / Touch ID ***/}
          {st.supportedMode &&
            <TouchableOpacity activeOpacity={0.9}>
              <View style={cs.shadowBox}>
                <View style={[cs.flex_row_space_between, styles.subcontainer]}>
                  <Text style={[styles.subtitletext, cs.ml5]}>Face ID / Touch ID Login </Text>
                  <Switch
                    style={{ margin: -2 }}
                    onValueChange={this.handleFaceIdSwitch}
                    value={this.state.usefaceid}
                  />
                </View>
              </View>
            </TouchableOpacity>
          }

          { /*** Identity ***/ }
          <View style={cs.shadowBox}>

            {this.renderMenuHeader('Identity')}

            <View style={styles.breakline} />

            {this.renderInnerMenu('Name', 'settings_name')}
            
            <View style={styles.breakline} />

            {this.renderInnerMenu('Address', 'settings_address')}

            <View style={styles.breakline} />

            {this.renderInnerMenu('Phone', 'settings_phone')}
            
            <View style={styles.breakline} />

            {this.renderInnerMenu('Email', 'settings_email')}

            <View style={styles.breakline} />

            {this.renderInnerMenu('Date of Birth', 'settings_dob')}

          </View>

          <View style={cs.shadowBox}>

            {this.renderMenuHeader('Work')}

            <View style={styles.breakline} />

            {this.renderInnerMenu('Employer', 'settings_work_employer')}

          </View>

          {this.renderMenu('School', 'settings_school')}

          {this.renderMenu('Security Questions', 'security_questions')}

          {/*this.renderMenu('Help & Support', 'settings_help')*/}

          {this.renderMenu('Privacy Policy', 'settings_privacy')}


          {/*** Show Email ***/}
          {user &&
            <View style={cs.shadowBox}>
              {user.email &&
                <View style={styles.subcontainer}>
                  <Text style={styles.subtitletext_s}>Email {user.email}</Text>
                </View>
              }
              {user.id &&
                <View style={styles.subcontainer}>
                  <Text style={styles.subtitletext_s}>UserId {user.id}</Text>
                </View>
              }
            </View>
          }

          <View style={[cs.flex_row_center, cs.m10]}>
            <Button
              text='Logout'
              width={250}
              height={36}
              fontSize={16}
              backgroundColor='#D32F2F'
              onClick={this.logout} />
          </View>

          <View style={cs.mt10} />

        </ScrollView>

        <Footer navigate={navigate} />

      </View>
    );
  }
}

PageSettings.propTypes = {
  navigation: PropTypes.object.isRequired
};

PageSettings.defaultProps = {
  user: null,
};

const styles = StyleSheet.create({
  imgedit: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: -45,
  },
  subtitletext: {
    color: '#0bb795',
    fontSize: 16,
    letterSpacing: 0.75,
    fontFamily: fontFamily.regular
  },
  subtitletext_s: {
    color: '#0bb795',
    fontSize: 12,
    letterSpacing: 0.75,
    fontFamily: fontFamily.regular
  },
  subcontainer: {
    margin: 1,
    padding: 2,
  },
  subcontainer2: {
    margin: 1,
    marginVertical: 6,
    padding: 6,
    height: 36
  },
  breakline: {
    borderColor: '#E0E0E0',
    borderBottomWidth: 0.8,
  },
  usericon: {
    zIndex: 100,
    marginTop: -30,
    backgroundColor: 'white',
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.1)',
        shadowOffset: {
          width: 0,
          height: 10
        },
        shadowRadius: 6,
        shadowOpacity: 0.4
      },
      android: {
        elevation: 6,
      },
    }),
    borderRadius: 100
  },
  usertype: {
    flex: 1,
    flexDirection: 'row',
    marginTop: -50,
  },
  ct: {
    minWidth: 135,
  },
  ac: {
    minWidth: 135,
  },
  vv: {
    width: 115
  },
  username: {
    marginTop: -10,
    flexDirection: 'row',
    justifyContent: 'center',
    color: '#000',
    fontSize: fontSize.title,
    fontFamily: fontFamily.medium,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0,0.8)',
        shadowOffset: {
          width: 0,
          height: 10
        },
        shadowRadius: 6,
        shadowOpacity: 0.4
      },
      android: {
        elevation: 6,
      },
    }),
    backgroundColor: 'transparent'
  },
  imageBackground: {
    height: 120,
    width: deviceWidth,
  }
});

//react
import React from 'react';
import PropTypes from 'prop-types';

import { Text, TouchableOpacity, View, FlatList } from 'react-native';

import { createGoal, getUserGoal } from '../../modules/api';

// google analytics
import { trackScreenView } from '../../modules/ga';


// elements
import Header from '../../elements/header';
import Footer from '../../elements/footer';

// store
import store from '../../redux/store';

// styles
import cs from '../../styles/common_styles';
import { colors } from '../../styles/variables';
import Icon from 'react-native-vector-icons/Ionicons';


export default class GoalsSelect extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      user: store.getState().user,
      my_goals: store.getState().this_user_goals,
      laterLinking: false,
      error: null,
      loading: null,
      selectedCount: 0,
      goals: [
        { id: 0, name: 'Loan', selected: false, icon: 'md-cash' },
        { id: 1, name: 'House', selected: false, icon: 'md-home' },
        { id: 2, name: 'Car', selected: false, icon: 'md-car' },
        { id: 3, name: 'Stocks', selected: false, icon: 'md-trending-up' },
        { id: 4, name: 'College', selected: false, icon: 'md-school' },
        { id: 5, name: 'Credit Card', selected: false, icon: 'ios-card' },
        { id: 6, name: 'Custom', selected: false, icon: 'ios-cog' },
      ]
    };

    trackScreenView('GoalsSelect');
  }

  updateState = () => {
    this.setState({
      user: store.getState().user,
      my_goals: store.getState().this_user_goals,
    });
  }

  async goalDetailNavigate() {
    var selected = []
    for (let i = 0; i < this.state.goals.length; i++) {
      if (this.state.goals[i].selected === true) {
        selected.push({ userId: this.state.user.id, goalType: this.state.goals[i].name, startAmount: 0, targetAmount: 0, targetMonths: 0, status: 'A', icon: this.state.goals[i].icon })
      }
    }
    var selectedNext = selected

    var toCreate = []

    selected.forEach(element => {
      var index = this.state.my_goals.findIndex(sel => sel.goalType == element.goalType);
      if (index < 0) {
        toCreate.push(element);
      }
    });

    var old = toCreate
    
    for (let i = 0; i < old.length; i++) {
      delete old[i].icon
    }

    for (let i = 0; i < old.length; i++) {
      await createGoal(old[i]);
    }

    await getUserGoal((this.state.user.id), '');

    this.props.navigation.navigate('goals_detail', { selectedGoals: selectedNext })
  }

  async monitorCount() {
    var count = 0;
    for (let i = 0; i < this.state.goals.length; i++) {
      if (this.state.goals[i].selected == true) {
        count = count + 1
      }
      await this.setState({ selectedCount: count })
    }
  }

  async selectGoal(goalid) {

    const data = [...this.state.goals];
    const index = data.findIndex(el => el.id === goalid);
    if (this.state.goals[index].selected == true) {
      data[index] = { ...data[index], selected: !this.state.goals[index].selected };

      await this.setState({ goals: data })

      this.monitorCount();
    }

    else if (this.state.selectedCount < 3) {
      const data = [...this.state.goals];
      const index = data.findIndex(el => el.id === goalid);
      data[index] = { ...data[index], selected: !this.state.goals[index].selected };

      await this.setState({ goals: data })

      this.monitorCount();
    }
  }
  
  componentWillUnmount() {
  }

  componentDidMount() {
    var thisgoals = this.state.goals
    for (let i = 0; i < thisgoals.length; i++) {
      for (let j = 0; j < this.state.my_goals.length; j++) {
        if (thisgoals[i].name == this.state.my_goals[j].goalType) {
          thisgoals[i].selected = true
        }
      }
    }
    this.setState({ goals: thisgoals })
  }

  renderMenuHeader = (title) => {
    return (
      <View style={cs.flex_row_space_between}>
        <Text style={[cs.title1black, cs.m5]}>{title}</Text>
      </View>
    );
  }

  render() {
    let st = this.state;

    let user = st.user;

    const { navigate } = this.props.navigation;
    if (!user) { return (<View style={cs.normalPage} />); }


    return (
      <View style={cs.normalPage}>

        <Header
          navigate={navigate}
          title='Set Goals'
          back='credit_profile'
        />
        { /*** Identity ***/}
        <View style={{ flex: 1, marginHorizontal: 6, marginVertical: 10, backgroundColor: 'white', paddingHorizontal: 15, borderRadius: 14 }}>
          <View style={{ width: '100%', height: '100%', flexWrap: 'wrap' }}>


            <View style={{ alignSelf: 'center', paddingVertical: 13 }}>
              {this.renderMenuHeader('Select your top 3 goals')}
            </View>

            <FlatList
              style={{ alignSelf: 'center', width: '100%' }}
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              data={this.state.goals}
              numColumns={2}
              contentContainerStyle={{ alignItems: 'center' }}
              renderItem={({ item }) => (
                <View style={{ alignItems: 'center', marginHorizontal: 50, marginVertical: 10 }}>
                  <TouchableOpacity onPress={() => this.selectGoal(item.id)} activeOpacity={0.9} style={{ width: 60, height: 60, padding: 2, borderRadius: 30, borderColor: item.selected ? '#3C5468' : colors.seagreen, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: item.selected ? '#3C5468' : 'white' }}>
                    <Icon name={item.icon} color={item.selected ? 'white' : colors.seagreen} size={35} />
                  </TouchableOpacity>
                  <Text style={{ color: '#2e3131', marginVertical: 3 }}>{item.name}</Text>
                </View>
              )} />


            <TouchableOpacity onPress={() => this.goalDetailNavigate()} activeOpacity={0.9} style={{ bottom: 0, marginBottom: 7, marginVertical: 12, width: '100%', paddingVertical: 13, borderRadius: 29, alignSelf: 'center', borderColor: colors.seagreen, borderWidth: 2, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.seagreen }}>
              <Text style={{ color: 'white', fontSize: 17 }}>NEXT</Text>
            </TouchableOpacity>
          </View>
        </View>
        <Footer navigate={navigate} />

      </View>
    );
  }
}

GoalsSelect.propTypes = {
  navigation: PropTypes.object.isRequired
};

GoalsSelect.defaultProps = {
  user: null,
};
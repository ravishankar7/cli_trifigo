import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';

import PrimeModal from '../elements/prime_modal';
import Button from '../elements/button';

import cs from '../styles/common_styles';
import { colors, fontFamily, fontSize, deviceWidth } from '../styles/variables';

const credit = {
  image1: {
    uri: require('../../images/credit_page/creditscore.png')
  }
}

export default class SocialRank extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
    };
  }

  render() {
    let st = this.state;

    return (
      <TouchableOpacity style={styles.w} activeOpacity={0.8} onPress={() => this.setState({ modalVisible: true })}>
        {/* <View style={cs.shadowBox}> */}
          <View style={[cs.flex_center, , { paddingTop: 25 }]}>
          <Text style={[styles.headertext, { marginTop: 5, fontSize: 12 }]}> {'Socail Ranking'} </Text>
            <View style={[{ flexDirection: 'row', height: 50, justifyContent: 'center', marginTop: 5, alignItems : 'center' }]}>
            <Image style={[{ marginLeft: 5,  width: 50, height:50 }]} source={credit.image1.uri} />
            <Text style={[ styles.headertext, { color:colors.green, fontWeight: '700', fontSize: 16, flexWrap: 'wrap' }]} > {'2'} </Text>
            </View>
          {/* </View> */}

          <View style={cs.flex_center}>
          </View>

          <PrimeModal
            modalVisible={st.modalVisible}
            onRequestClose={() => { this.setState({ modalVisible: false }) }}>
            <View style={[cs.shadowBox, styles.wp]}>
              <View style={[cs.flex_row_start, cs.p5]}>
                <Text style={styles.headertextpopup}>Social Rank</Text>
              </View>
              <Text style={[cs.p10]}>Coming soon</Text>
              <View style={[cs.flex_center, cs.p10, cs.mb20]}>
                <Button
                  text='Close'
                  width={100}
                  height={28}
                  fontSize={13}
                  backgroundColor='#546E7A'
                  onClick={() => this.setState({ modalVisible: false })} />
              </View>
            </View>
          </PrimeModal>

        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  headertext: {
    fontFamily: fontFamily.normal,
    fontSize: fontSize.xsmall
  },
  headertextpopup: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.normal
  },
  subtext: {
    fontFamily: fontFamily.regular,
    fontSize: fontSize.xsmall,
    color: colors.green,
  },
  subtextpopup: {
    fontFamily: fontFamily.normal,
    fontSize: fontSize.normal,
    color: colors.green,
  },
  w: {
    width: deviceWidth /3
  },
  wp: {
    width: deviceWidth - 50
  }
});